//
//  PopoverTableView.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 15/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class PopoverTableView: UIView,UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate{
    let cellReuseIdentifier = "cell"
    var categoryModedls = [CategoryModel]()
    var selectedTextField = ""
    var delegate:UIViewController?
    var selectedCategory = CategoryModel()
    @IBOutlet weak var tableViewContainerHeightContstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTextField == kEventCategory  {
            
            return self.categoryModedls.count
        }
        return 0
        
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            // create a new cell if needed or reuse an old one
    
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            }
            
        if selectedTextField == kEventCategory  {
            
            if self.categoryModedls.count > 0 {
               // cell?.textLabel!.text = self.arrray[indexPath.row]
                cell?.textLabel!.text = self.categoryModedls[indexPath.row].cat_name
                
//                let filter = self.selectedCategory.filter { (cat) -> Bool in
//                    cat.categoryId == self.categoryModedls[indexPath.row].categoryId
//                }
//                    if filter.count > 0
//                    {
//                        cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
//                    }
//                    else
//                    {
//                         cell!.accessoryType = UITableViewCell.AccessoryType.none
//                    }
                
                print("selectedcatid = \(self.selectedCategory.categoryId)")
                print("modelid = \(self.categoryModedls[indexPath.row].categoryId)")
               if self.selectedCategory.categoryId == self.categoryModedls[indexPath.row].categoryId    {
                
                    cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                }
                else    {
                
                    cell!.accessoryType = UITableViewCell.AccessoryType.none
                }
            }
        }
          
        cell?.textLabel?.numberOfLines = 0
            
        return cell!
           
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)   {
        
    let cell = tableView.cellForRow(at: indexPath as IndexPath)

    if cell!.isSelected {
            
    cell!.isSelected = false
        
        if cell!.accessoryType == UITableViewCell.AccessoryType.none    {
            
            if selectedTextField == kEventCategory  {
//                   let filterValue =  self.selectedCategory.filter { (model) -> Bool in
//                        model.cat_name == self.categoryModedls[indexPath.row].cat_name
//                    }
//
//                    if filterValue.count == 0
//                    {
//                    self.selectedCategory.append(self.categoryModedls[indexPath.row])
//                          cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
//                    }
                  
                self.selectedCategory = categoryModedls[indexPath.row]
//                    self.selectedCategory.removeAll()
//                    self.selectedCategory.append(self.categoryModedls[indexPath.row])
                cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                   
                tableView.reloadData()
//                    print("selected category count = \(self.selectedCategory.count)")
                   
            }
       
               // self.selectedCategory.append(self.categoryModedls[indexPath.row])
        }
        else {
            
            if selectedTextField == kEventCategory {
                    
//                let filteVlaue = self.selectedCategory.filter { (model) -> Bool in
//                model.cat_name != self.categoryModedls[indexPath.row].cat_name
//              }
//
//                self.selectedCategory = filteVlaue
//                print("selected category count = \(self.selectedCategory.count)")
                    
            self.selectedCategory = CategoryModel()
//                    self.selectedCategory.removeAll()
            cell!.accessoryType = UITableViewCell.AccessoryType.none
            tableView.reloadData()
                    
//                     print("selected category count = \(self.selectedCategory.count)")
        }
               
        cell!.accessoryType = UITableViewCell.AccessoryType.none
     }
   }
}

        
    @IBOutlet weak var image: UIImageView!
        // var image:UIImage?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
           
    }
  
    init(frame: CGRect ,categoryArray:[CategoryModel],selectedTextField:String,selectedCat:CategoryModel) {
        
        super.init(frame:frame)
        self.selectedTextField = selectedTextField
            
        if selectedTextField == kEventCategory {
            
            self.categoryModedls = categoryArray
            self.selectedCategory = selectedCat
        }
//            self.tableView.reloadData()
        self.xibSetup()
            
    }
        
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         self.xibSetup()
    }
        
    func xibSetup() {
            
        let view: UIView? = loadViewFromNib()
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            
        if let view = view {
                addSubview(view)
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
            
        if selectedTextField == kEventCategory {
            
         self.lbl_Title.text = "Select Category"
                
         let height = CGFloat( (Double(self.categoryModedls.count)*44.0 ))
         print("height = \(height)")
         print("view height = \(self.frame.size.height )")
            
         if self.frame.size.height > height {
            
                       /// self.containerTableViewHeightConstraint.constant = height
            self.tableViewContainerHeightContstraint.constant = height + 100
            self.tableViewHeightConstraint.constant = height
            self.tableView.layoutIfNeeded()
            self.layoutIfNeeded()
        }
        else {
            
            self.tableViewContainerHeightContstraint.constant = self.frame.size.height - 100
            self.tableViewHeightConstraint.constant = self.frame.size.height - 150
            self.tableView.layoutIfNeeded()
            self.layoutIfNeeded()
        }
    }
           

    //        UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    //        swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    //        [self addGestureRecognizer:swipeleft];
 }
    
    
    func loadViewFromNib() -> UIView? {
            
        let nibs = Bundle.main.loadNibNamed("PopoverTableView", owner: self, options: nil)
        return nibs?[0] as? UIView

    }
        
    //    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    //        closePopover()
    //        return true
    //    }
    
    func closePopover() {
            //super.removeFromSuperview()
          
        }
        
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
            if touch.view?.isDescendant(of: self) ?? true {
                return false
            }
            return true
        }
        
        @IBAction func tapGesture(_ sender: Any) {
           self.removeFromSuperview()
        }
        
    @IBAction func backAction(_ sender: Any) {
        
        self.removeFromSuperview()
    }
    
    @IBAction func actionDoneButton(_ sender: Any) {
       
        if selectedTextField == kEventCategory {
            
            (self.delegate as! UploadAnEventVC).selectedCategory(selectedCategory: self.selectedCategory)
        }
            
        self.removeFromSuperview()
    }
}
