//
//  PopoverNewsCatTableView.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 18/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class PopoverNewsCatTableView: UIView ,UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate{
    
    let cellReuseIdentifier = "cell"
    var categoryModedls = [CategoryModel]()
    var newsTopicModels = [TopicModel]()
    
    var selectedCategory = [CategoryModel]()
    var selectedNewsTopic = [TopicModel]()
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var selectedTextField = ""
    var delegate:UIViewController?
    
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTableViewHeightConstraint: NSLayoutConstraint!
    
     func numberOfSections(in tableView: UITableView) -> Int {
            
        return 1
        }
            
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // return self.arrray.count
        if selectedTextField == kCategory
        {
            return self.categoryModedls.count
        }
        else if selectedTextField == kNewsTopic
        {
            return self.newsTopicModels.count
        }
                
        return 0
    }
            
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                // create a new cell if needed or reuse an old one
    
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if cell == nil {
        
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }
                
        if selectedTextField == kCategory
            {
            if self.categoryModedls.count > 0 {
                   
            // cell?.textLabel!.text = self.arrray[indexPath.row]
            cell?.textLabel!.text = self.categoryModedls[indexPath.row].cat_name
                    
            let filter = self.selectedCategory.filter { (cat) -> Bool in
            cat.categoryId == self.categoryModedls[indexPath.row].categoryId
            }
       
        if filter.count > 0
            {
                cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
        else
            {
                cell!.accessoryType = UITableViewCell.AccessoryType.none
            }
          }
        }
        
        else if selectedTextField == kNewsTopic
        {
            if self.newsTopicModels.count > 0 {
                        
                 // cell?.textLabel!.text = self.arrray[indexPath.row]
                cell?.textLabel!.text = self.newsTopicModels[indexPath.row].value
                         
                 let filter = self.selectedNewsTopic.filter { (cat) -> Bool in
                 cat.key == self.newsTopicModels[indexPath.row].key
                 }
            
             if filter.count > 0
                 {
                     cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                 }
             else
                 {
                     cell!.accessoryType = UITableViewCell.AccessoryType.none
                 }
               }
        }
              
        cell?.textLabel?.numberOfLines = 0
            
        return cell!
            
    }
        
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
        let cell = tableView.cellForRow(at: indexPath as IndexPath)

        if cell!.isSelected
          {
                cell!.isSelected = false
                if cell!.accessoryType == UITableViewCell.AccessoryType.none
                {
                    if selectedTextField == kCategory
                    {
                       let filterValue =  self.selectedCategory.filter { (model) -> Bool in
                            model.cat_name == self.categoryModedls[indexPath.row].cat_name
                        }
    
                        if filterValue.count == 0
                        {
                        self.selectedCategory.append(self.categoryModedls[indexPath.row])
                              cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                        }
                      
                        //******
//                        self.selectedCategory = categoryModedls[indexPath.row]
                        //*******
                        
//                        self.selectedCategory.removeAll()
//                        self.selectedCategory.append(self.categoryModedls[indexPath.row])
                        cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                       
                        tableView.reloadData()                       
                    }
                    else if selectedTextField == kNewsTopic
                    {
                        let filterValue =  self.selectedNewsTopic.filter { (model) -> Bool in
                    
                        model.value == self.newsTopicModels[indexPath.row].value
                        }
                            
                        if filterValue.count == 0
                        {
                        self.selectedNewsTopic.append(self.newsTopicModels[indexPath.row])
                        cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                        }
                                    
//*****
//                        self.selectedCategory = categoryModedls[indexPath.row]
                                                //*******
                                            
//                        self.selectedCategory.removeAll()
//                        self.selectedCategory.append(self.categoryModedls[indexPath.row])
                        cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                                               
                        tableView.reloadData()
                        
                    }
           
                   // self.selectedCategory.append(self.categoryModedls[indexPath.row])
                }
                else
                {
                    if selectedTextField == kCategory {
                        
                    let filteVlaue = self.selectedCategory.filter { (model) -> Bool in
                    model.cat_name != self.categoryModedls[indexPath.row].cat_name
                  }
    
                    self.selectedCategory = filteVlaue
                    print("selected category count = \(self.selectedCategory.count)")
                        
                        //********
                       // self.selectedCategory = CategoryModel()
                        
                        //********
                        
                        
//                        self.selectedCategory.removeAll()
                        cell!.accessoryType = UITableViewCell.AccessoryType.none
                        tableView.reloadData()
                        
                         print("selected category count = \(self.selectedCategory.count)")
                    }
            else if selectedTextField == kNewsTopic
                {
                     let filteVlaue = self.selectedNewsTopic.filter { (model) -> Bool in
                     model.value != self.newsTopicModels[indexPath.row].value
                    
                    }
                        
                 self.selectedNewsTopic = filteVlaue
                 print("selected news topic count = \(self.selectedNewsTopic.count)")
                                            
        //********
       // self.selectedCategory = CategoryModel()
                                            
              //********
                                            
                                            
//                        self.selectedCategory.removeAll()
                    cell!.accessoryType = UITableViewCell.AccessoryType.none
                    tableView.reloadData()
                                            
                    print("selected news topic count = \(self.selectedCategory.count)")
                    
                    }
                    
                    
                    cell!.accessoryType = UITableViewCell.AccessoryType.none
                }
            }
        }

            
            @IBOutlet weak var image: UIImageView!
            // var image:UIImage?
            override init(frame: CGRect) {
                super.init(frame: frame)
                self.xibSetup()
               
            }
      
    init(frame: CGRect ,categoryArray:[CategoryModel],selectedTextField:String,selectedCategory:[CategoryModel])
             {
                super.init(frame:frame)
                self.selectedTextField = selectedTextField
                
                if selectedTextField == kCategory
                {
                    self.categoryModedls = categoryArray
                    self.selectedCategory = selectedCategory
                }
//              self.tableView.reloadData()
                self.xibSetup()
                
//                let height = CGFloat( (Double(self.categoryModedls.count)*44.0 + 60.0))
//                print("height = \(height)")
//                print("view height = \(self.delegate?.view.frame.height ?? 0.0)")
//                if self.delegate?.view.frame.height ?? 0.0 > height
//                {
//                    self.containerTableViewHeightConstraint.constant = height
//                }
//                else
//                {
//                    self.containerTableViewHeightConstraint.constant = self.delegate?.view.bounds.height ?? 0.0
//                }
//                self.containerTableViewHeightConstraint.constant = self.tableView.contentSize.height
                
             }
    
    init(frame: CGRect ,newsTopic:[TopicModel],selectedTextField:String,selectedTopic:[TopicModel])
                {
                   super.init(frame:frame)
                   self.selectedTextField = selectedTextField
                   
                   if selectedTextField == kNewsTopic
                   {
                       self.newsTopicModels = newsTopic
                     self.selectedNewsTopic = selectedTopic
                   }
                 // self.tableView.reloadData()
                   self.xibSetup()
                
                }
            
            required init?(coder aDecoder: NSCoder) {
                super.init(coder: aDecoder)
                self.xibSetup()
                
                
            }
            
            func xibSetup() {
                
                let view: UIView? = loadViewFromNib()
                view?.frame = bounds
                view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                view?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                if let view = view {
                    addSubview(view)
                }
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.tableFooterView = UIView()
               
                if selectedTextField == kCategory
                {
                    self.lbl_title.text = "Select Category"
                    
                    let height = CGFloat( (Double(self.categoryModedls.count)*44.0 - 120.0))
                    print("height = \(height)")
                    print("view height = \(self.frame.size.height ?? 0.0)")
                    if self.frame.size.height > height
                    {
                                         /// self.containerTableViewHeightConstraint.constant = height
                        self.containerTableViewHeightConstraint.constant = height
                        self.tableViewHeightConstraint.constant = height
                        self.tableView.layoutIfNeeded()
                        self.layoutIfNeeded()
                    }
                else
                    {
                        self.containerTableViewHeightConstraint.constant = self.frame.size.height - 50
                        self.tableViewHeightConstraint.constant = self.frame.size.height - 100
                        
                        self.tableView.layoutIfNeeded()
                        self.layoutIfNeeded()
                    }
                }
                else if selectedTextField == kNewsTopic
                {
                    self.lbl_title.text = "Select News Topic"
                    let height = CGFloat( (Double(self.newsTopicModels.count)*44.0 ))
                    print("height = \(height)")
                    print("view height = \(self.frame.size.height ?? 0.0)")
                    if self.frame.size.height > height
                    {
                       /// self.containerTableViewHeightConstraint.constant = height
                        self.containerTableViewHeightConstraint.constant = height + 100
                        self.tableViewHeightConstraint.constant = height
                        self.tableView.layoutIfNeeded()
                        self.layoutIfNeeded()
                    }
                else
                  {
                    self.containerTableViewHeightConstraint.constant = self.frame.size.height - 100
                    self.tableViewHeightConstraint.constant = self.frame.size.height - 150
                    self.tableView.layoutIfNeeded()
                    self.layoutIfNeeded()
                  }
                }
               
                self.delegate?.view.endEditing(true)
        //        UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
        //        swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
        //        [self addGestureRecognizer:swipeleft];
            }
        
        
            func loadViewFromNib() -> UIView? {
                
                let nibs = Bundle.main.loadNibNamed("PopoverNewsCatTableView", owner: self, options: nil)
                return nibs?[0] as? UIView

            }
            
        //    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        //        closePopover()
        //        return true
        //    }
            func closePopover() {
                //super.removeFromSuperview()
              
            }
            
        func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
                if touch.view?.isDescendant(of: self) ?? true {
                    return false
                }
                return true
            }
            
            @IBAction func tapGesture(_ sender: Any) {
               self.removeFromSuperview()
            }
            
        @IBAction func backAction(_ sender: Any) {
            self.delegate?.view.endEditing(true)
            self.removeFromSuperview()
        }
        
        @IBAction func actionDoneButton(_ sender: Any) {
           
        if selectedTextField == kCategory {
        print("selected category count = \(self.selectedCategory.count)")
        (self.delegate as! AddNewsVC).selectedCategory(selectedCategory: self.selectedCategory)
            }
            
    if selectedTextField == kNewsTopic {
        
        (self.delegate as! AddNewsVC).selectedNewsTopics(selectedNewsTopic: self.selectedNewsTopic)
            }
            
             self.delegate?.view.endEditing(true)
            self.removeFromSuperview()
        }
    
    @IBAction func actionBack(_ sender: Any) {
    }
    

}
