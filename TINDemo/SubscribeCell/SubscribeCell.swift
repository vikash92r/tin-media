//
//  SubscribeCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 05/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SubscribeCell: UITableViewCell {
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var btnSubscribe: UIButton!
    var delegate:PopoverView?
    override func awakeFromNib() {
        super.awakeFromNib()
        txt_email?.placeholderColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        self.background.layer.cornerRadius = 4
        self.btnSubscribe.layer.cornerRadius = 4
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionSubscribe(_ sender: Any) {
        print("email = \(txt_email.text)")
        print("subscribe button click")
        callSubscribeApi()
    }
    
    func callSubscribeApi()
    {
    
     let myurl = kSubscribePath
        guard   self.txt_email.text?.count ?? 0 > 0 else {
            Utility.displayAlert(title: kAppTitle, message: kEnterEmailToSubscribe)
            return
        }
        
        let email = self.txt_email.text ?? ""
        guard !Utility.isNotValidEmail(email: email) else
                  {
                      Utility.displayAlert(title: kAppTitle, message: kEnterValidEmail)
                      return
                  }
        
              
               print("path url = \(myurl)")
               var param =  Parameters()
               param["api_key"] = kApiKey
               param["email"] = email
                        
               WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                            
               let swiftyJsonVar = JSON(result)
                
                switch swiftyJsonVar["response"]
                {
                case  "200":
                             
                    Utility.displayAlert(title: kAppTitle, message: swiftyJsonVar["message"].stringValue)
                default:
                    Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
                    break
                }
                print("result = \(swiftyJsonVar)")
              
//                     Utility.displayAlert(title: kAppTitle, message: "you have success fully subscribed.")
               }
     
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}
