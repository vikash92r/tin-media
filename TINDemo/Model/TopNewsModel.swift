//
//  TopNewsModel.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 06/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import Foundation

class TopNewsModel
{
    var id:String?
    var news_heading:String?
    var image:String?
    var days_ago:String?
    var categoryId:String?
    var news_description:String?
}
