//
//  GalleryModel.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 07/02/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import Foundation
class GalleryModel
{
    var gallery_id:String?
    var event:String?
    var image:String?
    var date:String?
    var location:String?
}
