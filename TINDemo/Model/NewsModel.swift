//
//  NewsModel.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 09/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import Foundation

class NewsModel
{
    var id:String?
    var news_heading:String?
    var image:String?
    var days_ago:String?
    var categoryId:String?
    var from:String?
    var image_url:String?
    var video_url:String?
    var description:NSAttributedString?
   
    var event_cat:String?
    var title:String?
    var location:String?
    var date_from:String?
    var date_to:String?
   
    var modelType:String?
    var news_url:String?
    var created_date:String?
    
}
