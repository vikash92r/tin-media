//
//  NewsDetailModel.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 09/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import Foundation
class NewsDetailModel
{
var id:String?
   var news_heading:String?
   var image:String?
   var days_ago:String?
   var categoryId:String?
   var news_description:NSAttributedString?
   var description:NSAttributedString?
   var from:String?
    var news_url:String?
    var location:String?
    var news_cat:String?
    var news_topic:String?
}
