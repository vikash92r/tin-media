//
//  pagesDataModel.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 21/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import Foundation
class PagesDataModel
{
    var page_title:String?
    var page_content:String?
}
