//
//  BlankTableViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 19/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class BlankTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
