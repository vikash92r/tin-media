//
//  VediosDetailsTableViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 13/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class VediosDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var vedioImageView: UIImageView!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_timeDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
