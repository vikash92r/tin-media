//
//  TextViewTableViewCell.swift
//  
//
//  Created by Anuj Mohan Saxena on 16/12/19.
//

import UIKit

class TextViewTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_from: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var lbl_heading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
