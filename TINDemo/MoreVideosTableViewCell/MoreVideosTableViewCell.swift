//
//  MoreVideosTableViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 23/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class MoreVideosTableViewCell: UITableViewCell {
    @IBOutlet weak var btn_moreVideos: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.btn_moreVideos.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
