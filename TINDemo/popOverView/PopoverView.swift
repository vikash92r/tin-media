//
//  PopOverView.swift
//  SQLiteSwiftDem
//
//  Created by Anuj Mohan Saxena on 27/05/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
class PopoverView: UIView,UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate
{
    @IBOutlet weak var backgroungImage: UIImageView!
    var categoryList = [[String:String]]()
    var arrray:[String] = ["a","b","c"]
    var menuArray = [MenuModel]()
    var menuItems = [String]()
    let cellReuseIdentifier = "cell"
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var userProfileCard: UIView!
    @IBOutlet weak var userLoginSignupCard: UIView!
    @IBOutlet weak var signupCardHeight: NSLayoutConstraint!
    var delegate:UIViewController?
    @IBOutlet weak var loginCardHeight: NSLayoutConstraint!
    
    @IBOutlet weak var logoutButton: UIButton!
    let singOutNotification = NotificationCenter.default
    
    var fromPreviousController = ""
    
    var itemarrayOld = ["My Profile","Update Profile","Change Password","My News","Add News","Add Event","My Event","Upload Videos","Upload Photos","Subscribe","Logout"]
    
    var itemarrySocialLoginOld = ["My Profile","Update Profile","My News","Add News","Add Event","My Event","Upload Videos","Upload Photos","Subscribe","Logout"]
    
    var itemarray = ["My Profile","Update Profile","Change Password","My News","Add News","Add Event","My Event","Subscribe","Logout"]
       
    var itemarrySocialLogin = ["My Profile","Update Profile","My News","Add News","Add Event","My Event","Subscribe","Logout"]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.menuItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if cell == nil
        {
          cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }
        
        if self.menuItems.count > 0
        {
          cell?.textLabel!.text = menuItems[indexPath.row]
         cell?.textLabel?.numberOfLines = 0
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      if UserDetails.sharedInstance.email.count > 0 {
        
        if  (self.fromPreviousController == "Profile") {
          
          if menuItems[indexPath.row] == "My Profile" {
                
             let viewControllerObj = (self.delegate as! UIViewController)
             let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
             self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
             self.closeAnimation()
                     
         }
     else if menuItems[indexPath.row] == "Update Profile" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "UpdateProfileViewController") as! UpdateProfileViewController
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
                         
         }
    else if menuItems[indexPath.row] == "Change Password"  {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
        }
     else if menuItems[indexPath.row] == "My News" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "MyNewsVC") as! MyNewsVC
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
        }
     else if menuItems[indexPath.row] == "Add News" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "AddNewsVC") as! AddNewsVC
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
        }
    else if menuItems[indexPath.row] == "Add Event" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "UploadAnEventVC") as! UploadAnEventVC
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
         }
    else if menuItems[indexPath.row] == "My Event" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
        }
    else if menuItems[indexPath.row] == "Subscribe" {
            
            let viewControllerObj = (self.delegate as! UIViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
            self.closeAnimation()
        }
   else if menuItems[indexPath.row] == "Photos" {
                       
         let viewControllerObj = (self.delegate as! UIViewController)
         let pvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        self.delegate?.navigationController?.pushViewController(pvc, animated: true)
        self.closeAnimation()
    }
    else if menuItems[indexPath.row] == "Logout" {
            
                 self.logoutButtonTapped()
                /*
                let dialogMessage = UIAlertController(title: kAppTitle, message: "Are you sure, you want to logout?", preferredStyle: .alert)
                       
                       // Create OK button with action handler
                let ok = UIAlertAction(title: "Logout", style: .default, handler: { (action) -> Void in
                            print("Ok button tapped")
                    
              if self.fromPreviousController  == "Profile"
                {
                    Utility.clearUserDetails()
                    self.signupCardHeight.constant = 120
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("userLogout"), object: nil)
                    
                    let updateProfilePicNotification = NotificationCenter.default
                    updateProfilePicNotification.post(name:Notification.Name("userUpdateProfilePic"),object: nil)
                                             
                    let ncsetting =  NotificationCenter.default
                    nc.post(name: Notification.Name("userLogoutSetting"), object: nil)
                                                              
                    GIDSignIn.sharedInstance()?.signOut()
                    
                    let loginManager = LoginManager()
                    loginManager.logOut()
                                          
                    self.menuItems = leftMenuItems
                    tableView.reloadData()
                    Utility.tabBarAsRootViewController()
                }
            else
              {
                    Utility.clearUserDetails()
                    self.signupCardHeight.constant = 120
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("userLogout"), object: nil)
                
                   let updateProfilePicNotification = NotificationCenter.default
                   updateProfilePicNotification.post(name:Notification.Name("userUpdateProfilePic"),object: nil)
                                         
                    let ncsetting =  NotificationCenter.default
                    nc.post(name: Notification.Name("userLogoutSetting"), object: nil)
                                                          
                    GIDSignIn.sharedInstance()?.signOut()
                                                           
                    Utility.displayAlert(title: kAppTitle, message: "You Logout Successfully.")
                                                        
                    let loginManager = LoginManager()
                    loginManager.logOut()
                                      
                    self.menuItems = leftMenuItems
                    tableView.reloadData()
                
                    }
                   
                })
                       
          // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                print("Cancel button tapped")
            }
                       
        //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                       
        // Present dialog message to user
                      
                       
            if (self.delegate as AnyObject?) is ViewController
                {
                                      
                 let viewControllerObj = (self.delegate as! ViewController)
                 viewControllerObj.present(dialogMessage, animated: true, completion: nil)

                }
            
            else if (self.delegate as AnyObject?) is PopularNewsViewController
                {
                  let popular = (self.delegate as! PopularNewsViewController)
                  popular.present(dialogMessage, animated: true, completion: nil)
                }
                
          else if (self.delegate as AnyObject?) is BookMarkVC
               {
                    let bookmark = (self.delegate as! BookMarkVC)
                    bookmark.present(dialogMessage, animated: true, completion: nil)
               }
         
         else if (self.delegate as AnyObject?) is VideosVC
              {
                let video = (self.delegate as! VideosVC)
                video.present(dialogMessage, animated: true, completion: nil)
             }
         
        else if (self.delegate as AnyObject?) is SettingVC
            {
                let setting = (self.delegate as! SettingVC)
                setting.present(dialogMessage, animated: true, completion: nil)
            }
        
        else if (self.delegate as AnyObject?) is ProfileViewController
            {
                let setting = (self.delegate as! ProfileViewController)
                setting.present(dialogMessage, animated: true, completion: nil)
            }
                 */
         }
        else
            {
                 Utility.displayAlert(title: kAppTitle, message: "Coming soon...")
            }
            
    }
 else {
            
        if menuItems[indexPath.row] == "Subscribe" {
            
          if (self.delegate as AnyObject?) is ViewController {
            
                         
            let viewControllerObj = (self.delegate as! ViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
                                 
            closeAnimation()
            self.delegate?.navigationController?.pushViewController(pdvc, animated: true)

          }
        else if (self.delegate as AnyObject?) is PopularNewsViewController {
            
            let popular = (self.delegate as! PopularNewsViewController)
            let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
            self.closeAnimation()
            popular.leftMenuClosed()
            popular.navigationController?.pushViewController(pdvc, animated: true)
         }
       else if (self.delegate as AnyObject?) is BookMarkVC {
            
            let bookmark = (self.delegate as! BookMarkVC)
            let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
            self.closeAnimation()
            bookmark.leftMenuClosed()
            bookmark.navigationController?.pushViewController(pdvc, animated: true)
         }
       else if (self.delegate as AnyObject?) is VideosVC {
            
            let video = (self.delegate as! VideosVC)
            let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
            self.closeAnimation()
            video.leftMenuClosed()
            video.navigationController?.pushViewController(pdvc, animated: true)
         }
       else if (self.delegate as AnyObject?) is SettingVC {
            
            let setting = (self.delegate as! SettingVC)
            let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
            self.closeAnimation()
            setting.leftMenuClosed()
            setting.navigationController?.pushViewController(pdvc, animated: true)
        }
                     
     }
 else if menuItems[indexPath.row] == "Videos" {
                    
           self.delegate?.tabBarController?.selectedIndex = 3
           closeAnimation()
      }
    else if menuItems[indexPath.row] == "Events" ||   menuItems[indexPath.row] == "Webinars" ||   menuItems[indexPath.row] == "Fam Trips" ||   menuItems[indexPath.row] == "Conferences" ||   menuItems[indexPath.row] == "Networking" ||   menuItems[indexPath.row] == "Trade Shows" {
                     /*
                      "Webinars","Fam Trips","Conferences","Networking","Trade Shows"
                      */
        if (self.delegate as AnyObject?) is ViewController {
                           
            let viewControllerObj = (self.delegate as! ViewController)
            let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
                     
            if menuItems[indexPath.row] == "Events" {
                
               pdvc.previousController = "Events"
               pdvc.catId = "999"
               pdvc.catName = "UPCOMING EVENTS"
            }
         else if menuItems[indexPath.row] == "Webinars" {
                
               pdvc.catId = "webinars"
               pdvc.catName = "Webinars"
            }
        else if menuItems[indexPath.row] == "Fam Trips" {
                
               pdvc.catId = "famtrips"
               pdvc.catName = "Fam Trips"
            }
        else if menuItems[indexPath.row] == "Conferences" {
                
               pdvc.catId = "conferences"
               pdvc.catName = "Conferences"
            }
        else if menuItems[indexPath.row] == "Networking" {
                
               pdvc.catId = "networking"
               pdvc.catName = "Networking"
            }
        else if menuItems[indexPath.row] == "Trade Shows" {
                
               pdvc.catId = "tradeshows"
               pdvc.catName = "Trade Shows"
            }
                         
        closeAnimation()
        self.delegate?.navigationController?.pushViewController(pdvc, animated: true)

        }
    else if (self.delegate as AnyObject?) is PopularNewsViewController {
            
        let popular = (self.delegate as! PopularNewsViewController)
        let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
        self.closeAnimation()
                        
        if menuItems[indexPath.row] == "Events" {
            
           pdvc.previousController = "Events"
           pdvc.catId = "999"
           pdvc.catName = "UPCOMING EVENTS"
        }
       else if menuItems[indexPath.row] == "Webinars" {
            
           pdvc.catId = "webinars"
           pdvc.catName = "Webinars"
        }
      else if menuItems[indexPath.row] == "Fam Trips" {
            
           pdvc.catId = "famtrips"
           pdvc.catName = "Fam Trips"
        }
     else if menuItems[indexPath.row] == "Conferences" {
            
           pdvc.catId = "conferences"
           pdvc.catName = "Conferences"
        }
    else if menuItems[indexPath.row] == "Networking" {
            
           pdvc.catId = "networking"
           pdvc.catName = "Networking"
        }
     else if menuItems[indexPath.row] == "Trade Shows" {
            
           pdvc.catId = "tradeshows"
           pdvc.catName = "Trade Shows"
        }
                        
      popular.leftMenuClosed()
      popular.navigationController?.pushViewController(pdvc, animated: true)
    }
  else if (self.delegate as AnyObject?) is BookMarkVC {
            
      let bookmark = (self.delegate as! BookMarkVC)
      let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
      self.closeAnimation()
                     
      if menuItems[indexPath.row] == "Events" {
        
         pdvc.previousController = "Events"
         pdvc.catId = "999"
         pdvc.catName = "UPCOMING EVENTS"
        }
      else if menuItems[indexPath.row] == "Webinars" {
        
        pdvc.catId = "webinars"
        pdvc.catName = "Webinars"
       }
     else if menuItems[indexPath.row] == "Fam Trips" {
        
        pdvc.catId = "famtrips"
        pdvc.catName = "Fam Trips"
      }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
      }
    else if menuItems[indexPath.row] == "Networking" {
        
        pdvc.catId = "networking"
        pdvc.catName = "Networking"
     }
    else if menuItems[indexPath.row] == "Trade Shows" {
        
        pdvc.catId = "tradeshows"
        pdvc.catName = "Trade Shows"
     }
                                  
     bookmark.leftMenuClosed()
     bookmark.navigationController?.pushViewController(pdvc, animated: true)
   }
 else if (self.delegate as AnyObject?) is VideosVC {
            
     let video = (self.delegate as! VideosVC)
     let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
     self.closeAnimation()
                     
     if menuItems[indexPath.row] == "Events" {
        
        pdvc.previousController = "Events"
        pdvc.catId = "999"
        pdvc.catName = "UPCOMING EVENTS"
      }
    else if menuItems[indexPath.row] == "Webinars" {
        
        pdvc.catId = "webinars"
        pdvc.catName = "Webinars"
      }
   else if menuItems[indexPath.row] == "Fam Trips" {
        
        pdvc.catId = "famtrips"
        pdvc.catName = "Fam Trips"
      }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
      }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
      }
    else if menuItems[indexPath.row] == "Networking" {
        
        pdvc.catId = "networking"
        pdvc.catName = "Networking"
      }
    else if menuItems[indexPath.row] == "Trade Shows" {
        
        pdvc.catId = "tradeshows"
        pdvc.catName = "Trade Shows"
      }
                     
    video.leftMenuClosed()
    video.navigationController?.pushViewController(pdvc, animated: true)
  }
 else if (self.delegate as AnyObject?) is SettingVC {
            
    let setting = (self.delegate as! SettingVC)
    let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
    self.closeAnimation()
                         
    if menuItems[indexPath.row] == "Events" {
        
       pdvc.previousController = "Events"
       pdvc.catId = "999"
       pdvc.catName = "UPCOMING EVENTS"
     }
   else if menuItems[indexPath.row] == "Webinars" {
        
       pdvc.catId = "webinars"
       pdvc.catName = "Webinars"
     }
   else if menuItems[indexPath.row] == "Fam Trips" {
        
       pdvc.catId = "famtrips"
       pdvc.catName = "Fam Trips"
     }
   else if menuItems[indexPath.row] == "Conferences" {
        
       pdvc.catId = "conferences"
       pdvc.catName = "Conferences"
     }
  else if menuItems[indexPath.row] == "Networking" {
        
      pdvc.catId = "networking"
      pdvc.catName = "Networking"
    }
  else if menuItems[indexPath.row] == "Trade Shows" {
        
      pdvc.catId = "tradeshows"
      pdvc.catName = "Trade Shows"
    }
  
    setting.leftMenuClosed()
    setting.navigationController?.pushViewController(pdvc, animated: true)
  }
 }
else if menuItems[indexPath.row] == "Upload and Event"  {
            
        let viewControllerObj = (self.delegate as! UIViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "UploadAnEventVC") as! UploadAnEventVC
        self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
        self.closeAnimation()
                
     }
  else if menuItems[indexPath.row] == "Break Your Own news" {
            
        let viewControllerObj = (self.delegate as! UIViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "AddNewsVC") as! AddNewsVC
        self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
        self.closeAnimation()
     }
 else if menuItems[indexPath.row] == "Photos" {
            
        let viewControllerObj = (self.delegate as! UIViewController)
        let pvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        self.delegate?.navigationController?.pushViewController(pvc, animated: true)
        self.closeAnimation()
    }
  else if menuItems[indexPath.row] == "Logout" {
            
          self.logoutButtonTapped()
    }
          
  else {
        
        Utility.displayAlert(title: kAppTitle, message: "Coming soon...")
      }
    }
  }
else  {
        
  if menuItems[indexPath.row] == "Subscribe" {
        
    if (self.delegate as AnyObject?) is ViewController {
        
                       
        let viewControllerObj = (self.delegate as! ViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
                               
        closeAnimation()
        self.delegate?.navigationController?.pushViewController(pdvc, animated: true)

    }
  else if (self.delegate as AnyObject?) is PopularNewsViewController {
        
        let popular = (self.delegate as! PopularNewsViewController)
        let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
        self.closeAnimation()
        popular.leftMenuClosed()
        popular.navigationController?.pushViewController(pdvc, animated: true)
    }
  else if (self.delegate as AnyObject?) is BookMarkVC {
        
        let bookmark = (self.delegate as! BookMarkVC)
        let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
        self.closeAnimation()
        bookmark.leftMenuClosed()
        bookmark.navigationController?.pushViewController(pdvc, animated: true)
    }
  else if (self.delegate as AnyObject?) is VideosVC {
        
        let video = (self.delegate as! VideosVC)
        let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
        self.closeAnimation()
        video.leftMenuClosed()
        video.navigationController?.pushViewController(pdvc, animated: true)
    }
  else if (self.delegate as AnyObject?) is SettingVC {
        
        let setting = (self.delegate as! SettingVC)
        let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "SubscribeNewsletterVC") as! SubscribeNewsletterVC
        self.closeAnimation()
        setting.leftMenuClosed()
        setting.navigationController?.pushViewController(pdvc, animated: true)
    }
 }
else if menuItems[indexPath.row] == "Videos" {
    
        self.delegate?.tabBarController?.selectedIndex = 3
        closeAnimation()
    }
else if menuItems[indexPath.row] == "Events" ||   menuItems[indexPath.row] == "Webinars" ||   menuItems[indexPath.row] == "Fam Trips" ||   menuItems[indexPath.row] == "Conferences" ||   menuItems[indexPath.row] == "Networking" ||   menuItems[indexPath.row] == "Trade Shows" {
    
                   /*
                    "Webinars","Fam Trips","Conferences","Networking","Trade Shows"
                    */
    if (self.delegate as AnyObject?) is ViewController {
                         
    let viewControllerObj = (self.delegate as! ViewController)
    let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
                   
    if menuItems[indexPath.row] == "Events" {
            
      pdvc.previousController = "Events"
      pdvc.catId = "999"
      pdvc.catName = "UPCOMING EVENTS"
    }
  else if menuItems[indexPath.row] == "Webinars" {
        
      pdvc.catId = "webinars"
      pdvc.catName = "Webinars"
    }
 else if menuItems[indexPath.row] == "Fam Trips" {
        
      pdvc.catId = "famtrips"
      pdvc.catName = "Fam Trips"
    }
 else if menuItems[indexPath.row] == "Conferences" {
        
      pdvc.catId = "conferences"
      pdvc.catName = "Conferences"
    }
 else if menuItems[indexPath.row] == "Networking" {
        
      pdvc.catId = "networking"
      pdvc.catName = "Networking"
    }
 else if menuItems[indexPath.row] == "Trade Shows" {
        
      pdvc.catId = "tradeshows"
      pdvc.catName = "Trade Shows"
    }
                       
    closeAnimation()
    self.delegate?.navigationController?.pushViewController(pdvc, animated: true)

  }
 else if (self.delegate as AnyObject?) is PopularNewsViewController {
        
    let popular = (self.delegate as! PopularNewsViewController)
    let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
    self.closeAnimation()
                      
    if menuItems[indexPath.row] == "Events" {
        
       pdvc.previousController = "Events"
       pdvc.catId = "999"
       pdvc.catName = "UPCOMING EVENTS"
    }
   else if menuItems[indexPath.row] == "Webinars" {
        
       pdvc.catId = "webinars"
       pdvc.catName = "Webinars"
    }
   else if menuItems[indexPath.row] == "Fam Trips" {
        
       pdvc.catId = "famtrips"
       pdvc.catName = "Fam Trips"
    }
   else if menuItems[indexPath.row] == "Conferences" {
        
       pdvc.catId = "conferences"
       pdvc.catName = "Conferences"
    }
   else if menuItems[indexPath.row] == "Networking" {
        
       pdvc.catId = "networking"
       pdvc.catName = "Networking"
    }
   else if menuItems[indexPath.row] == "Trade Shows" {
        
       pdvc.catId = "tradeshows"
       pdvc.catName = "Trade Shows"
    }
                      
    popular.leftMenuClosed()
    popular.navigationController?.pushViewController(pdvc, animated: true)
  }
 else if (self.delegate as AnyObject?) is BookMarkVC {
        
    let bookmark = (self.delegate as! BookMarkVC)
    let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
    self.closeAnimation()
                   
    if menuItems[indexPath.row] == "Events" {
        
       pdvc.previousController = "Events"
       pdvc.catId = "999"
       pdvc.catName = "UPCOMING EVENTS"
     }
    else if menuItems[indexPath.row] == "Webinars" {
        
       pdvc.catId = "webinars"
       pdvc.catName = "Webinars"
     }
    else if menuItems[indexPath.row] == "Fam Trips" {
        
       pdvc.catId = "famtrips"
       pdvc.catName = "Fam Trips"
     }
    else if menuItems[indexPath.row] == "Conferences" {
        
       pdvc.catId = "conferences"
       pdvc.catName = "Conferences"
     }
    else if menuItems[indexPath.row] == "Networking" {
        
       pdvc.catId = "networking"
       pdvc.catName = "Networking"
     }
    else if menuItems[indexPath.row] == "Trade Shows" {
        
       pdvc.catId = "tradeshows"
       pdvc.catName = "Trade Shows"
     }
        
     bookmark.leftMenuClosed()
     bookmark.navigationController?.pushViewController(pdvc, animated: true)
  }
 else if (self.delegate as AnyObject?) is VideosVC {
        
     let video = (self.delegate as! VideosVC)
     let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
     self.closeAnimation()
            
     if menuItems[indexPath.row] == "Events" {
        
        pdvc.previousController = "Events"
        pdvc.catId = "999"
        pdvc.catName = "UPCOMING EVENTS"
    }
    else if menuItems[indexPath.row] == "Webinars" {
        
        pdvc.catId = "webinars"
        pdvc.catName = "Webinars"
    }
    else if menuItems[indexPath.row] == "Fam Trips" {
        
        pdvc.catId = "famtrips"
        pdvc.catName = "Fam Trips"
     }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
    }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
    }
    else if menuItems[indexPath.row] == "Networking" {
        
        pdvc.catId = "networking"
        pdvc.catName = "Networking"
    }
    else if menuItems[indexPath.row] == "Trade Shows" {
        
        pdvc.catId = "tradeshows"
        pdvc.catName = "Trade Shows"
    }
                   
    video.leftMenuClosed()
    video.navigationController?.pushViewController(pdvc, animated: true)
 }
else if (self.delegate as AnyObject?) is SettingVC {
    
    let setting = (self.delegate as! SettingVC)
    let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
    self.closeAnimation()
                       
    if menuItems[indexPath.row] == "Events" {
        
        pdvc.previousController = "Events"
        pdvc.catId = "999"
        pdvc.catName = "UPCOMING EVENTS"
    }
    else if menuItems[indexPath.row] == "Webinars" {
        
        pdvc.catId = "webinars"
        pdvc.catName = "Webinars"
    }
    else if menuItems[indexPath.row] == "Fam Trips" {
        
        pdvc.catId = "famtrips"
        pdvc.catName = "Fam Trips"
    }
    else if menuItems[indexPath.row] == "Conferences" {
        
        pdvc.catId = "conferences"
        pdvc.catName = "Conferences"
    }
    else if menuItems[indexPath.row] == "Networking" {
        
        pdvc.catId = "networking"
        pdvc.catName = "Networking"
    }
    else if menuItems[indexPath.row] == "Trade Shows" {
        
        pdvc.catId = "tradeshows"
        pdvc.catName = "Trade Shows"
    }
    
    setting.leftMenuClosed()
    setting.navigationController?.pushViewController(pdvc, animated: true)
 }
}
else if menuItems[indexPath.row] == "Upload and Event" {
    
        self.closeAnimation()
        self.redirectToLogin()
    }
else if menuItems[indexPath.row] == "Break Your Own news" {
    
        self.closeAnimation()
        self.redirectToLogin()
    }
   else {
    
        Utility.displayAlert(title: kAppTitle, message: "Coming soon...")
    }
        
  }
       
}

    
    @IBOutlet weak var image: UIImageView!
    // var image:UIImage?
    init(frame: CGRect ,mArray:[String],fromController:String)
      {
        super.init(frame: frame)
        self.xibSetup()
        self.logoutButton.isHidden = true
   // self.menuArray = mArray
        print("frompreviouscontrolller = \(fromController)")
        menuItems = mArray
        print("catecogry list = \(categoryList)")
        self.fromPreviousController = fromController
    
        self.userImageView.layer.borderWidth = 1
        self.userImageView.layer.masksToBounds = false
        self.userImageView.layer.borderColor = UIColor.gray.cgColor
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height/2
        self.userImageView.clipsToBounds = true
        
       // self.logoutButton.isHidden = true
    
        Utility.getUserDetails()
        
        if Int(UserDetails.sharedInstance.email.count) > 0 {
            
            self.signupCardHeight.constant = 200
            self.userProfileCard.isHidden = false
            self.userLoginSignupCard.isHidden = true
            self.lbl_username.text = UserDetails.sharedInstance.firstName + " " + UserDetails.sharedInstance.lastName
            self.lbl_email.text = UserDetails.sharedInstance.email
            self.userImageView.sd_setImage(with: URL(string: UserDetails.sharedInstance.image), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            
            if (fromController == "Profile") {
                
                if UserDetails.sharedInstance.accountType == "NormalLogin" {
                    
                    menuItems = self.itemarray
                }
                else {
                    
                    menuItems = self.itemarrySocialLogin
                }
             
            }
            else {
                
                menuItems = leftMenuItems
                menuItems = menuItems + ["Logout"]
            }
            
        }
    else {
            
            self.signupCardHeight.constant = 120
            self.userProfileCard.isHidden = true
            self.userLoginSignupCard.isHidden = false
        }
    
        singOutNotification.addObserver(self, selector: #selector(userLogout), name: Notification.Name("userLogout"), object: nil)
    
    }
    
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            self.xibSetup()
        }
    
    func xibSetup() {
            
        let view: UIView? = loadViewFromNib()
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view?.backgroundColor = UIColor.white.withAlphaComponent(0.01)
            
        //  tableView.register(UINib(nibName: "SubscribeCell", bundle: nil), forCellReuseIdentifier: "subscribeCell")
            
        tableView.register(UINib(nibName: "BlankTableViewCell", bundle: nil), forCellReuseIdentifier: "BlankTableViewCell")
            
        self.tableView.tableFooterView = UIView()
        if let view = view {
            
                    addSubview(view)
          }
           
         print("catecogry list = \(categoryList)")

    }
    
    @objc func userLogout() {
        
        print("get message using notification")
            
        Utility.getUserDetails()
        if UserDetails.sharedInstance.email.count > 0 {
            
            self.userProfileCard.isHidden = false
            self.userLoginSignupCard.isHidden = true
            self.lbl_username.text = UserDetails.sharedInstance.name
            self.lbl_email.text = UserDetails.sharedInstance.email
        }
        else{
            
            self.userProfileCard.isHidden = true
            self.userLoginSignupCard.isHidden = false
        }
        
    }
        
    @objc func swipeleft(gestureRecognizer:UIGestureRecognizer) {
//     self.closePopover()
            self.closeAnimation()
        }
    
    func loadViewFromNib() -> UIView? {
        
            let nibs = Bundle.main.loadNibNamed("PopoverView", owner: self, options: nil)
            return nibs?[0] as? UIView
            
        }
    
    func logoutButtonTapped() {
        
        self.closeAnimation()
        
        let dialogMessage = UIAlertController(title: kAppTitle, message: "Are you sure, you want to logout?", preferredStyle: .alert)
                       
        // Create OK button with action handler
        let ok = UIAlertAction(title: "Logout", style: .default, handler: { (action) -> Void in
        print("Ok button tapped")
                       
        Utility.clearUserDetails()
        self.signupCardHeight.constant = 120
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("userLogout"), object: nil)
                    
        let updateProfilePicNotification = NotificationCenter.default
        updateProfilePicNotification.post(name:Notification.Name("userUpdateProfilePic"),object: nil)
                   
        let ncsetting =  NotificationCenter.default
        ncsetting.post(name: Notification.Name("userLogoutSetting"), object: nil)
              
        GIDSignIn.sharedInstance()?.signOut()
                  
        let loginManager = LoginManager()
        loginManager.logOut()
            
        self.menuItems = leftMenuItems
                    
        if self.fromPreviousController  == "Profile" {
//                    self.tableView.reloadData()
                Utility.tabBarAsRootViewController()
            }
        else {
                   // Utility.clearUserDetails()
                  //  self.signupCardHeight.constant = 120
                //   let nc = NotificationCenter.default
                //     nc.post(name: Notification.Name("userLogout"), object: nil)
                
//                   let updateProfilePicNotification = NotificationCenter.default
//                   updateProfilePicNotification.post(name:Notification.Name("userUpdateProfilePic"),object: nil)
                                         
//                    let ncsetting =  NotificationCenter.default
//                    nc.post(name: Notification.Name("userLogoutSetting"), object: nil)
                                                          
//                    GIDSignIn.sharedInstance()?.signOut()
                                                           
                Utility.displayAlert(title: kAppTitle, message: "You Logout Successfully.")
                                                        
//                    let loginManager = LoginManager()
//                    loginManager.logOut()
                                      
//                    self.menuItems = leftMenuItems
                self.tableView.reloadData()
                
                    }
                   
                })
                       
// Create Cancel button with action handlder

    let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
    }
                       
//Add OK and Cancel button to dialog message
    
    dialogMessage.addAction(ok)
    dialogMessage.addAction(cancel)
                       
// Present dialog message to user
                      
                       
    if (self.delegate as AnyObject?) is ViewController {
        
        let viewControllerObj = (self.delegate as! ViewController)
        viewControllerObj.present(dialogMessage, animated: true, completion: nil)

    }
    else if (self.delegate as AnyObject?) is PopularNewsViewController {
        
        let popular = (self.delegate as! PopularNewsViewController)
        popular.present(dialogMessage, animated: true, completion: nil)
    }
    else if (self.delegate as AnyObject?) is BookMarkVC {
        
        let bookmark = (self.delegate as! BookMarkVC)
        bookmark.present(dialogMessage, animated: true, completion: nil)
    }
         
    else if (self.delegate as AnyObject?) is VideosVC {
               
        let video = (self.delegate as! VideosVC)
        video.present(dialogMessage, animated: true, completion: nil)
    }
    else if (self.delegate as AnyObject?) is SettingVC {
        
        let setting = (self.delegate as! SettingVC)
        setting.present(dialogMessage, animated: true, completion: nil)
    }
    else if (self.delegate as AnyObject?) is ProfileViewController {
        
        let setting = (self.delegate as! ProfileViewController)
        setting.present(dialogMessage, animated: true, completion: nil)
    }
  }

    func closePopover(){
            
        }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            
        if touch.view?.isDescendant(of: self.tableView) ?? true {
            
                    return false
            }
            
        return true

    }

    
    func closeAnimation() {
           
        CATransaction.begin()
        CATransaction.setCompletionBlock {
        self.closePopover()
        if (self.delegate as AnyObject?) is ViewController {
            
            (self.delegate as! ViewController).leftMenuClosed()
        }
        else if (self.delegate as AnyObject?) is PopularNewsViewController {
            
            (self.delegate as! PopularNewsViewController).leftMenuClosed()
        }
        else if (self.delegate as AnyObject?) is BookMarkVC {
            
            (self.delegate as! BookMarkVC).leftMenuClosed()
        }
        else if (self.delegate as AnyObject?) is VideosVC {
            
            (self.delegate as! VideosVC).leftMenuClosed()
        }
        else if (self.delegate as AnyObject?) is SettingVC {
            
            (self.delegate as! SettingVC).leftMenuClosed()
        }
        else if (self.delegate as AnyObject?) is ProfileViewController  {
            
            (self.delegate as! ProfileViewController).leftMenuClosed()
        }
            
    }
        
    let transition:CATransition = CATransition.init()
    transition.duration = 0.5;
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromRight
    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.layer.add(transition, forKey: nil)
    CATransaction.commit()
    self.layer.isHidden = true
        
 }
    
    @IBAction func actionLoginSignup(_ sender: Any) {
        
    if (self.delegate as AnyObject?) is ViewController {
                
        let viewControllerObj = (self.delegate as! ViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        viewControllerObj.leftMenuClosed()
        viewControllerObj.navigationController?.pushViewController(pdvc, animated: true)

    }
    else if (self.delegate as AnyObject?) is PopularNewsViewController  {
        
        let popular = (self.delegate as! PopularNewsViewController)
        let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        popular.leftMenuClosed()
        popular.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is BookMarkVC {
        
        let bookmark = (self.delegate as! BookMarkVC)
        let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        bookmark.leftMenuClosed()
        bookmark.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is VideosVC   {
        
        let video = (self.delegate as! VideosVC)
        let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        video.leftMenuClosed()
        video.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is SettingVC  {
        
        let setting = (self.delegate as! SettingVC)
        let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        setting.leftMenuClosed()
        setting.navigationController?.pushViewController(pdvc, animated: true)
    }
        
 }
    
func redirectToLogin()  {
        
    if (self.delegate as AnyObject?) is ViewController  {
        
                       
        let viewControllerObj = (self.delegate as! ViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        viewControllerObj.leftMenuClosed()
        viewControllerObj.navigationController?.pushViewController(pdvc, animated: true)

    }
    else if (self.delegate as AnyObject?) is PopularNewsViewController  {
        
        let popular = (self.delegate as! PopularNewsViewController)
        let pdvc = popular.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        popular.leftMenuClosed()
        popular.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is BookMarkVC {
        
        let bookmark = (self.delegate as! BookMarkVC)
        let pdvc = bookmark.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        bookmark.leftMenuClosed()
        bookmark.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is VideosVC   {
        
        let video = (self.delegate as! VideosVC)
        let pdvc = video.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        video.leftMenuClosed()
        video.navigationController?.pushViewController(pdvc, animated: true)
    }
    else if (self.delegate as AnyObject?) is SettingVC  {
        
        let setting = (self.delegate as! SettingVC)
        let pdvc = setting.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.closeAnimation()
        setting.leftMenuClosed()
        setting.navigationController?.pushViewController(pdvc, animated: true)
    }
 }
    
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        
            self.closeAnimation()
    }
    
    @IBAction func actionFacebookClick(_ sender: Any) {
        
    if let url = URL(string: kFacebookLinkPath) {
        
            UIApplication.shared.open(url)
            self.closeAnimation()
        }
        
    }
    
    @IBAction func actionTwitterClick(_ sender: Any) {
        
    if let url = URL(string: kTwitterLinkPath)  {
        
            UIApplication.shared.open(url)
            self.closeAnimation()
        }
        
    }
    @IBAction func actionInstagramClick(_ sender: Any){
        
    if let url = URL(string: kInstagramLinkPath)    {
            
            UIApplication.shared.open(url)
            self.closeAnimation()
        }
        
    }
    @IBAction func actionLinkedinClick(_ sender: Any){
        
    if let url = URL(string: kLinkedinLinkPath) {
        
            UIApplication.shared.open(url)
            self.closeAnimation()
        }
        
    }
    @IBAction func actionYoutubeClick(_ sender: Any){
        
    if let url = URL(string: kYoutubeLinkPath)  {
        
            UIApplication.shared.open(url)
             self.closeAnimation()
        }
    }
    
    @IBAction func actionShowProfile(_ sender: Any) {
        
        let viewControllerObj = (self.delegate as! UIViewController)
        let pdvc = viewControllerObj.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.delegate?.navigationController?.pushViewController(pdvc, animated: true)
        self.closeAnimation()
    }
    
}
