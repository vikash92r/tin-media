//
//  Utility.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 06/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Reachability
import Alamofire

class Utility
{
   class func saveUserDetails(){
    
    if UserDetails.sharedInstance.email.count > 0 {
        
    let defaults =  UserDefaults.standard
                
    defaults.set(UserDetails.sharedInstance.name, forKey: "name")
    defaults.set(UserDetails.sharedInstance.firstName, forKey: "firstName")
    defaults.set(UserDetails.sharedInstance.lastName,forKey: "lastName")
    defaults.set(UserDetails.sharedInstance.email,forKey:"email")
    defaults.set(UserDetails.sharedInstance.company, forKey: "company")
    defaults.set(UserDetails.sharedInstance.designation, forKey: "designation")
    defaults.set(UserDetails.sharedInstance.title, forKey: "title")
    defaults.set(UserDetails.sharedInstance.image, forKey: "image")
    defaults.set(UserDetails.sharedInstance.userId, forKey: "ID")
    defaults.set(UserDetails.sharedInstance.usercontact,forKey: "usercontact")
    defaults.set(UserDetails.sharedInstance.password,forKey:"password")
    defaults.set(UserDetails.sharedInstance.userdata,forKey: "userdata")
    defaults.set(UserDetails.sharedInstance.bannerUrl,forKey: "bannerUrl")
    defaults.set(UserDetails.sharedInstance.bannerUrl,forKey: "bannerAddUrl")
    defaults.set(UserDetails.sharedInstance.accountType, forKey: "accountType")
               
    defaults.synchronize()
        
    }
 }
    
    class func getUserDetails() {
        
    let defaults = UserDefaults.standard
    let userId  = defaults.integer(forKey: "ID")
    let email = defaults.string(forKey: "email")
        
    if email?.count ?? 0 > 0 {
            
        UserDetails.sharedInstance.name = defaults.string(forKey: "Name") ?? ""
        UserDetails.sharedInstance.firstName = defaults.string(forKey: "firstName")!
        UserDetails.sharedInstance.lastName = defaults.string(forKey: "lastName")!
        UserDetails.sharedInstance.title = defaults.string(forKey: "title") ?? ""
        UserDetails.sharedInstance.company = defaults.string(forKey: "company")!
        UserDetails.sharedInstance.designation = defaults.string(forKey: "designation")!
        UserDetails.sharedInstance.email = defaults.string(forKey: "email")!
        UserDetails.sharedInstance.usercontact = defaults.string(forKey: "usercontact")!
        UserDetails.sharedInstance.password = defaults.string(forKey: "password")!
        UserDetails.sharedInstance.userdata = defaults.string(forKey: "userdata") ?? ""
        UserDetails.sharedInstance.image = defaults.string(forKey: "image") ?? ""
        UserDetails.sharedInstance.userId = defaults.integer(forKey: "ID") as NSNumber
        UserDetails.sharedInstance.bannerUrl = defaults.string(forKey: "bannerUrl") ?? ""
        UserDetails.sharedInstance.bannerAddUrl = defaults.string(forKey: "bannerAddUrl") ?? ""
        UserDetails.sharedInstance.accountType = defaults.string(forKey: "accountType") ?? ""
       
    }
 }
    
 class func clearUserDetails() {
        
    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    dictionary.keys.forEach { key in
    defaults.removeObject(forKey: key)
    }
        
    UserDetails.sharedInstance.userdata = ""
    UserDetails.sharedInstance.title = ""
    UserDetails.sharedInstance.firstName = ""
    UserDetails.sharedInstance.lastName = ""
    UserDetails.sharedInstance.email = ""
    UserDetails.sharedInstance.company = ""
    UserDetails.sharedInstance.designation = ""
    UserDetails.sharedInstance.usercontact = ""
    UserDetails.sharedInstance.password = ""
    UserDetails.sharedInstance.name = ""
    UserDetails.sharedInstance.image = ""
    UserDetails.sharedInstance.userId = 0
    
    UserDetails.sharedInstance.accountType = ""
//   UserDetails.sharedInstance.bannerUrl = ""
 
 }
    
    
class  func getTopNews(dict:JSON)->[TopNewsModel] {
        
    var topNewModels = [TopNewsModel]()
    guard let dictObject = dict["top"].arrayObject else { return topNewModels }
            
    for object  in dictObject as! [[String:String]] {
            
    let topNewModel = TopNewsModel()
    let dictObject = object
    topNewModel.id = dictObject["id"]
    topNewModel.news_heading = dictObject["news_heading"]
    topNewModel.image = dictObject["image"]
    topNewModel.days_ago = dictObject["days_ago"]
    topNewModel.categoryId = dictObject["categoryId"]
    topNewModels.append(topNewModel)
    
    }
    
    return topNewModels
    
  }
    
    class  func getTopNewsModels(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["top"].arrayObject else { return topNewModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image = dictObject["image"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.from = dictObject["from"]
    newsmodel.modelType = "TopNews"
    topNewModels.append(newsmodel)
    
    }
    
    return topNewModels

 }
    
    class  func getNewByParentId(dict:JSON)->(newsModelArray:[[NewsModel]],sectionKeyArray:[String]) {
        
    var NewModels = [[NewsModel]]()
    guard let dictObject = dict["allnews"].dictionary else { return (NewModels,[String]()) }
    
    let allnews = dict["allnews"].dictionary
    let m = allnews?.keys
    let sectionKeyArray = [String](allnews!.keys )
      
    for obj in m! {
        
    var newsModelses = [NewsModel]()
//            var news = dictObject[obj]!
    guard let dictObjectNews = dictObject[obj]!.arrayObject else { return (NewModels,[String]() )}
    print("my key = \(m!)")
           
    for mNews  in dictObjectNews as! [[String:String]] {
                
    let newsmodel = NewsModel()
    let dictObject = mNews
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image = dictObject["image"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.from = dictObject["from"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.modelType = "getByParentId"
    newsModelses.append(newsmodel)
                
    }
          //  sectionKeyArray.append(m)
    NewModels.append(newsModelses)
    
    }
    return (NewModels,sectionKeyArray)

  }
    
    class  func getCategories(dict:JSON)->[CategoryModel] {
        
    var categoryModels = [CategoryModel]()
    guard let categoryArray = dict["categories"].arrayObject else { return categoryModels }
    
    for category  in categoryArray as! [[String:String]] {
    
    var cat = CategoryModel()
    print(category)
    cat.cat_name = category["cat_name"] ?? ""
    cat.categoryId = category["categoryId"] ?? ""
    categoryModels.append(cat)
    }
        
    return categoryModels
}
    
    class func  getTopics(dict:JSON)->[TopicModel] {
        
    var topicsModels = [TopicModel]()
    guard let topicArray = dict["topics"].dictionary else { return topicsModels }
        
    for dict in topicArray as Dictionary {
            
    print("key = \(dict.key)")
    print("value = \(dict.value.stringValue)")
    let topic = TopicModel()
    topic.key = dict.key
    topic.value = dict.value.stringValue
    topicsModels.append(topic)
    }
    
    return topicsModels
  }
    
    
    class  func getHomeCombineCategory(dict:JSON)->(newsModelArray:[[NewsModel]],sectionKeyArray:[String]) {
        
    var NewModels = [[NewsModel]]()
    guard let dictObject = dict["allnews"].dictionary else { return (NewModels,[String]()) }
    
    let allnews = dict["allnews"].dictionary
    let m = allnews?.keys
    let sectionKeyArray = [String](allnews!.keys )
        
    for obj in m! {
                
    var newsModelses = [NewsModel]()
    //            var news = dictObject[obj]!
    guard let dictObjectNews = dictObject[obj]!.arrayObject else { return (NewModels,[String]() )}
    print("my key = \(m!)")
               
    for mNews  in dictObjectNews as! [[String:Any]] {
                
    let newsmodel = NewsModel()
    let dictObject = mNews
    newsmodel.id = dictObject["id"] as? String
    newsmodel.news_heading = dictObject["news_heading"] as? String
    newsmodel.image = dictObject["image"] as? String
    newsmodel.days_ago = dictObject["days_ago"] as? String
    newsmodel.from = dictObject["from"] as? String
    newsmodel.categoryId = "\(dictObject["categoryId"] as! NSNumber)"
    newsmodel.modelType = "HomeCombine"
    newsModelses.append(newsmodel)
             
    }
              //  sectionKeyArray.append(m)
    NewModels.append(newsModelses)
    }
    return (NewModels,sectionKeyArray)
            
}
        
    
    class  func getVediosDetais(dict:JSON) -> (vediosDetail:[NewsModel],vediosList:[NewsModel]) {
        
    var vedioDetail = [String:[NewsModel]]()
        
    var vediosDetails = [NewsModel]()

    guard let dictObject = dict["videodetails"].arrayObject else { return (vediosDetails,vediosDetails) }
        
    for object  in dictObject as! [[String:String]] {
            
    let newsmodel = NewsModel()
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image_url = dictObject["image_url"]
    newsmodel.video_url = dictObject["video_url"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.from = dictObject["from"]
            
    let htmlDataDescription = NSString(string: dictObject["description"]!).data(using: String.Encoding.utf8.rawValue)
          let attributedStringDescription = try! NSAttributedString(data: htmlDataDescription!,options: options,documentAttributes: nil)
            
    newsmodel.description = attributedStringDescription
            
    vediosDetails.append(newsmodel)
            
    }
        
    var vediosListModels = [NewsModel]()
        
    guard let dictObjectVedio = dict["morevideos"].arrayObject else { return (vediosDetails,vediosDetails)}
        
    for object  in dictObjectVedio as! [[String:String]] {
            
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image_url = dictObject["image_url"]
    newsmodel.video_url = dictObject["video_url"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.from = dictObject["from"]
    vediosListModels.append(newsmodel)
           
    }
        
    vedioDetail["videodetails"]  = vediosDetails
    vedioDetail["morevideos"]  = vediosListModels
        
    return (vediosDetails,vediosListModels)
    
    }
    
  class  func getTopNewsRecents(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["recent"].arrayObject else { return topNewModels }
        
    for object  in dictObject as! [[String:String]] {
        
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image = dictObject["image"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.from = dictObject["from"]
    newsmodel.modelType = "RecentNews"
    topNewModels.append(newsmodel)
    
    }
       
    return topNewModels
        
 }
    
    class  func getPopularNews(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["popular"].arrayObject else { return topNewModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.news_heading = dictObject["news_heading"]
    newsmodel.image = dictObject["image"]
    newsmodel.days_ago = dictObject["days_ago"]
    newsmodel.categoryId = dictObject["categoryId"]
    newsmodel.from = dictObject["from"]
    newsmodel.modelType = "RecentNews"
    topNewModels.append(newsmodel)
    
    }
       
    return topNewModels
        
 }
    
    class  func getEvents(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["events"].arrayObject else { return topNewModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.event_cat = dictObject["event_cat"]
    newsmodel.title = dictObject["title"]
    newsmodel.image = dictObject["image"]
    newsmodel.location = dictObject["location"]
    newsmodel.date_from = dictObject["date_from"]
    newsmodel.date_to = dictObject["date_to"]
    newsmodel.news_url = dictObject["news_url"]
            
            //news_url
            
    let htmlData = NSString(string: dictObject["description"] ?? "").data(using: String.Encoding.utf8.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
            
    newsmodel.description =  attributedString
    newsmodel.modelType = "Events"
    topNewModels.append(newsmodel)
            
    }
       
    return topNewModels
    
 }
    
    
    class  func getEventDetail(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["details"].arrayObject else { return topNewModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.event_cat = dictObject["event_cat"]
    newsmodel.title = dictObject["title"]
    newsmodel.image = dictObject["image"]
    newsmodel.location = dictObject["location"]
    newsmodel.date_from = dictObject["date_from"]
    newsmodel.date_to = dictObject["date_to"]
    newsmodel.news_url = dictObject["news_url"]
            
            //news_url
            
    let htmlData = NSString(string: dictObject["description"] ?? "").data(using: String.Encoding.utf8.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
            
    newsmodel.description =  attributedString
    topNewModels.append(newsmodel)
    
    }
        return topNewModels
        
 }
    
    
    class  func getAllEvents(dict:JSON)->[NewsModel] {
        
    var topNewModels = [NewsModel]()
    guard let dictObject = dict["allevents"].arrayObject else { return topNewModels }
           
    for object  in dictObject as! [[String:String]] {
            
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
               
    let newsmodel = NewsModel()
    let dictObject = object
    newsmodel.id = dictObject["id"]
    newsmodel.event_cat = dictObject["event_cat"]
    newsmodel.title = dictObject["title"]
    newsmodel.image = dictObject["image"]
    newsmodel.location = dictObject["location"]
    newsmodel.date_from = dictObject["date_from"]
    newsmodel.date_to = dictObject["date_to"]
    newsmodel.news_url = dictObject["news_url"]
               
               //news_url
               
    let htmlData = NSString(string: dictObject["description"] ?? "").data(using: String.Encoding.utf8.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
            
    newsmodel.description =  attributedString
    newsmodel.modelType = "Events"
    topNewModels.append(newsmodel)
               
    }
          
    return topNewModels
           
 }
    
    
    
    class func getNewsDetails(dict:JSON)->[NewsDetailModel] {
        
    var newsDetailModels = [NewsDetailModel]()
    guard let dictObject = dict["details"].arrayObject  else { return newsDetailModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

    let newsModel = NewsDetailModel()
    newsModel.id = object["id"]
//            newsModel.news_description = object["news_description"] as? String
    newsModel.news_heading = object["news_heading"]
    newsModel.image = object["image"]
    newsModel.days_ago = object["days_ago"]
    newsModel.from = object["from"]
    newsModel.news_url = object["news_url"]
            
    let htmlData = NSString(string: object["news_description"]!).data(using: String.Encoding.utf8.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
          
    newsModel.news_description = attributedString
           
    newsDetailModels.append(newsModel)
    
    }
        
    return newsDetailModels
        
  }
    
    class func getMyNewsDetails(dict:JSON)->[NewsDetailModel] {
            
    var newsDetailModels = [NewsDetailModel]()
    guard let dictObject = dict["details"].arrayObject  else { return newsDetailModels }
            
    for object  in dictObject as! [[String:Any]] {
            
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

    let newsModel = NewsDetailModel()
    newsModel.id = object["id"] as? String
    //            newsModel.news_description = object["news_description"] as? String
    newsModel.news_heading = object["news_heading"] as? String
    newsModel.image = object["image"] as? String
    newsModel.days_ago = object["days_ago"] as? String
    newsModel.from = object["from"] as? String
    newsModel.news_url = object["news_url"] as? String
    newsModel.location = object["location"] as? String
    newsModel.news_cat = object["news_cat"] as? String
    newsModel.news_topic = object["news_topic"] as? String
                
    let htmlData = NSString(string: object["news_description"] as? String ?? "").data(using: String.Encoding.utf8.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
              
    newsModel.news_description = attributedString
               
    newsDetailModels.append(newsModel)
    }
            
    return newsDetailModels
            
 }
        
    class func getGalleryModels(dict:JSON)-> [GalleryModel] {
           
        var galleryModels = [GalleryModel]()
           
       guard let dictObject = dict["list"].arrayObject else { return galleryModels }

       for object in dictObject as! [[String:String]] {
        let galleryModel = GalleryModel()
       print(" \(String(describing: object["image"]))")
        galleryModel.gallery_id = object["gallery_id"]
        galleryModel.event = object["event"]
        galleryModel.image = object["image"]
        galleryModel.date = object["date"]
        galleryModel.location = object["location"]
        
        galleryModels.append(galleryModel)
       
       }
           
       return galleryModels
           
    }
    
    class func getHomeTopBannerModel(dict:JSON)-> homeTopBannerModel {
        
    let homeTopBannerModels = homeTopBannerModel()
        
    guard let dictObject = dict["banner"].arrayObject else { return homeTopBannerModels }

    for object in dictObject as! [[String:String]] {
            
    print(" \(String(describing: object["image"]))")
    homeTopBannerModels.banner.append(object["image"]!)
    homeTopBannerModels.urlPath.append(object["url"]!)
    
    }
        
    return homeTopBannerModels
        
 }
    
    
    class func getHomeMidleAdd(dict:JSON)-> HomeMidleAddModel {
        
    let homeMidleAddModel = HomeMidleAddModel()
           
    guard let dictObject = dict["add"].arrayObject else { return homeMidleAddModel }

    for object in dictObject as! [[String:String]] {
            
    print("image = \(String(describing: object["image"]))")
    print("add_url = \(String(describing: object["add_url"]))")
    print("addType = \(String(describing: object["addType"]))")
    homeMidleAddModel.banner.append(object["image"]!)
    homeMidleAddModel.add_url.append(object["add_url"] ?? "")
    homeMidleAddModel.addType.append(object["addType"] ?? "")
            
    }
           
    return homeMidleAddModel
        
}
    
    class func getMenuList(dict:JSON) -> [MenuModel] {
        
    var menuModels = [MenuModel]()
    guard let dictObject = dict["result"].arrayObject else { return menuModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let menuModel = MenuModel()
    let dictObject = object
    menuModel.id = dictObject["id"]
    menuModel.cat_name = dictObject["cat_name"]
                  
    menuModels.append(menuModel)
    
    }
        
    return menuModels
        
 }
    
    
    class func getNewsList(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["allnews"].arrayObject else { return newsModels }
    
    for object  in dictObject as! [[String:String]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"]
    newsModel.days_ago = dictObject["days_ago"]
    newsModel.news_heading = dictObject["news_heading"]
    newsModel.image = dictObject["image"]
    newsModel.from   = dictObject["from"]
    newsModel.date_from = dictObject["date_from"] ?? ""
    newsModel.location = dictObject["location"] ?? ""
            
    newsModels.append(newsModel)
                 
    }
    
    return newsModels
        
 }
    
    class func getPagesData(dict:JSON) -> [PagesDataModel] {
        
    var pageDataModels = [PagesDataModel]()
    guard let dictObject = dict["data"].arrayObject else { return pageDataModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let pageModel = PagesDataModel()
    let dictObject = object
               
    pageModel.page_content = dictObject["page_content"]
    pageModel.page_title = dictObject["page_title"]
    pageDataModels.append(pageModel)
        
    }
        
    return pageDataModels
        
 }
    
    
    class func getMyNewsList(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["allnews"].arrayObject else { return newsModels }
           
    for object  in dictObject as! [[String:Any]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"] as? String
    newsModel.days_ago = dictObject["days_ago"]  as? String
    newsModel.news_heading = dictObject["news_heading"]  as? String
    newsModel.image = dictObject["image"]  as? String
    newsModel.from   = dictObject["from"]  as? String
    newsModel.date_from = dictObject["date_from"] as? String
    newsModel.location = dictObject["location"] as? String
    newsModel.created_date = dictObject["created_date"] as? String

    newsModels.append(newsModel)

    }
       
    return newsModels
           
 }
    
    class func getSearchNewsList(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["results"].arrayObject else { return newsModels }
        
    for object  in dictObject as! [[String:String]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"]
    newsModel.days_ago = dictObject["days_ago"]
    newsModel.news_heading = dictObject["news_heading"]
    newsModel.image = dictObject["image"]
    newsModel.from   = dictObject["from"]
    newsModel.date_from = dictObject["date_from"] ?? ""
    newsModel.location = dictObject["location"] ?? ""
    newsModels.append(newsModel)
                
    }
    
    return newsModels
        
 }
    
    
    class func getNewsListByMustWatch(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["mustwatch"].arrayObject else { return newsModels }
           
    for object  in dictObject as! [[String:String]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"]
    newsModel.days_ago = dictObject["days_ago"]
    newsModel.news_heading = dictObject["news_heading"]
    newsModel.image = dictObject["image"]
    newsModel.image_url = dictObject["image_url"]
    newsModel.modelType = "MustWatch"
               
    newsModels.append(newsModel)
    }
       
    return newsModels
        
 }
    
    class func getNewsBookmarkedList(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["bookmarked"].arrayObject else { return newsModels }
             
    for object  in dictObject as! [[String:String]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"]
    newsModel.days_ago = dictObject["days_ago"]
    newsModel.news_heading = dictObject["news_heading"]
    newsModel.image = dictObject["image"]
    newsModel.from   = dictObject["from"]
    newsModel.image_url = dictObject["image_url"]
                 
    newsModels.append(newsModel)
                      
    }
         
    return newsModels
        
 }
    
    class func getNewsBookmarkedDetail(dict:JSON) -> [NewsModel] {
        
    var newsModels = [NewsModel]()
    guard let dictObject = dict["bookmarked"].arrayObject else { return newsModels }
               
    for object  in dictObject as! [[String:String]] {
            
    let newsModel = NewsModel()
    let dictObject = object
    newsModel.id = dictObject["id"]
    newsModel.days_ago = dictObject["days_ago"]
    newsModel.news_heading = dictObject["news_heading"]
    newsModel.image = dictObject["image"]
    newsModel.from   = dictObject["from"]
    newsModel.image_url = dictObject["image_url"]
                        
    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
    let htmlData = NSString(string: dictObject["description"] ?? "").data(using: String.Encoding.utf8.rawValue)

                        
    let attributedString = try! NSAttributedString(data: htmlData!,options: options,documentAttributes: nil)
    newsModel.description =  attributedString
                   
    newsModels.append(newsModel)
                    
    }
           
        return newsModels
            
 }
      
    
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func getAttributedString(color:UIColor ,colorText:String,fullString:String) -> NSAttributedString {
        
    let lblDayString = fullString
    let strNumber: NSString = colorText as NSString
    let range = (strNumber).range(of: colorText)
    let attribute = NSMutableAttributedString.init(string: lblDayString)
    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
    return attribute
        
 }
    
    class func customAlertControlView(message:String,title:String)  {
        
    let alert = UIAlertController(title: kNewtworkNotAvailable, message: nil, preferredStyle: .alert)

    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

    if let name = alert.textFields?.first?.text {
            print("Your name: \(name)")
      }
            
     }))
    
 }
    
    static func displayAlert(title: String, message: String) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(defaultAction)

        guard let viewController = UIApplication.shared.keyWindow?.rootViewController else {
            fatalError("keyWindow has no rootViewController")
            return
        }

        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func showCustomAlert() {
        
        let alert = UIAlertController(title: "This match is closed", message: nil, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action -> Void in
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateListOnPullToRefresh"), object: nil)
            if let navigationVC = APPDELEGATE.window??.rootViewController as? UINavigationController{
                navigationVC.popToRootViewController(animated: true)
            }
        }))
        APPDELEGATE.window??.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func makeLoginAsRootViewController() {
        
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        let navVC = UINavigationController(rootViewController: loginViewController)
        navVC.isNavigationBarHidden = false
        UIApplication.shared.keyWindow?.rootViewController = navVC
    
    }
    
    class func tabBarAsRootViewController()
    {
        
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
       let loginViewController = storyboard.instantiateViewController(withIdentifier: "HomeTabBarViewController") as! HomeTabBarViewController
       let navVC = UINavigationController(rootViewController: loginViewController)
       
       navVC.isNavigationBarHidden = true
       UIApplication.shared.keyWindow?.rootViewController = navVC
       
    }
    
    class func profileAsRootViewController() {
        
       let storyboard = UIStoryboard(name: "Main", bundle: nil)
       let loginViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
       let navVC = UINavigationController(rootViewController: loginViewController)
       
       navVC.isNavigationBarHidden = true
       UIApplication.shared.keyWindow?.rootViewController = navVC
       
    }
    
    class func formatedDateStringFromString(dateString:String) -> String {
        
        if dateString.count > 0 {
            
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let date = formatter.date(from: dateString ?? "")
                  
        formatter.dateFormat = "dd MMM yyyy"
        let dateStr = formatter.string(from: date!)
        return dateStr
        }
        
        return ""
    }
    
    
    class func stringAfterTrim(name:String)->String {
        
           let str = name.trimmingCharacters(in: NSCharacterSet.whitespaces)
           return str
       }
    
    class func isNotValidEmail(email:String) -> Bool {
        
         let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
           
         let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
         return !emailTest.evaluate(with: email)
       }
    
    class func performMultipartRequest(urlString: String?, fileName: String, params: Parameters?, imageDataArray: Array<Data>?, accessToken: String?, completion completionBlock: @escaping ( _ result: [String: JSON]?,  _ error: Error?) -> Void) {
        
        var apiToken = accessToken
        if apiToken == nil {
            apiToken = ""
        }
        
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
        

        let headers: HTTPHeaders = [
            "accessToken": apiToken!,
            "loginId": UserDetails.sharedInstance.userId.stringValue,
            "platform": "iOS",
            "osVersion": osVersion,
            "versionCode": appVersion,
            "versionName": buildVersion
        ]
        
        print(params!)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageDataArray != nil{
                var count = 0
                for data in imageDataArray!{
                    multipartFormData.append(data, withName: fileName, fileName: "profile_image.jpg", mimeType: "image/jpeg")
                    count += 1
                }
            }
        }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _ , _ ):
                
//                upload.responseJSON
//                               {
//                                   response in
//                                   print("Response :\(response.result.value)")
//                                let responseJSON = JSON(response.data as AnyObject);
//                                //                        print("responseJSON::", responseJSON)
//                              // let responseJSON = JSON(response.data as AnyObject) as? [String : JSON]
//                                completionBlock(response.result.value! as? [String : JSON],nil)
//                                return
//                           }
//
//                print("result = \(result) ")
//                Utility.tabBarAsRootViewController()
//                break
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                
                    if response.error != nil{
                        completionBlock(nil, response.error)
                        return
                    }

                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        let responseJSON = JSON(response.data as AnyObject);
                        print("responseJSON::", responseJSON)

                        let statusCodeOnResponse = responseJSON.dictionary!["response"]?.string

//                        if !UserDetails.sharedInstance.isLiveServerUpdate{
//                            let serverTimestemp = responseJSON.dictionary!["server_timestamp"]?.stringValue
//                            UserDetails.sharedInstance.isLiveServerUpdate = true
//                            UserDetails.sharedInstance.serverTimeStemp = serverTimestemp ?? "0"
//                        }

                        if statusCodeOnResponse == "200" {

                            if let userDetails = responseJSON.dictionary!["userdata"] {

                                do {
                                
                                    let mdetail = userDetails.arrayObject as! [[String:String]]
                                    print("firstname = \(mdetail[0]["fname"])")
                                    
                                    let firstName = mdetail[0]["fname"]
                                    let title = mdetail[0]["title"]
                                    let usercontact = mdetail[0]["usercontact"]
                                    let last_name = mdetail[0]["last_name"]
                                    let image = mdetail[0]["image"]
                                    let email = mdetail[0]["email"]
                                    let password = mdetail[0]["password"]
                                    let userdata = mdetail[0]["userdata"]
                                    let company =   mdetail[0]["company"]
                                    let designation =  mdetail[0]["designation"]  
                                    
                                    UserDetails.sharedInstance.title = title ?? ""
                                    UserDetails.sharedInstance.firstName = firstName ?? ""
                                    UserDetails.sharedInstance.lastName  = last_name ?? ""
                                    UserDetails.sharedInstance.email = email ?? ""
                                    UserDetails.sharedInstance.image = image ?? ""
                                    UserDetails.sharedInstance.usercontact = usercontact ?? ""
                                    UserDetails.sharedInstance.password = password ?? ""
                                    UserDetails.sharedInstance.userdata = userdata ?? ""
                                    UserDetails.sharedInstance.company = company ?? ""
                                    UserDetails.sharedInstance.designation = designation ?? ""
                                    saveUserDetails()
                                    let results = ["response":userDetails]
                                    completionBlock(results,nil)
                                        
                                
                                   // let data = try userDetails.rawData()
//                                    AppHelper.updateValueInCoreData(urlString: userDetailsUrl, resultData: data)
//                                    if let savedUserJson = AppHelper.getValueFromCoreData(urlString: userDetailsUrl){
//                                        UserDetails.sharedInstance.getAllUserDetails(details: savedUserJson)
//                                    }
                                } catch let error as NSError {
                                    print("Could not save. \(error), \(error.userInfo)")
                                }
                            }

                            completionBlock(responseJSON.dictionary, nil)

                        }
//                        else if statusCodeOnResponse == "801"{
//                            AppHelper.callGenrateUserAccessTokenAPIWithLoader(isNeedToShowLoader: false, completionBlock: {
//
//                                WebServiceHandler.performPOSTRequest(urlString: urlString, andParameters: params, andAcessToken: UserDetails.sharedInstance.accessToken, completion: { (result, error) in
//                                    completionBlock(result, error)
//                                })
//                            })
//                        }
                        else{
                            // let results = ["response":responseJSON.dictionary]
                            completionBlock(responseJSON.dictionary, nil)
                        }
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionBlock(nil, error)
            }
        }
    }
    
    
    class func performMultipartRequestAddEvent(urlString: String?, fileName: String, params: Parameters?, imageDataArray: Array<Data>?, accessToken: String?, completion completionBlock: @escaping ( _ result: [String: JSON]?,  _ error: Error?) -> Void) {
            
            var apiToken = accessToken
            if apiToken == nil {
                apiToken = ""
            }
            
            let osVersion = UIDevice.current.systemVersion
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
            

            let headers: HTTPHeaders = [
                "accessToken": apiToken!,
                "loginId": UserDetails.sharedInstance.userId.stringValue,
                "platform": "iOS",
                "osVersion": osVersion,
                "versionCode": appVersion,
                "versionName": buildVersion
            ]
            
            print(params!)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
             for (key, value) in params! {
                
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
               }
               
             if imageDataArray != nil {
                
                var count = 0
                for data in imageDataArray!{
                 
                  var imageName = fileName
                  if count != 0 {
                    
                            imageName = imageName + String(count) + ".jpg"
                      }
                   else  {
                            imageName = imageName + ".jpg"
                     }
                    
                    multipartFormData.append(data, withName: fileName, fileName: imageName, mimeType: "image/jpeg")
                    count += 1
                  }
              }
            }, usingThreshold: UInt64.init(), to: urlString!, method: .post, headers: headers) { (result) in
               
            switch result {
                
              case .success(let upload, _ , _ ):
            
              upload.responseJSON { response in
              
              print("Succesfully uploaded")
                    
             if response.error != nil {
                
                    completionBlock(nil, response.error)
                    return
                 }

             let statusCode = response.response?.statusCode
             if statusCode == 200{
             
                  let responseJSON = JSON(response.data as AnyObject);
                  print("responseJSON::", responseJSON)

                 let statusCodeOnResponse = responseJSON.dictionary!["response"]?.string

                 if statusCodeOnResponse == "200" {

                    print("data = \(responseJSON)")

                    completionBlock(responseJSON.dictionary, nil)

                   }
                 else {
                         // let results = ["response":responseJSON.dictionary]
                         completionBlock(responseJSON.dictionary, nil)
                      }
                  }
                }
                
                
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    completionBlock(nil, error)
                }
            }
        }
    
   class func getImageSizeFromUrlString(urlString:String) -> CGSize
    {
        var imageSize = CGSize.zero
        
        let imageData = NSData.init(contentsOf: URL(string: urlString)!)
                
        if imageData == nil{
            return CGSize.zero
        }
        
        let addimage = UIImage(data: imageData! as Data)
        let imageHeight = CGFloat(addimage?.size.height ?? 0.0)
        let imageWidth = CGFloat(addimage?.size.width ?? 0.0)
        imageSize = CGSize(width: imageWidth, height: imageHeight)
        
        return imageSize
    }
    
    class func getNoDataLabel(msg:String,view:UIView) -> UILabel {
        let nodataLabel = UILabel()
        
        nodataLabel.text = msg
        nodataLabel.translatesAutoresizingMaskIntoConstraints = false
        nodataLabel.lineBreakMode = .byWordWrapping
        nodataLabel.numberOfLines = 0
        nodataLabel.textAlignment = .center
        return nodataLabel
    }
    
    class func setConstraintForNoDataLabel(view:UIView,noDataLabel:UILabel)
    {
        noDataLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        noDataLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noDataLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
   
}

    
extension UITextField {
    func placeholderColor(_ color: UIColor) {
        var placeholderText = ""
        if self.placeholder != nil {
            
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
    
}


