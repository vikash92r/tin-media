//
//  WebServiceHandler.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 05/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class WebServiceHandler: NSObject {

    class func performPOSTRequestFormDat(urlString:String?, andParmeters params:Parameters?, completion completionBlock:@escaping (_ result : [String:JSON]?, _ error :Error?)->Void)
    {
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
            switch response.result
            {
             case .success:
                            if let value = response.result.value{
                            if let result12 = response.result.value {
                            let jsonObject = result12 as! NSDictionary
                             //  completionBlock(jsonObject as! [String : JSON],nil)
                            }
                                
                            let jsonObject = response.result.value as! JSON
                                completionBlock(response.result.value as! [String:JSON],nil)
                                }
            case .failure(let error):
                                    print(error)
                                    completionBlock(nil,error)
            }
        }
    }

class func performPOSTRequest(urlString: String?, andParameters params: Parameters?, completion completionBlock: @escaping (_ result: [String: JSON]?, _ error: Error?) -> Void) {
       
        Alamofire.request(urlString!, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
        
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in

        }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in

                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let responseJSON = JSON(response.data as AnyObject);
                    completionBlock(responseJSON.dictionary, response.error)
                }
                else{
                    completionBlock(nil, response.error)
                }
        }
    }

}



