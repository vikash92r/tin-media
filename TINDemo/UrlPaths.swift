//
//  UrlPaths.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 05/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

let kBASEURL = "https://www.tin.media/api/"
let kTopNewsPath =  kBASEURL + "topNews"
let kHomeTopBannerPath = kBASEURL + "homeTopbanner"
let kMenuListPath = kBASEURL + "menuList"
let kNewsDetailPath = kBASEURL + "newsDetail/"
let kEventsDetailPath   = kBASEURL +  "eventdetails/"
let kNewsListPath = kBASEURL + "newsList/"
let kNewsListByParentId = kBASEURL + "newsByParent/"
let kRecentNewsPath = kBASEURL + "recentNews"
let kAddBookMarkPath = kBASEURL + "addbookmark/"
let kUpdateProfilePath = kBASEURL + "updateprofile/"

let kViewMorePath = kBASEURL + "viewMore/"
let kEventMorePath = kBASEURL + "moreevents/"
let kEventLeftMenuPath = kBASEURL + "eventsbycategory/"
let kChangePasswordPath = kBASEURL + "changepwd"
let kSearchPath = kBASEURL + "search/"

let kHomeMidleAdd = kBASEURL + "homeMiddleAdd"
let kPopularNewsPath = kBASEURL + "popularNews"
let kSubscribePath = kBASEURL + "subscribenews"
let kNewsCategoryPath  = kBASEURL + "newscategories"
let kEventCategoryPath = kBASEURL + "eventCategories"
let kNewsTopicsPath = kBASEURL + "newstopics"
let kAddNewsPath = kBASEURL + "addnews/"
let kAddEventPath = kBASEURL + "addEvent/"
let kMyNewsPath = kBASEURL + "myNews/"
let kEditEventPath = kBASEURL + "editEvent/"
let kEditNewsPath = kBASEURL + "editNews/"
let kMyEventPath = kBASEURL + "myEvents/"
let kRemoveEventPath = kBASEURL + "removeEvent/"
let kRemoveNewsPath = kBASEURL + "removeNews/"
let kEventDetailPath = kBASEURL + "myEventInfo/"
let kMyNewsDetailPath = kBASEURL + "myNewsInfo/"

let kEventsByCategory = kBASEURL + "eventsbycategory/"

let kViewBookMarkNewsPath = kBASEURL + "bookmarklist/"
let kBookMarkCheckPath = kBASEURL + "isbookmarked/"
let kVediosListPath = kBASEURL + "Videos"
let kVediosDetailsPath = kBASEURL + "videoDetails"
let kMustWatchPath = kBASEURL + "mustWath"
let kEventsPath = kBASEURL + "events"
let KGalleryPath = kBASEURL + "gallery"


let kPrivacyUrl = "https://www.tin.media/pages/privacypolicy"
let kAboutusUrl  = "https://www.tin.media/pages/aboutus"
let kContactUsUrl = "https://www.tin.media/pages/contactus"
let kDatapolicyUrl = "https://www.tin.media/pages/datapolicy"

let kPolicyUrlPath = "https://www.tin.media/api/pagesdata/privacypolicy"
let kCookiePolicyUrl = "https://www.tin.media/api/pagesData/cookiepolicy"
let kCopyrightPolicyUrl  = "https://www.tin.media/api/pagesData/copyrightpolicy"
let kTermsAndConditionUrl = "https://www.tin.media/api/pagesData/subpolicy"
let kAboutUsUrlPath = "https://www.tin.media/api/pagesdata/aboutus"
let kContactUsUrlPath = "https://www.tin.media/api/pagesdata/contactus"
let kDataPolicyUrlPath = "https://www.tin.media/api/pagesdata/datapolicy"
                  

let kPagesDataUrl = kBASEURL + "pagesData/"

let homeCombineCategoryPath = kBASEURL + "homeCombineCategories"

let kRegistrationApi = kBASEURL + "register"
let kLoginApi = kBASEURL + "login"
let kuserDetailApi = kBASEURL + "userDetails/"
let kSocialLoginApi = kBASEURL + "sociallogin"
let kForgotApi = kBASEURL + "forgotpassword"
let kFacebookLinkPath = "https://www.facebook.com/tindotmedia"
let kTwitterLinkPath = "https://twitter.com/media_tin"
let kInstagramLinkPath = "https://www.instagram.com/accounts/login/"
let kLinkedinLinkPath = "https://www.linkedin.com/company/tinmedia"
let kYoutubeLinkPath = "https://www.youtube.com/channel/UCUllquVDjqQYdlO--ujwWsw"
