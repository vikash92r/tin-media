//
//  FacebookLoginManager.swift
//  Ballebaazi
//
//  Created by Vikash Rajput on 5/30/18.
//  Copyright © 2018 Vikash Rajput. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Alamofire
import SwiftyJSON


class FacebookLoginManager: NSObject {
     
    var delegate:UIViewController?
    
    var facebookLoginCompletion = { (result: AnyObject?, error: Error?) -> () in }

    static let sharedInstance = FacebookLoginManager()
    
    func callLoginMangerWithCompletion(completionblock: @escaping(_ result: AnyObject?, _ error: Error?) -> Void) {
        
        let loginManager = LoginManager()
        facebookLoginCompletion = completionblock
        if AccessToken.isCurrentAccessTokenActive {
//            AppHelper.sharedInstance.displaySpinner()
            getFBUserData()
        }
        else{
            weak var weakSelf = self
        
            loginManager.logIn(permissions: [.publicProfile, .email], viewController : nil) { loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                     MBProgressHUD.hideAllHUDs(for: self.delegate?.view, animated: true)
                case .cancelled:
                    print("User cancelled login")
                    MBProgressHUD.hideAllHUDs(for: self.delegate?.view, animated: true)
                case .success( _, _, _):
                    
//                    AppHelper.sharedInstance.displaySpinner()
                    weakSelf!.getFBUserData()
                }
            }
        }
    }
    
    // MARK: Get User Data From Facebook
    
    func getFBUserData(){
        weak var weakSelf = self

        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, first_name,last_name,name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    weakSelf!.callFacebookLoginApi(dict: dict)
                }
                else{
//                    AppHelper.sharedInstance.removeSpinner()
                }
            })
        }
    }
    
    func callFacebookLoginApi(dict :Dictionary<String, AnyObject>)  {
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
//        var deviceToken: String = UserDetails.sharedInstance.deviceToken
//
//        if deviceToken.count == 0 {
//            deviceToken = "1111111111";
//        }
        
        let facebookId = dict["id"] as! String
        let userName = dict["name"] as? String
        let userEmail = dict["email"] as? String ?? ""
        let firstName = dict["first_name"] as? String ?? ""
        let lastName = dict["last_name"] as? String ?? ""
       var imageData  = Array<Data>()
      
        if let imageURL = ((dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                       print(imageURL)
                       let url = URL(string: imageURL)
                       let data = NSData(contentsOf: url!)
                       let image = UIImage(data: data! as Data)
            UserDetails.sharedInstance.image = imageURL
            imageData.append(data as! Data)
                   }
        
        UserDetails.sharedInstance.name = userName!
        UserDetails.sharedInstance.firstName = firstName
        UserDetails.sharedInstance.lastName = lastName
        UserDetails.sharedInstance.email = userEmail
        UserDetails.sharedInstance.accountType = "facebook"
        
        Utility.saveUserDetails()
       let pathUrl = kSocialLoginApi
        
        
        let parameters: Parameters = [
                "api_key" : "8a90436bdef6e286465a918d089a9ca1",
                "fname":firstName,
                "lname": lastName,
                "email":userEmail
                ]
        
        
        Utility.performMultipartRequest(urlString: pathUrl, fileName: "image", params: parameters, imageDataArray: imageData, accessToken: nil) { (response, Error) in
                       if Error == nil
                       {
                       print("image upload")
                        print("facebook login result = \(response)")
                        print("result = \(response)")
                                 switch response?["response"]
                                                        {
                                                        case "200":
                                                                            
                        //                                            Utility.displayAlert(title: kAppTitle, message: "Login successfully updated."
                                                                             Utility.tabBarAsRootViewController()
                                                        case "404":
                                                                    Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                                                        default:
                                                                   
                                                                    break
                                                        }
                        Utility.tabBarAsRootViewController()
                       }
                       
                   }  
    
        
//        UserDetails.sharedInstance.name = userName!
//        UserDetails.sharedInstance.emailAdress = userEmail ?? ""
//
//        let parameters: Parameters = ["option": "register","name": UserDetails.sharedInstance.name, "email": UserDetails.sharedInstance.emailAdress, "facebook_id": facebookId, "account_type": "2", "device_type": "1", "device_id": deviceID, "device_token": deviceToken]
//        weak var weakSelf = self
//
//
//        WebServiceHandler.performPOSTRequest(urlString: kSignupURL, andParameters: parameters, andAcessToken: UserDetails.sharedInstance.guestAccessToken) { (result, error) in
//            AppHelper.sharedInstance.removeSpinner()
//            if result != nil{
//                weakSelf!.facebookLoginCompletion(result as AnyObject,error)
//            }
//        }
    }
    
    
}
