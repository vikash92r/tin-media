//
//  AppDelegate.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 03/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()

        Utility.getUserDetails()
        GIDSignIn.sharedInstance().clientID = kClientID
        GIDSignIn.sharedInstance().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        UIApplication.shared.applicationIconBadgeNumber = 0;


        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
         Messaging.messaging().subscribe(toTopic: "NewsUpdates") { error in
            print("Subscribed to NewsUpdates topic")
            print("Subscribed to NewsUpdates topic", error)

        }
        
         return true
    }
       
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert, .badge, .sound])
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    // [START openurl]
         func application(_ application: UIApplication,
       open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
            
       return GIDSignIn.sharedInstance().handle(url)
         }
       // [END openurl]
       // [START openurl_new]
         @available(iOS 9.0, *)
       func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
            
       return GIDSignIn.sharedInstance().handle(url)
            
         }
       // [END openurl_new]
       // [START signin_handler]
         func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
       withError error: Error!) {
       if let error = error {
       if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
       print("The user has not signed in before or they have since signed out.")
             } else {
       print("\(error.localizedDescription)")
             }
       // [START_EXCLUDE silent]
             NotificationCenter.default.post(
       name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
       // [END_EXCLUDE]
             return
           }
            
       // Perform any operations on signed in user here.
           let userId = user.userID                  // For client-side use only!
           let idToken = user.authentication.idToken // Safe to send to the server
           let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
      
            // [START_EXCLUDE]
           NotificationCenter.default.post(
       name: Notification.Name(rawValue: "ToggleAuthUINotification"),
       object: nil,
       userInfo: ["statusText": "Signed in user:\n\(fullName!)"])
       // [END_EXCLUDE]
         }
       // [END signin_handler]
       // [START disconnect_handler]
      
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
       withError error: Error!) {
       // Perform any operations when the user disconnects from app here.
           // [START_EXCLUDE]
           NotificationCenter.default.post(
       name: Notification.Name(rawValue: "ToggleAuthUINotification"),
       object: nil,
       userInfo: ["statusText": "User has disconnected."])
       // [END_EXCLUDE]
         }

}

