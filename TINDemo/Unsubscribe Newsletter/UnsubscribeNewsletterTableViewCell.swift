//
//  UnsubscribeNewsletterTableViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 21/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class UnsubscribeNewsletterTableViewCell: UITableViewCell {

    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var lbl_unsubscribeNewsletter: UILabel!
    @IBOutlet weak var lbl_unsubscribeStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func stateChanged(_ sender: Any) {
      
            if switchButton.isOn {
                lbl_unsubscribeStatus.text = "The Switch is Off"
                switchButton.setOn(false, animated:true)
            } else {
                lbl_unsubscribeStatus.text = "The Switch is On"
                switchButton.setOn(true, animated:true)
            }
        }
}
