//
//  ProfileViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 14/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileViewController: UIViewController {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_password: UILabel!
    @IBOutlet weak var lbl_company: UILabel!
    
    @IBOutlet weak var lbl_designation: UILabel!
    @IBOutlet weak var emailViewHeight: NSLayoutConstraint!
    @IBOutlet weak var companyViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contactViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var addressView: UIView!
    var backgroundView:UIView?
    var fullName = ""
    var phone = ""
    var email = ""
    var password = ""
    var imageUrl = ""
    var company = ""
    var designation = ""
    let updateProfileNotification = NotificationCenter.default
    
    @IBOutlet weak var cardView: UIView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "My Profile"
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10

        
        self.userImageView.layer.borderWidth = 1
        self.userImageView.layer.masksToBounds = false
        self.userImageView.layer.borderColor = UIColor.gray.cgColor
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height/2
        self.userImageView.clipsToBounds = true
        
        if  UserDetails.sharedInstance.firstName.count > 0 {
            fullName = UserDetails.sharedInstance.firstName
        }
        
        if UserDetails.sharedInstance.lastName.count > 0 {
            
            fullName = fullName + " " + UserDetails.sharedInstance.lastName
        }
        
        if UserDetails.sharedInstance.usercontact.count > 0 {
            
            phone = UserDetails.sharedInstance.usercontact
        }
        else {
            
            contactViewHeight.constant = 0
            self.phoneView.isHidden = true
        }
        
        if UserDetails.sharedInstance.email.count > 0 {
            
            email =  UserDetails.sharedInstance.email
        }
        else {
            
            self.emailViewHeight.constant = 0
            self.emailView.isHidden = true
        }
        
        if UserDetails.sharedInstance.password.count > 0 {
            
            password = UserDetails.sharedInstance.password
        }
        
        if UserDetails.sharedInstance.image.count > 0 {
            
            imageUrl = UserDetails.sharedInstance.image
        }
        
        if UserDetails.sharedInstance.company.count > 0 {
            
            company = UserDetails.sharedInstance.company
        }
        else {
            
            companyViewHeight.constant = 0
            self.addressView.isHidden = true
        }
        
        if UserDetails.sharedInstance.designation.count > 0 {
            
            designation = UserDetails.sharedInstance.designation
        }
        
        self.lbl_name.text = fullName
        self.lbl_email.text = email
        self.lbl_phone.text = phone
        self.lbl_company.text = company
        self.lbl_designation.text = designation
    
       // self.lbl_password.text = password
        self.userImageView.sd_setImage(with: URL(string: imageUrl ?? ""), placeholderImage: UIImage(named: "imagePlaceholder.png"))
        
        // Do any additional setup after loading the view.
        
        self.setNavigationBarItem()
        
         updateProfileNotification.addObserver(self, selector: #selector(profileUpdated), name: Notification.Name("profileUpdated"), object: nil)
        
    }
    
    @objc func profileUpdated() {
        
        if  UserDetails.sharedInstance.firstName.count > 0 {
        
              fullName = UserDetails.sharedInstance.firstName
              }
              
        if UserDetails.sharedInstance.lastName.count > 0 {
            
                fullName = fullName + " " + UserDetails.sharedInstance.lastName
            }
              
        if UserDetails.sharedInstance.usercontact.count > 0 {
            
            phone = UserDetails.sharedInstance.usercontact
         }
        else {
            
          contactViewHeight.constant = 0
          self.phoneView.isHidden = true
        }
              
        if UserDetails.sharedInstance.email.count > 0 {
            
                  email =  UserDetails.sharedInstance.email
         }
        else {
              
            self.emailViewHeight.constant = 0
            self.emailView.isHidden = true
        }
              
        if UserDetails.sharedInstance.password.count > 0 {
            
                  password = UserDetails.sharedInstance.password
              }
              
        if UserDetails.sharedInstance.image.count > 0 {
            
                  imageUrl = UserDetails.sharedInstance.image
              }
              
        if UserDetails.sharedInstance.company.count > 0 {
            
                  company = UserDetails.sharedInstance.company
              }
           else {
            
            companyViewHeight.constant = 0
            self.addressView.isHidden = true
            }
              
        if UserDetails.sharedInstance.designation.count > 0 {
            
             designation = UserDetails.sharedInstance.designation
            }
              
        self.lbl_name.text = fullName
        self.lbl_email.text = email
        self.lbl_phone.text = phone
        self.lbl_company.text = company
        self.lbl_designation.text = designation
          
        // self.lbl_password.text = password
        self.userImageView.sd_setImage(with: URL(string: imageUrl ?? ""), placeholderImage: UIImage(named: "imagePlaceholder.png"))
              
    }
    
    
func setNavigationBarItem() {

     let btn1 = UIButton(type: .custom)

     btn1.setImage(UIImage(named: "menu_icon.png"), for: .normal)
     btn1.imageEdgeInsets.left = 5.0
     btn1.imageEdgeInsets.right = 5.0
     btn1.imageEdgeInsets.top = 5.0
     btn1.imageEdgeInsets.bottom = 5.0
     btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
     btn1.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)

    let item1 = UIBarButtonItem(customView: btn1)

    let btn2 = UIButton(type: .custom)
    btn2.setImage(UIImage(named: "logo.png"), for: .normal)
    btn2.imageEdgeInsets.left = 0.0
    btn2.imageEdgeInsets.right = 0.0
    btn2.imageEdgeInsets.top = 0.0
    btn2.imageEdgeInsets.bottom = 0.0
    btn2.imageView?.contentMode = .scaleAspectFill

    btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
           //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
    btn2.isEnabled = false
    btn2.isUserInteractionEnabled = false
    let item2 = UIBarButtonItem(customView: btn2)

    self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)

    let btn4 = UIButton(type: .custom)
    btn4.setImage(UIImage(named: "home.png"), for: .normal)
    btn4.imageEdgeInsets.left = 5.0
    btn4.imageEdgeInsets.right = 5.0
    btn4.imageEdgeInsets.top = 5.0
    btn4.imageEdgeInsets.bottom = 5.0

    btn4.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
    btn4.addTarget(self, action: #selector(self.backToHome), for: .touchUpInside)
    btn4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    let item4 = UIBarButtonItem(customView: btn4)

    self.navigationItem.setRightBarButtonItems([item4],animated: true)
 }

    
    @objc func backToHome()  {
        
        print("search")
      //  Utility.tabBarAsRootViewController()
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: false)
    
        }

    @objc func openMenu()  {

          print("open menu")
          let window = UIApplication.shared.keyWindow!
          let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems ,fromController: "Profile")
          rootView.delegate = self
          rootView.fromPreviousController = "Profile"

          let mainWindow = UIApplication.shared.keyWindow!
          backgroundView = UIView(frame: CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height))

          backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
               mainWindow.addSubview(backgroundView!);

          let transition:CATransition = CATransition.init()
          transition.type = CATransitionType.moveIn
          transition.subtype = CATransitionSubtype.fromLeft
          transition.duration = 0.5
          transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
          rootView.layer.add(transition, forKey: "rightToLeftTransition")
          window.addSubview(rootView)

      }

    func leftMenuClosed() {
        
        backgroundView?.removeFromSuperview()
    }
   
}
