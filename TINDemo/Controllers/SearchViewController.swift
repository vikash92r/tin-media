//
//  SearchViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 10/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SearchViewController: UIViewController ,UISearchResultsUpdating{
@IBOutlet weak var topStoryCollectionView: UICollectionView!
    
    private let storyreuseIdentifier = "storyCell"
    var newList = [NewsModel]()
    var catId  = ""
    var catName:String?
    var previousController = ""
    private let loadMoreuseIdentifier = "loadMoreCell"
    var initialLoadNews = 0
    var incrementLoad:Int = 10
    var isMore:Bool = false
    var refreshControl: UIRefreshControl!
    
    lazy   var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width:( self.view.frame.size.width - 60), height: 20))
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchBar.searchTextField.delegate = self
        searchBar.placeholder = "Search Text"
        searchBar.searchTextField.leftView = UIView()
        self.addRefreshControll()
        self.topStoryCollectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: loadMoreuseIdentifier)
        
       // var leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.titleView = searchBar
        
    }
    
    func addRefreshControll() {
        
        self.topStoryCollectionView.alwaysBounceVertical = true
        self.topStoryCollectionView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.topStoryCollectionView.addSubview(refreshControl)
              
    }
    
    @objc func didPullToRefresh() {
   
        self.initialLoadNews = 0
        self.incrementLoad = 10
        self.newList.removeAll()
        self.callSearchApi()
        self.refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        print("Searching with: " + (searchController.searchBar.text ?? ""))
        let searchText = (searchController.searchBar.text ?? "")
        print("search text = \(searchText)")
    }

     func callSearchApi()
        {
           let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
           spinnerActivity?.labelText = kLodindMessage
              
           let myurl = kSearchPath  + Utility.stringAfterTrim(name: self.searchBar.searchTextField.text ?? "")
           var param =  Parameters()
           param["api_key"] = kApiKey
            
            self.initialLoadNews = 0
            self.incrementLoad = 10
            self.newList.removeAll()
              
            print("url = \(myurl)")
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                  
            let swiftyJsonVar = JSON(result)
          //  print("result = \(swiftyJsonVar)")
            
            print("swiftyjsonvar result = \(swiftyJsonVar)")
                
        switch swiftyJsonVar["response"] {
            
          case "200" :
                      let newArray =   Utility.getSearchNewsList(dict: swiftyJsonVar)
                    
                      if swiftyJsonVar["next"] != "0" {
                        if self.newList.count > 0 {
                            
                                self.newList.removeLast()
                            }
                               // self.newList = Array(self.newList.prefix(self.initialLoadNews))
                            self.newList =  self.newList + newArray + [NewsModel()]
                            self.isMore = true
                        }
                    else {
                        
                        if self.newList.count > 0 {
                            
                                self.newList.removeLast()
                            }
                            
                        self.newList = self.newList + newArray
                        self.isMore = false
                                //  self.initialLoadNews = 4
                        }
                           
                        self.topStoryCollectionView.reloadData()
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        
                case "404" :
                            if self.newList.count > 0 {
                                
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                                 
                            }
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                
            default:
                      if self.newList.count > 0 {
                        
                        self.isMore = false
                        self.newList.removeLast()
                        self.topStoryCollectionView.reloadData()
                     }
                    
                    break
                }
        
           MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
         }
     }
    
     func callLoadMoreSearchApi() {
        
          let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
          spinnerActivity?.labelText = kLodindMessage
                  
          let myurl = kSearchPath  + Utility.stringAfterTrim(name: self.searchBar.searchTextField.text ?? "") + "/\(initialLoadNews)"
                
           print("myurl = \(myurl)")
           var param =  Parameters()
           param["api_key"] = kApiKey
                  
           print("url = \(myurl)")
          WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                      
            let swiftyJsonVar = JSON(result)
            print("result = \(swiftyJsonVar)")
            print("swiftyjsonvar result = \(swiftyJsonVar)")
        
            switch swiftyJsonVar["response"] {
                
            case "200" :
                        var newArray =   Utility.getSearchNewsList(dict: swiftyJsonVar)
                         
                        if swiftyJsonVar["next"] != "0" {
                            
                            if self.newList.count > 0 {
                                
                                    self.newList.removeLast()
                                }
                                   // self.newList = Array(self.newList.prefix(self.initialLoadNews))
                                self.newList =  self.newList + newArray + [NewsModel()]
                                self.isMore = true
                            }
                        else {
                            
                            if self.newList.count > 0 {
                                
                                    self.newList.removeLast()
                                }
                            
                            self.newList = self.newList + newArray
                            self.isMore = false
                                    //  self.initialLoadNews = 4
                            }
                               
                            self.topStoryCollectionView.reloadData()
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                
             case "404" :
                            if self.newList.count > 0 {
                                
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                                print("number of items = \(self.newList.count)")
                             }
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                
              case "300" :
                                if self.newList.count > 0 {
                                    
                                  self.newList.removeLast()
                                  self.topStoryCollectionView.reloadData()
                                  self.isMore = false
                                  print("number of items = \(self.newList.count)")
                                }
                    
              default:
                          if self.newList.count > 0 {
                            
                              self.isMore = false
                              self.newList.removeLast()
                              self.topStoryCollectionView.reloadData()
                            }
                            
                        break
                    }
            
               MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
             }
         }
}

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
       
       func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView.tag == collectionViewsTag.story.rawValue && self.newList.count > 0 {
             
            return 1
               // return self.storyElements.count
           }
        
         return 0
       }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
       {

          if collectionView.tag == collectionViewsTag.story.rawValue {
  
                   return self.newList.count
             }
        
           return 1
       }
       
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
        if collectionView.tag == collectionViewsTag.story.rawValue  {
            
            if isMore && indexPath.row == (self.newList.count - 1) {
                
                let kWhateverHeightYouWant = 80.0
                return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
            }
         else {
               
            let kWhateverHeightYouWant = collectionView.bounds.size.width/2
            return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
          }
        }
           
        let kWhateverHeightYouWant = collectionView.frame.height
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
           
    }
       
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
      {
           
       if collectionView.tag == collectionViewsTag.story.rawValue {
        
         if isMore  && (indexPath.row == (self.newList.count - 1)) {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadMoreuseIdentifier, for: indexPath) as! LoadMoreCollectionViewCell
            cell.backgroundColor = UIColor.red
            cell.lbl_loadMore.text = "Load More..."
//          initialLoadNews = initialLoadNews + incrementLoad
//          callSearchApi()
            initialLoadNews = initialLoadNews + incrementLoad
            callLoadMoreSearchApi()
            isMore = false
            return cell
            }
          else {
            
           let cell = collectionView
                         .dequeueReusableCell(withReuseIdentifier: storyreuseIdentifier, for: indexPath) as! StoryCollectionViewCell
           
                       // cell style
           cell.imageView.layer.cornerRadius = 10.0
           cell.imageView.layer.borderWidth = 1.0
           cell.imageView.layer.borderColor = UIColor.clear.cgColor
           cell.imageView.layer.masksToBounds = true
           
           cell.imageView.layer.shadowColor = UIColor.lightGray.cgColor
           cell.imageView.layer.backgroundColor = UIColor.clear.cgColor
            
            if self.newList.count > 0 {
             
              cell.lbl_detail.text = self.newList[indexPath.row].news_heading
              cell.lbl_timedetail.text = self.newList[indexPath.row].days_ago
                       
              let lblDayString = (self.newList[indexPath.row].from ?? "") + "    " +  (self.newList[indexPath.row].days_ago ?? "")
              let colorText = self.newList[indexPath.row].from ?? ""
                       
              cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
                                                    
              cell.lbl_detail.text = self.newList[indexPath.row].news_heading
                   
              cell.imageView.sd_setImage(with: URL(string: self.newList[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            }
            
           return cell
        }
     }
       
    let cell = collectionView
           .dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UICollectionViewCell
    return cell
   
  }
       
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
   {
     if collectionView.tag == collectionViewsTag.story.rawValue {
               
        if Utility.isConnected() {
               
               let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
               pdvc.newId = self.newList[indexPath.row].id
               self.navigationController?.pushViewController(pdvc, animated: true)
               print("Internet is available.")
                                        // ...
             }
           else {
            
             let alert = UIAlertController(title: kNewtworkNotAvailable, message: nil, preferredStyle: .alert)

             alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                       
                }))
                
              self.present(alert, animated: true)
           }
             
         }
   }
   
       
func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
          
    switch kind {
            
      case UICollectionView.elementKindSectionHeader:
        
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.label.text = "Search Result".uppercased()
              
            return headerView

    case UICollectionView.elementKindSectionFooter:
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
               
            return footerView

    default:
                return UICollectionReusableView()
           
           }
       }
       
   }

extension SearchViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        print("TextField should begin editing method called")
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        // became first responder
        print("TextField did begin editing method called")
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("TextField should snd editing method called")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        print("While entering the characters this method gets called")
        print("Search text string = \(string)")
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // called when 'return' key pressed. return NO to ignore.
        print("text field value = \(self.searchBar.searchTextField.text)")
        print("TextField should return method called")
        self.searchBar.searchTextField.resignFirstResponder()
        self.callSearchApi()
        // may be useful: textField.resignFirstResponder()
        return true
    }

}
