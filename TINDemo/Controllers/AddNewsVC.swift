//
//  AddNewsVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 18/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AddNewsVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
 @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_newsheading: UITextField!
    @IBOutlet weak var txt_newscategory: UITextField!
    @IBOutlet weak var txt_newstopic: UITextField!
    @IBOutlet weak var txt_location: UITextField!
    @IBOutlet weak var textview_newsDescription: UITextView!
    @IBOutlet weak var txt_newsimage: UITextField!
    var selectedTextField:UITextField!
    var categoryModelArray = [CategoryModel]()
    var selectedCat = [CategoryModel]()
    var subCategoryModelArray = [TopicModel]()
    var selectedSubCat = [TopicModel]()
    var selectedImage:UIImage!
    var fromController = ""
    var imagePicker = UIImagePickerController()
    var delegate:UIViewController!
    var selectedEvent:NewsModel!
    var catId = ""
    
    @IBOutlet weak var fileImageView: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.cornerRadius = CGFloat(kCornerRadiusValue)
        // Do any additional setup after loading the view.
        callNewsCategoryApi()
        callNewsTopicApi()
        
        imagePicker.delegate = self
//        imagePicker.allowsEditing = true
        
        self.textview_newsDescription.inputAccessoryView = self.toolbarWithTarget()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
       
        if self.fromController.count > 0 {
            
            self.title = "Edit News"
        }
    }

   
    func callNewsCategoryApi() {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
       
        let myurl = kNewsCategoryPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                  
        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        print("swiftyjsonvar result = \(swiftyJsonVar)")
        switch swiftyJsonVar["response"]  {
                
         case "200" :
                     self.categoryModelArray = Utility.getCategories(dict: swiftyJsonVar)
                      
                     if self.fromController.count > 0 {
                        
                                self.newsDetails()
                      }
                    
                    break
                
          case "404" :
                         break
                        
          default:
                    break
          }
        
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
      }
  }
    
    func newsDetails() {
        
       let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
       spinnerActivity?.labelText = kLodindMessage
       
       let myurl = kMyNewsDetailPath + self.catId
       var param =  Parameters()
       param["api_key"] = kApiKey
             
       WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                              
        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
                           
        print("swiftyjsonvar result = \(swiftyJsonVar)")
        switch swiftyJsonVar["response"] {
            
                               
         case "200" :
                    let detail = Utility.getMyNewsDetails(dict: swiftyJsonVar)
                    self.txt_newsheading.text = detail[0].news_heading
               // self.txt_newscategory.text = self.categoryModelArray[Int(detail[0].categoryId ?? "0") ?? 0].cat_name
                    self.txt_location.text = detail[0].location
                    self.textview_newsDescription.text = detail[0].news_description?.string ?? ""
               
                    let selectedCatArray = detail[0].news_cat?.components(separatedBy: ",")
                
                    for catId in selectedCatArray ?? [String]() {
                        
                    let catModel = self.categoryModelArray.filter { (model) -> Bool in
                        
                        model.categoryId == catId
                    }
                    
                    if catModel.count > 0   {
                        
                        self.selectedCat.append(catModel[0])
                    }
                }
                
                let values =  self.selectedCat.reduce(""){ (Result,cat) in
                        
                if Result.count > 0 {
            
                    return Result + "," + (cat.cat_name ?? "")
                }
                else  {
                    
                  return cat.cat_name ?? ""
                }
              }
                    
                    
            self.txt_newscategory.text = values
            self.txt_newstopic.text = detail[0].news_topic
                    
        // For news Topic
                    
                /*  uncomment after api changes when news topic value not null
                     
        let selectedtopicArray = detail[0].news_topic?.components(separatedBy: ",")
                                   
        for key in selectedtopicArray ?? [String]() {
                                           
            let catModel = self.subCategoryModelArray.filter { (model) -> Bool in
                                           
            model.key == key
        }
                                       
        if catModel.count > 0   {
                                        
            self.selectedSubCat.append(catModel[0])
          }
        }
                   
        let topicValues =  self.selectedSubCat.reduce(""){ (Result,cat) in
                                       
        if Result.count > 0 {
                           
            return Result + "," + (cat.value ?? "")
          }
        else  {
                                   
                return cat.value ?? ""
            }
        }
                    
      self.txt_newstopic.text = topicValues
                    
       */
                    
       // For news topic end
           
                    
       // self.txt_newsimage.text = detail[0].image

            Alamofire.request(detail[0].image ?? "", method: .get).validate().responseData(completionHandler: { (responseData) in
                
            self.selectedImage = UIImage(data: responseData.data!)
                
            if self.selectedImage == nil    {
                
              self.fileImageView.image = UIImage(named: "imagePlaceholder.png")
            }
          else {
                self.fileImageView.image = self.selectedImage
           }
                
            DispatchQueue.main.async {
                                   // Refresh you views
                               }
            })
        
            self.txt_newsheading.text = detail[0].news_heading
    
            break
                               
   case "404" :
                break
                                       
   default:
                break
            }
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
      }
    }
    
    func callNewsTopicApi()  {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kNewsTopicsPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"]{
            
        case "200" :
                    let topics = Utility.getTopics(dict: swiftyJsonVar)
                    self.subCategoryModelArray = topics
                    break
            
        case "404" :
                     break
                    
        default:
                    break
            }
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
        
    }
    
    
    
    @IBAction func chooseNewsImage(_ sender: Any) {
        
        self.view.endEditing(true)
        self.selectedTextField = self.txt_newsimage
        self.alertshow()
    }
    
    
    //****
    func alertshow() {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            
            self.openCamera()
        }))
                   
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                    
            self.openGallary()
        }))
                   
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
                
        }
            

    func openCamera() {
    
        if UIImagePickerController.isSourceTypeAvailable(.camera)   {
            
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera

//                    self.imagePicker.cameraCaptureMode = .photo
//                    self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker,animated: true,completion: nil)
        }
        else {
            
            Utility.displayAlert(title: kAppTitle, message: "camera is not available")
        }
    }


    func openGallary()  {
        
        self.imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
            
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
            {

                if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {

                  //  selectedTextField.text = fileUrl.lastPathComponent
                    
                    if selectedTextField == self.txt_newsimage {
                        
                        self.selectedImage = pickedImage
                        self.fileImageView.image = pickedImage
                        
                    }
                }
                dismiss(animated: true, completion: nil)
                
        }
            
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    //****
    func validation() -> Bool {
        
        var isValidate:Bool = true
        
        guard self.txt_newsheading.text?.count ?? 0 > 0 else {
            
         Utility.displayAlert(title: kAppTitle, message: kEnterNewsHeading)
         return false
        }
        
        guard self.txt_newscategory.text?.count ?? 0 > 0 else {
        
          Utility.displayAlert(title: kAppTitle, message:kEnterCategory)
          return false
        }
        
        
        guard self.txt_newstopic.text?.count ?? 0 > 0 else {
          
         Utility.displayAlert(title: kAppTitle, message:kEnterNewsTopic)
         return false
        }
       
        guard self.txt_location.text?.count ?? 0 > 0 else {
            
         Utility.displayAlert(title: kAppTitle, message:kEnterLocation)
         return false
        
        }
        
        guard self.textview_newsDescription.text.count > 0 else {
            
         Utility.displayAlert(title: kAppTitle, message: kEnterNewsDescription)
         return false
        }
        
        return isValidate
    }
    
    @IBAction func actionUploadNews(_ sender: Any) {
        
     if validation() {
        
        var pathUrl = kAddNewsPath + UserDetails.sharedInstance.userdata
                   
        if self.fromController.count > 0 {
            
            pathUrl = kEditNewsPath + self.catId
        }
        
    let selectedCategoryIDs =  self.selectedCat.reduce(""){ (Result,cat) in
        
       if Result.count > 0 {
        
            return Result + "," + (cat.categoryId ?? "")
         }
       else  {
        
        return cat.categoryId ?? ""
        
        }
    }
        
    let keys =  self.selectedSubCat.reduce(""){ (Result,topicModel) in
          
        if Result.count > 0 {
                   
            return Result + "," + (topicModel.key ?? "")
            }
           else  {
        
            return topicModel.key ?? ""
            }
        }
                 

        if self.selectedImage != nil {
            
        if let data = self.selectedImage.jpegData(compressionQuality: 1) {
        let parameters: Parameters = [
        "api_key" : "8a90436bdef6e286465a918d089a9ca1",
        "news_cat": selectedCategoryIDs,
        "heading":self.txt_newsheading.text ?? "",
        "news_topic": keys,
        "news_desc":self.textview_newsDescription.text ?? "",
        "location": self.txt_location.text ?? ""
        ]
                               
        var imageData  = Array<Data>()
                              
        imageData.append(self.selectedImage.jpegData(compressionQuality: 1)!)

                   //
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        Utility.performMultipartRequestAddEvent(urlString: pathUrl, fileName: "news_pic", params: parameters, imageDataArray: imageData, accessToken: nil) { (response, Error) in
                                   
        if Error == nil {
           
        print("image upload")
        print("result =\(response?["response"]!)")
        
        switch response?["response"]  {
            
            case "200":
                        if response?["inserted"] == 1 {
                                
                        let dialogMessage = UIAlertController(title: kAppTitle, message: kNewsAddedSuccessfuly, preferredStyle: .alert)
                                                  
                        // Create OK button with action handler
                        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                                                       print("Ok button tapped")
                    
                        let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyNewsVC") as! MyNewsVC
                        self.navigationController?.pushViewController(pdvc, animated: true)
                                
                        })
                                                  
        //Add OK and Cancel button to dialog message
                        dialogMessage.addAction(ok)
                        self.present(dialogMessage, animated: true, completion: nil)
                                           
                        // Utility.displayAlert(title: kAppTitle, message: kNewsAddedSuccessfuly)
                              
                        }
                    else if response?["updated"] == 1 {
                            
                        let dialogMessage = UIAlertController(title: kAppTitle, message: "News Updated Successfully.", preferredStyle: .alert)
                                            
                        // Create OK button with action handler
                        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                            
                                 print("Ok button tapped")
                                                   
                                (self.delegate as! MyNewsVC).newsUpdated()
                                self.navigationController?.popViewController(animated: true)
//                                let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyNewsVC") as! MyNewsVC
//                                self.navigationController?.pushViewController(pdvc, animated: true)
                                                            
                        })
                                                                                 
                        //Add OK and Cancel button to dialog message
                        dialogMessage.addAction(ok)
                        self.present(dialogMessage, animated: true, completion: nil)

                        }
                                           
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        case "404":
                    Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                   
        default:
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    break
        }
        
     }
                   
    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    
    }
  }
 }
else
  {
    let parameters: Parameters = [
        "api_key" : "8a90436bdef6e286465a918d089a9ca1",
        "news_cat": selectedCategoryIDs,
        "heading":self.txt_newsheading.text ?? "",
        "news_topic": keys,
        "news_desc":self.textview_newsDescription.text ?? "",
        "location": self.txt_location.text ?? ""
                    ]
                                           
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        Utility.performMultipartRequestAddEvent(urlString: pathUrl, fileName: "news_pic", params: parameters, imageDataArray: nil, accessToken: nil) { (response, Error) in
                                               
        if Error == nil {
            
            print("image upload")
            print("result =\(response?["response"]!)")
            
        switch response?["response"] {
            
         case "200":
                    if response?["inserted"] == 1 {
                                            
                    let dialogMessage = UIAlertController(title: kAppTitle, message: kNewsAddedSuccessfuly, preferredStyle: .alert)
                                                              
                    // Create OK button with action handler
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                                                
                    print("Ok button tapped")
                                
                    let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyNewsVC") as! MyNewsVC
                    self.navigationController?.pushViewController(pdvc, animated: true)
                                              
                })
                                                              
        //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                self.present(dialogMessage, animated: true, completion: nil)
                                                       
                // Utility.displayAlert(title: kAppTitle, message: kNewsAddedSuccessfuly)
                                          
            }
          else if response?["updated"] == 1 {
                                        
            let dialogMessage = UIAlertController(title: kAppTitle, message: "News Updated Successfully.", preferredStyle: .alert)
                                                        
            // Create OK button with action handler
            let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
            
            print("Ok button tapped")
                                                               
            (self.delegate as! MyNewsVC).newsUpdated()
            self.navigationController?.popViewController(animated: true)
            
                                                                        
            })
                                                                                             
            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            self.present(dialogMessage, animated: true, completion: nil)
                                            
            }
                           
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
    case "404":
                Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                               
    default:
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                break
                
               }
            }
                               
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
       
        }
                    
      }
    }
 }
    
    func selectedCategory(selectedCategory:[CategoryModel]){
          
    print("selected Category")
    //        print("category coun = \(selectedCategory.count)")
    self.selectedCat = selectedCategory
            
    let values =  self.selectedCat.reduce(""){ (Result,cat) in
    
        if Result.count > 0 {
                
           return Result + "," + (cat.cat_name ?? "")
        }
       else {
            
        return cat.cat_name ?? ""
      }
    }

    self.txt_newscategory.text = values
 }
    
    
    func selectedNewsTopics(selectedNewsTopic:[TopicModel]) {
        
            print("selected Category")
    //        print("category coun = \(selectedCategory.count)")
      self.selectedSubCat = selectedNewsTopic
            
      let values =  self.selectedSubCat.reduce(""){ (Result,cat) in
      
        if Result.count > 0 {
        
            return Result + "," + (cat.value ?? "")
        }
        else {
        
         return cat.value ?? ""
        }
     }
            
    self.txt_newstopic.text = values

}
    
    func toolbarWithTarget() -> UIToolbar {
        
        // Adding Button ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.barTintColor = .white
        toolBar.sizeToFit()
               
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.done))
               
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
              
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
               
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    @objc func cancelClick() {
        
        self.view.endEditing(true)
        print("Cancel button is clicked")
    }
    
    @objc func done() {
        
        self.view.endEditing(true)
    }
    
    @IBAction func categoryClickAction(_ sender: Any) {
        
        let window = UIApplication.shared.keyWindow!
        self.selectedTextField = self.txt_newscategory
    
               // let rootView:PopoverNewsCatTableView = PopoverNewsCatTableView.init(frame:self.view.frame)
        let rootView:PopoverNewsCatTableView = PopoverNewsCatTableView.init(frame:window.bounds,categoryArray:categoryModelArray ,selectedTextField:kCategory,selectedCategory:self.selectedCat)
        rootView.delegate = self
        rootView.selectedCategory = self.selectedCat
        window.addSubview(rootView)
        self.view.endEditing(true)
        
    }
    
    @IBAction func topicClickAction(_ sender: Any) {
        
        let window = UIApplication.shared.keyWindow!
        self.selectedTextField = self.txt_newstopic

        // let rootView:PopoverNewsCatTableView = PopoverNewsCatTableView.init(frame:self.view.frame)
        let rootView:PopoverNewsCatTableView = PopoverNewsCatTableView.init(frame:window.bounds,newsTopic:subCategoryModelArray ,selectedTextField:kNewsTopic,selectedTopic:selectedSubCat)
        rootView.delegate = self
        rootView.selectedNewsTopic = self.selectedSubCat
        window.addSubview(rootView)
        self.view.endEditing(true)
        
    }
    

}

extension AddNewsVC:UITextFieldDelegate
{
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
                // return NO to disallow editing.
                print("TextField should begin editing method called")
                return true
            }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
      if textField == txt_newsimage {
        
         self.selectedTextField = textField
                   // self.alertshow()
         }
                
        // became first responder
        print("TextField did begin editing method called")
     }

     func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
                // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
          print("TextField should snd editing method called")
          return true
          
     }

    func textFieldDidEndEditing(_ textField: UITextField) {
    
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
//                 textField.resignFirstResponder()
//                self.view.endEditing(true)
    }

      func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
//                textField.resignFirstResponder()
//                self.view.endEditing(true)
        
     }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        // return NO to not change text
        print("While entering the characters this method gets called")
        print("Search text string = \(string)")
        return true
        
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
    
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        // called when 'return' key pressed. return NO to ignore.
        print("TextField should return method called")
        textField.resignFirstResponder()
        return true
               
    }
            
}
