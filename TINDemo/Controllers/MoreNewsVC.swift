//
//  MoreNewsVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 11/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MoreNewsVC: UIViewController ,UIScrollViewDelegate{
 @IBOutlet weak var topStoryCollectionView: UICollectionView!
    
    private let storyreuseIdentifier = "storyCell"
    private let loadMoreuseIdentifier = "loadMoreCell"
     
    var newList = [NewsModel]()
    var catId  = ""
    var catName:String?
    var previousController = ""
    var initialLoadNews = 0
    var incrementLoad:Int = 10
    var isMore:Bool = false
    var noDataFound:UILabel!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad()
     {
        super.viewDidLoad()
       
        self.title = catName

         self.addRefreshControll()
        if catName == "UPCOMING EVENTS" || catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows"
        {
            callEventMoreApi(catId: catId )
        }
        else
        {
            self.callViewMoreApi(catId: catId )
        }
        
         self.topStoryCollectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: loadMoreuseIdentifier)
        
        nodataFoundLabel()
        self.noDataFound.isHidden = true
     }
    
    
    func addRefreshControll() {
        
        self.topStoryCollectionView.alwaysBounceVertical = true
        self.topStoryCollectionView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.topStoryCollectionView.addSubview(refreshControl)
        
    }
    
    @objc func didPullToRefresh() {

        self.initialLoadNews = 0
        self.incrementLoad = 10
        if catName == "UPCOMING EVENTS" || catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows" {
            
            self.newList.removeAll()
            callEventMoreApi(catId: catId )
         }
        else {
            
          self.newList.removeAll()
          self.callViewMoreApi(catId: catId )
        }
               // self.topStoryCollectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: loadMoreuseIdentifier)

        nodataFoundLabel()
        self.noDataFound.isHidden = true
        self.refreshControl.endRefreshing()
    }
    
    func nodataFoundLabel() {
        
        noDataFound = UILabel()
        noDataFound.text = ""
        noDataFound.translatesAutoresizingMaskIntoConstraints = false
        noDataFound.lineBreakMode = .byWordWrapping
        noDataFound.numberOfLines = 0
        noDataFound.textAlignment = .center

        self.view.addSubview(noDataFound)

        noDataFound.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        noDataFound.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        noDataFound.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    
      func callLoadMoreViewMoreApi(catId:String) {
        
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
               
            let myurl = kViewMorePath  + self.catId + "/\(initialLoadNews)"
            var param =  Parameters()
            param["api_key"] = kApiKey
               
             print("url = \(myurl)")
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                   
            let swiftyJsonVar = JSON(result)
            let newArray =   Utility.getNewsList(dict: swiftyJsonVar)
   
                
        print("result = \(swiftyJsonVar)")
        switch swiftyJsonVar["response"] {
            
          case "200":
                     let newArray =   Utility.getNewsList(dict: swiftyJsonVar)
                     //  self.newList = newArray
                                   
                     if swiftyJsonVar["next"] != "0" {
                        
                        if self.newList.count > 0 {
                            
                        self.newList.removeLast()
                        }
                                        
                        //self.newList = Array(self.newList.prefix(self.initialLoadNews))
                        self.newList = self.newList + newArray + [NewsModel()]
                        self.isMore = true
                    }
                else {
                      
                       if self.newList.count > 0 {
                        
                            self.newList.removeLast()
                                            
                        }
                        self.newList = self.newList + newArray
                        self.isMore = false
                                //  self.initialLoadNews = 4
                    }
                                    
                    self.topStoryCollectionView.reloadData()
                
        case "404" :
                    
                        if self.newList.count > 0 {
                            
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                            print("number of items = \(self.newList.count)")
                        }
                     else {
                          
                        self.noDataFound.text = kNoNewsFound
                        self.noDataFound.isHidden = false
                    }
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    // Utility.displayAlert(title: kAppTitle, message: "Data not found")
                
        case "300" :
            
                    if self.newList.count > 0 {
                                
                        self.isMore = false
                        self.newList.removeLast()
                        self.topStoryCollectionView.reloadData()
                                                               
                        print("number of items = \(self.newList.count)")

                    }
                            
                            
        default :
                    if self.newList.count > 0 {
                        
                        self.isMore = false
                        self.newList.removeLast()
                        self.topStoryCollectionView.reloadData()
                     }
                    
                    break
                    
                }
                
            self.topStoryCollectionView.reloadData()
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
          }
      }
    
     func callViewMoreApi(catId:String) {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
           
//        let myurl = kViewMorePath  + self.catId + "/\(initialLoadNews)"
        let myurl = kViewMorePath  + self.catId
        var param =  Parameters()
        param["api_key"] = kApiKey
           
         print("url = \(myurl)")
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
               
        let swiftyJsonVar = JSON(result)
        let newArray =   Utility.getNewsList(dict: swiftyJsonVar)

            
    print("result = \(swiftyJsonVar)")
    switch swiftyJsonVar["response"] {
        
     case "200":
                let newArray =   Utility.getNewsList(dict: swiftyJsonVar)
                //  self.newList = newArray
                               
                if swiftyJsonVar["next"] != "0" {
                    
                    if self.newList.count > 0 {
                        
                        self.newList.removeLast()
                    }
                                    
                //self.newList = Array(self.newList.prefix(self.initialLoadNews))
                self.newList = self.newList + newArray + [NewsModel()]
                self.isMore = true
                }
              else {
                   
                    if self.newList.count > 0 {
                        
                                self.newList.removeLast()
                                        
                            }
                        self.newList = self.newList + newArray
                        self.isMore = false
                            //  self.initialLoadNews = 4
                    }
                                
                    self.topStoryCollectionView.reloadData()
        
        
    case "404" :
                  if self.newList.count > 0 {
                    
                    self.isMore = false
                    self.newList.removeLast()
                    self.topStoryCollectionView.reloadData()
                  }
                else {
                     
                    self.noDataFound.text = kNoNewsFound
                    self.noDataFound.isHidden = false
                  }
                
                 MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                               // Utility.displayAlert(title: kAppTitle, message: "Data not found")
     case "300" :
        
                 if self.newList.count > 0 {
                    
                    self.newList.removeLast()

                 }
                        
                        
     default :
                if self.newList.count > 0 {
                    
                    self.isMore = false
                    self.newList.removeLast()
                    self.topStoryCollectionView.reloadData()
                }
                
                break
            }
            
        self.topStoryCollectionView.reloadData()
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
      }
  }
    
    
 func callEventMoreApi(catId:String) {
    
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        
        var myurl = ""
        myurl = kEventMorePath  + catId

        if catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows" {        
        
            myurl = kEventLeftMenuPath  + catId
        }
        else{
            
            myurl = kEventMorePath  + catId
        }
//        myurl = myurl + "/\(initialLoadNews)"
      
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        print("url = \(myurl)")
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
         let swiftyJsonVar = JSON(result)
            
        switch swiftyJsonVar["response"] {
            
            case "200":
                        let newArray =   Utility.getEvents(dict: swiftyJsonVar)
                      //  self.newList = newArray
                       
                       if swiftyJsonVar["next"] != "0" {
                        
                          if self.newList.count > 0 {
                              self.newList.removeLast()
                          }
                            
                        //self.newList = Array(self.newList.prefix(self.initialLoadNews))
                        self.newList = self.newList + newArray + [NewsModel()]
                        self.isMore = true
                        }
                    else {
                            if self.newList.count > 0 {
                                self.newList.removeLast()
                            }
                        
                        self.newList = self.newList + newArray
                        self.isMore = false
                            //  self.initialLoadNews = 4
                            
                        }
                        
                        self.topStoryCollectionView.reloadData()
                
            case "300" :
                        self.noDataFound.isHidden = false
                        if self.newList.count > 0
                        {
                            self.newList.removeLast()

                        }
                       
                         self.topStoryCollectionView.reloadData()
                        
            case "404" :
                        if self.newList.count > 0 {
                           
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                        }
                        else {
                            
                            self.noDataFound.text = kNoEventFound
                            self.noDataFound.isHidden = false
                        }
                           MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                       // Utility.displayAlert(title: kAppTitle, message: "Data not found")
                
            default :
                     
                        if self.newList.count > 0 {
                            
                        self.isMore = false
                        self.newList.removeLast()
                        self.topStoryCollectionView.reloadData()
                      
                    }
                break
            }
            
        print("result view more = \(swiftyJsonVar)")
        
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    func callLoadMoreEventMoreApi(catId:String) {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        
        var myurl = ""
        myurl = kEventMorePath  + catId

        if catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows" {
            
            myurl = kEventLeftMenuPath  + catId
        }
        else{
            
            myurl = kEventMorePath  + catId
        }
        myurl = myurl + "/\(initialLoadNews)"
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        print("url = \(myurl)")
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
         let swiftyJsonVar = JSON(result)
            
        switch swiftyJsonVar["response"] {
            
         case "200":
                    let newArray =   Utility.getEvents(dict: swiftyJsonVar)
                    //  self.newList = newArray
                       
                    if swiftyJsonVar["next"] != "0" {
                          if self.newList.count > 0 {
                            
                              self.newList.removeLast()
                          }
                            
                        //self.newList = Array(self.newList.prefix(self.initialLoadNews))
                        self.newList = self.newList + newArray + [NewsModel()]
                        self.isMore = true
                        }
                    else {
                            if self.newList.count > 0 {
                                
                                self.newList.removeLast()
                            }
                         self.newList = self.newList + newArray
                         self.isMore = false
                            //  self.initialLoadNews = 4
                            
                        }
                        
                        self.topStoryCollectionView.reloadData()
                
            case "300" :
                        self.noDataFound.isHidden = false
                        if self.newList.count > 0 {
                            
                            self.newList.removeLast()

                        }
                       
                         self.topStoryCollectionView.reloadData()
                        
            case "404" :
                        if self.newList.count > 0 {
                            
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                        }
                        else {
                            
                            self.noDataFound.text = kNoEventFound
                            self.noDataFound.isHidden = false
                        }
                           MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                       // Utility.displayAlert(title: kAppTitle, message: "Data not found")
                
            default :
                     
                        if self.newList.count > 0 {
                            
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                        }
                        break
            }
            
        print("result view more = \(swiftyJsonVar)")
        
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
}


extension MoreNewsVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
       
       
 func numberOfSections(in collectionView: UICollectionView) -> Int {
    
     if collectionView.tag == collectionViewsTag.story.rawValue && self.newList.count > 0 {
          return 1
       }
        
         return 0
    }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
            return self.newList.count
       }
       
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
      if collectionView.tag == collectionViewsTag.story.rawValue {
        
          if isMore && indexPath.row == (self.newList.count - 1) {
            
             let kWhateverHeightYouWant = 80.0
             return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
            }
         else {
                
             let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                
             return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
            }
         }
        
          let kWhateverHeightYouWant = collectionView.frame.height
          
          return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
        
       }
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
           
    if collectionView.tag == collectionViewsTag.story.rawValue {
        
        if isMore  && (indexPath.row == (self.newList.count - 1)) {
                
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadMoreuseIdentifier, for: indexPath) as! LoadMoreCollectionViewCell
        cell.backgroundColor = UIColor.red
        cell.lbl_loadMore.text = "Load More..."
        initialLoadNews = initialLoadNews + incrementLoad
        
        if catName == "UPCOMING EVENTS" || catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows" {
//                callEventMoreApi(catId: catId )
                callLoadMoreEventMoreApi(catId: catId)
            }
        else {
//                self.callViewMoreApi(catId: catId )
                self.callLoadMoreViewMoreApi(catId: catId)
            }
           // callViewMoreApi(catId: catId)
            isMore = false
            return cell
         }
        else {
                
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storyreuseIdentifier, for: indexPath) as! StoryCollectionViewCell
           
            // cell style
            cell.imageView.layer.cornerRadius = 10.0
            cell.imageView.layer.borderWidth = 1.0
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
            cell.imageView.layer.masksToBounds = true
    
            cell.imageView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.imageView.layer.backgroundColor = UIColor.clear.cgColor

        if catName == "UPCOMING EVENTS" || catName == "Webinars" || catName == "Fam Trips" ||  catName == "Conferences" || catName == "Networking" ||   catName == "Trade Shows" {

            if self.newList.count > 0 {
                
            var lblDayString = ""
                    
            if let location = self.newList[indexPath.row].location {
                
                lblDayString = location
            }
                    
            if let date_from = self.newList[indexPath.row].date_from {
                
                lblDayString = lblDayString + "  " + date_from
            }
                  //  let lblDayString = (self.newList[indexPath.row].location ?? "") + " " +  (self.newList[indexPath.row].date_from ?? "")
                                
            let colorText = self.newList[indexPath.row].location ?? ""
                                         
            cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
                                                                      
            cell.lbl_detail.text = self.newList[indexPath.row].title
                                        
            cell.imageView.sd_setImage(with: URL(string: self.newList[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
           
            }
                     
            return cell
        }
      else  {
                  
            if self.newList.count > 0 {
                
            let lblDayString = (self.newList[indexPath.row].from ?? "") + "    " +  (self.newList[indexPath.row].days_ago ?? "")
            
            let colorText = self.newList[indexPath.row].from ?? ""
                       
            cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
                                                    
            cell.lbl_detail.text = self.newList[indexPath.row].news_heading
                      
            cell.imageView.sd_setImage(with: URL(string: self.newList[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            }
                return cell
        }
      }
    }
        
     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UICollectionViewCell
        
     return cell
        
    }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
      {
         if collectionView.tag == collectionViewsTag.story.rawValue {
             if Utility.isConnected() {
                    
                 let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                 pdvc.newId = self.newList[indexPath.row].id
                 pdvc.catName = self.catName!
                 self.navigationController?.pushViewController(pdvc, animated: true)
                print("Internet is available.")
                                        // ...
                }
               else {
                
                   let alert = UIAlertController(title: kNewtworkNotAvailable, message: nil, preferredStyle: .alert)

                   alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                       
                   }))
                   self.present(alert, animated: true)
               }
           }
       }
   
       
       func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) ->
        UICollectionReusableView
       {
           switch kind {

              case UICollectionView.elementKindSectionHeader:
                
               let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
               headerView.label.text = self.catName
               
               return headerView

           case UICollectionView.elementKindSectionFooter:
            
               let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
               
               return footerView

           default:
            
               return UICollectionReusableView()
            
           }
       }
   }

