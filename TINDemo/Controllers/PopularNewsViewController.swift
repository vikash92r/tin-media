//
//  PopularNewsViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 10/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

enum collectionViewsesTag:Int {
    
    case topMenu = 101, carsole = 102, story = 103
}

class PopularNewsViewController: UIViewController ,UIScrollViewDelegate ,closeLeftMenuDelegate {

        private let reuseIdentifier = "Cell"
        private let topreuseIdentifier = "topCell"
        private let storyreuseIdentifier = "storyCell"
        private let loadMoreuseIdentifier = "loadMoreCell"
        var selectedIndex = 0
        var topNewsModels = [TopNewsModel]()
        var homeTopBanners = homeTopBannerModel()
        var topMenuList = [MenuModel]()
        var newList = [NewsModel]()
        var backgroundView:UIView?
        var initialLoadNews = 0
        var incrementLoad:Int = 10
        var isMore:Bool = false
        var noDataFound:UILabel!
        var refreshControl: UIRefreshControl!
       
       @IBOutlet weak var topStoryCollectionView: UICollectionView!
      let updateUserPictureNotification = NotificationCenter.default
       
        override func viewDidLoad() {
            
        super.viewDidLoad()
       
        self.setNavigationBarItem()
        if Utility.isConnected()
          {
            self.callPopularNewsApi()
            self.addRefreshControll()
            
            self.topStoryCollectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: loadMoreuseIdentifier)
            
            self.nodataFoundLabel()
            self.noDataFound.isHidden = true
             print("Internet is available.")
                        // ...
            }
        else
            {
                Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
            }
              
       // self.userUpdateProfilePic()
        updateUserPictureNotification.addObserver(self, selector: #selector(userUpdateProfilePic), name: Notification.Name("userUpdateProfilePic"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    func addRefreshControll() {
        
        self.topStoryCollectionView.alwaysBounceVertical = true
        self.topStoryCollectionView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.topStoryCollectionView.addSubview(refreshControl)
        
    }
    
    @objc func didPullToRefresh() {
        
        self.initialLoadNews = 0
        self.incrementLoad = 10
        self.newList.removeAll()
        self.callPopularNewsApi()
        self.refreshControl?.endRefreshing()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(true)
              self.tabBarController?.tabBar.isHidden = false
          }
    
    
       func nodataFoundLabel() {
        
              noDataFound = UILabel()
              noDataFound.text = ""
              noDataFound.translatesAutoresizingMaskIntoConstraints = false
              noDataFound.lineBreakMode = .byWordWrapping
              noDataFound.numberOfLines = 0
              noDataFound.textAlignment = .center

              self.view.addSubview(noDataFound)

              noDataFound.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
              noDataFound.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
              noDataFound.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
          }
    
    
    func leftMenuClosed() {
        
        backgroundView?.removeFromSuperview()
    }
    
    func callMenuApi() {
       
        let myurl = kMenuListPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

        let swiftyJsonVar = JSON(result)
        self.topMenuList =   Utility.getMenuList(dict: swiftyJsonVar)
        }
    }
    
    func setNavigationBarItem() {
       
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu_icon.png"), for: .normal)
        btn1.imageEdgeInsets.left = 5.0
        btn1.imageEdgeInsets.right = 5.0
        btn1.imageEdgeInsets.top = 5.0
        btn1.imageEdgeInsets.bottom = 5.0
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)

        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "logo.png"), for: .normal)
        btn2.imageEdgeInsets.left = 0.0
        btn2.imageEdgeInsets.right = 0.0
        btn2.imageEdgeInsets.top = 0.0
        btn2.imageEdgeInsets.bottom = 0.0
        btn2.imageView?.contentMode = .scaleAspectFill
        
        btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
        btn2.isEnabled = false
        btn2.isUserInteractionEnabled = false
        let item2 = UIBarButtonItem(customView: btn2)

        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
        
        
        let btn3 = UIButton(type: .custom)
        btn3.setImage(UIImage(named: "profile.png"), for: .normal)
        
        if UserDetails.sharedInstance.email.count > 0
        {
            Alamofire.request(UserDetails.sharedInstance.image, method: .get).validate()
                .responseData(completionHandler: { (responseData) in
                                   
                btn3.layer.cornerRadius = btn3.bounds.size.width/2
                //btn3.layer.borderColor = UIColor(red:0.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1).CGColor as CGColorRef
                // btn3.layer.borderWidth = 2.0
                btn3.layer.masksToBounds = false
                btn3.clipsToBounds = true
                btn3.setImage(UIImage(data: responseData.data!), for: .normal)
                DispatchQueue.main.async {
                            // Refresh you views
                    }
                })
         }
        
        
//        btn3.imageEdgeInsets.left = 5.0
//        btn3.imageEdgeInsets.right = 5.0
//        btn3.imageEdgeInsets.top = 5.0
//        btn3.imageEdgeInsets.bottom = 5.0
        btn3.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn3.addTarget(self, action: #selector(self.profile), for:.touchUpInside)
        let item3 = UIBarButtonItem(customView: btn3)
        
        let currWidth = item3.customView?.widthAnchor.constraint(equalToConstant: 35)
        currWidth?.isActive = true
                
        let currHeight = item3.customView?.heightAnchor.constraint(equalToConstant: 35)
        currHeight?.isActive = true
        
        let btn4 = UIButton(type: .custom)
        btn4.setImage(UIImage(named: "search.png"), for: .normal)
        btn4.imageEdgeInsets.left = 5.0
        btn4.imageEdgeInsets.right = 5.0
        btn4.imageEdgeInsets.top = 5.0
        btn4.imageEdgeInsets.bottom = 5.0
        
        btn4.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn4.addTarget(self, action: #selector(self.search), for: .touchUpInside)
        btn4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        let item4 = UIBarButtonItem(customView: btn4)

        self.navigationItem.setRightBarButtonItems([item4,item3],animated: true)
        
    }
    
   
     @objc func userUpdateProfilePic()
     {
            
            self.navigationItem.rightBarButtonItems?.removeAll()
                   
            let btn3 = UIButton(type: .custom)
            btn3.setImage(UIImage(named: "profile.png"), for: .normal)
            
            if UserDetails.sharedInstance.email.count > 0
            {
                Alamofire.request(UserDetails.sharedInstance.image, method: .get)
                         .validate()
                         .responseData(completionHandler: { (responseData) in
                            
                            btn3.layer.cornerRadius = btn3.bounds.size.width/2
                            //btn3.layer.borderColor = UIColor(red:0.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1).CGColor as CGColorRef
                           // btn3.layer.borderWidth = 2.0
                            btn3.layer.masksToBounds = false
                            btn3.clipsToBounds = true
                            btn3.setImage(UIImage(data: responseData.data!), for: .normal)
                             DispatchQueue.main.async {
                                 // Refresh you views
                             }
                         })
            }
            
    //        btn3.imageEdgeInsets.left = 5.0
    //        btn3.imageEdgeInsets.right = 5.0
    //        btn3.imageEdgeInsets.top = 5.0
    //        btn3.imageEdgeInsets.bottom = 5.0
            btn3.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
            btn3.addTarget(self, action: #selector(self.profile), for:.touchUpInside)
            btn3.imageView?.contentMode = .scaleAspectFit
            
           
            let item3 = UIBarButtonItem(customView: btn3)
            
            let currWidth = item3.customView?.widthAnchor.constraint(equalToConstant: 35)
            currWidth?.isActive = true
            
            let currHeight = item3.customView?.heightAnchor.constraint(equalToConstant: 35)
            currHeight?.isActive = true
            
            let btn4 = UIButton(type: .custom)
            btn4.setImage(UIImage(named: "search.png"), for: .normal)
            btn4.imageEdgeInsets.left = 5.0
            btn4.imageEdgeInsets.right = 5.0
            btn4.imageEdgeInsets.top = 5.0
            btn4.imageEdgeInsets.bottom = 5.0
            btn4.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            btn4.addTarget(self, action: #selector(self.search), for: .touchUpInside)
            btn4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
            btn4.imageView?.contentMode = .scaleAspectFit
            let item4 = UIBarButtonItem(customView: btn4)
                   
            self.navigationItem.setRightBarButtonItems([item4,item3],animated: true)
                   
        }
    
   @objc func fbButtonPressed() {
        print("facebook button pressd")
    }
    
    @objc func openMenu()  {
        print("firstName")
        
        let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems, fromController: "PopularNews" )
                    
        let mainWindow = UIApplication.shared.keyWindow!
        backgroundView = UIView(frame: CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height))
        backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainWindow.addSubview(backgroundView!);
        
        rootView.delegate = self
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
        
    }
    
    @objc func secondName()  {
        print("secondName")
    }
    
    @objc func profile()  {
        print("profile")
        if UserDetails.sharedInstance.email.count > 0 {
            
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(pdvc, animated: true)
        }
        else {
            
          let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
          self.navigationController?.pushViewController(pdvc, animated: true)
        }
    }
    
    @objc func search()  {
        
            print("search")
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            self.navigationController?.pushViewController(pdvc, animated: true)
       }
    
    
     func callPopularNewsApi() {
        
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
            let myurl = kPopularNewsPath
            var param =  Parameters()
            param["api_key"] = kApiKey
            
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
            let swiftyJsonVar = JSON(result)
                
                print("swiftyjsonvar result = \(swiftyJsonVar)")
            switch swiftyJsonVar["response"] {
                
             case "200" :
                         var newArray =   Utility.getPopularNews(dict: swiftyJsonVar)
                    
                        if swiftyJsonVar["next"] != "0" {
                            if self.newList.count > 0 {
                                        
                                self.newList.removeLast()
                               }
                                // self.newList = Array(self.newList.prefix(self.initialLoadNews))
                                self.newList =  self.newList + newArray + [NewsModel()]
                                self.isMore = true
                            }
                          else {
                                 if self.newList.count > 0 {
                                       
                                 self.newList.removeLast()
                                }
                                self.newList = self.newList + newArray
                                self.isMore = false
                                    //  self.initialLoadNews = 4
                             }
                               
                             self.topStoryCollectionView.reloadData()
                              MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
             case "404" :
                                if self.newList.count > 0 {
                                    
                                        self.isMore = false
                                        self.newList.removeLast()
                                        self.topStoryCollectionView.reloadData()
                                     
                                    }
                                else {
                                    
                                }
                                       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    
                default:
                          if self.newList.count > 0  {
                            
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                            }
                          break
                    }
                
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
         }
    }
    
    
    func callLoadMorePopularNewsApi() {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        
        let myurl = kPopularNewsPath + "/\(initialLoadNews)"
        print("myurl = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
                
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                
        let swiftyJsonVar = JSON(result)
                    
        print("swiftyjsonvar result = \(swiftyJsonVar)")
        
        switch swiftyJsonVar["response"] {
            
            case "200" :
                        var newArray =   Utility.getPopularNews(dict: swiftyJsonVar)
                        // self.newList = newArray
                                
                        // if self.initialLoadNews < self.newList.count
                        if swiftyJsonVar["next"] != "0" {
                            
                            if self.newList.count > 0 {
                                
                                    self.newList.removeLast()
                                }
                            // self.newList = Array(self.newList.prefix(self.initialLoadNews))
                            self.newList =  self.newList + newArray + [NewsModel()]
                            self.isMore = true
                            }
                          else {
                                
                                if self.newList.count > 0 {
                                    
                                    self.newList.removeLast()
                                    }
                                    
                                    self.newList = self.newList + newArray
                                    self.isMore = false
                                        //  self.initialLoadNews = 4
                                }
                                   
                                self.topStoryCollectionView.reloadData()
                                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        
            case "404" :
                           if self.newList.count > 0 {
                                
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                                print("number of items = \(self.newList.count)")
                                         
                                }
                             else
                                 {
                                        
                                  }
                                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    
            case "300" :
                            if self.newList.count > 0 {
                                
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                             }
                        
                   
            default:
                         if self.newList.count > 0 {
                            
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                            }
                        
                        break
                }
                    
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
             }
        }
    
    
    
    func callNewsListApi(id:String)  {
        
          let myurl =  kNewsListPath + id
          var param =  Parameters()
          param["api_key"] = kApiKey
          
          let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
          spinnerActivity?.labelText = kLodindMessage
          
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

          let swiftyJsonVar = JSON(result)
          self.newList =   Utility.getNewsList(dict: swiftyJsonVar)
            
          self.topStoryCollectionView.reloadData()
          MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                
          }
      }
        
        
        
        @IBAction func menuOpenAction(_ sender: Any) {
            
           let window = UIApplication.shared.keyWindow!
//            var rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray: self.topMenuList )
            let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray: leftMenuItems, fromController: "PopularNews" )
                   // rootView.categoryList = DBManager.shared.loadCategories()
           let transition:CATransition = CATransition.init()
           transition.type = CATransitionType.moveIn
           transition.subtype = CATransitionSubtype.fromLeft
           transition.duration = 0.5
           transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
           rootView.layer.add(transition, forKey: "rightToLeftTransition")
           window.addSubview(rootView)
            
     }
        
           // delegete method of category

       
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
        {

        }
    
    @IBAction func menuOpenActionPopular(_ sender: Any) {
        
        let window = UIApplication.shared.keyWindow!
          // var rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray: self.topMenuList )
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray: leftMenuItems, fromController: "PopularNews")

        // rootView.categoryList = DBManager.shared.loadCategories()
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
        
    }
       
}

    extension PopularNewsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            
            if collectionView.tag == collectionViewsTag.story.rawValue && self.newList.count > 0 {
                
                return 1
                // return self.storyElements.count
            }
          return 0
        }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

            if collectionView.tag == collectionViewsTag.topMenu.rawValue {
                
                return self.topMenuList.count
                
               }
            else if collectionView.tag == collectionViewsTag.carsole.rawValue
              {
                return self.homeTopBanners.banner.count
              }
            else if collectionView.tag == collectionViewsTag.story.rawValue
              {
   
                    return self.newList.count
              }
            return 1
        }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
        {
            
         if collectionView.tag == collectionViewsTag.story.rawValue {
            
                if isMore && indexPath.row == (self.newList.count - 1) {
                    
                    let kWhateverHeightYouWant = 80.0
                    return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
                }
                else {
                    
                    let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                    return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
                }
            }
            
            let kWhateverHeightYouWant = collectionView.frame.height
            return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
            
        }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
        if collectionView.tag == collectionViewsTag.story.rawValue {
            
            if isMore  && (indexPath.row == (self.newList.count - 1)) {
            
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadMoreuseIdentifier, for: indexPath) as! LoadMoreCollectionViewCell
              cell.backgroundColor = UIColor.red
              cell.lbl_loadMore.text = "Load More..."
//                initialLoadNews = initialLoadNews + incrementLoad
//                callPopularNewsApi()
              initialLoadNews = initialLoadNews + incrementLoad
              callLoadMorePopularNewsApi()
              isMore = false
              return cell
            }
          else {
                
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storyreuseIdentifier, for: indexPath) as! StoryCollectionViewCell
            
                        // cell style
            cell.imageView.layer.cornerRadius = 10.0
            cell.imageView.layer.borderWidth = 1.0
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
            cell.imageView.layer.masksToBounds = true
            
            cell.imageView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.imageView.layer.backgroundColor = UIColor.clear.cgColor
                
            if  self.newList.count > 0 {
            
            cell.lbl_detail.text = self.newList[indexPath.row].news_heading
            cell.lbl_timedetail.text = self.newList[indexPath.row].days_ago
                        
            let lblDayString = (self.newList[indexPath.row].from ?? "") + "    " +  (self.newList[indexPath.row].days_ago ?? "")
            let colorText = self.newList[indexPath.row].from ?? ""
                        
   
            cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
                                                     
            cell.lbl_detail.text = self.newList[indexPath.row].news_heading
                    
            cell.imageView.sd_setImage(with: URL(string: self.newList[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            
            }
    
            return cell
          }
        }
        
          let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UICollectionViewCell
         return cell
    }
        
 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
      if collectionView.tag == collectionViewsTag.story.rawValue {
                
        if Utility.isConnected() {
                
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            pdvc.newId = self.newList[indexPath.row].id
            self.navigationController?.pushViewController(pdvc, animated: true)
            print("Internet is available.")
            // ...
            }
         else {
                
            let alert = UIAlertController(title: kNewtworkNotAvailable, message: nil, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
            }))
        
            self.present(alert, animated: true)
         }
      }
 }
    
        
 func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
           
        switch kind {
             
         case UICollectionView.elementKindSectionHeader:
            
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
                    headerView.label.text = "Popular News".uppercased()
               
                    return headerView

         case UICollectionView.elementKindSectionFooter:
             
                    let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
                
                    return footerView

        default:
                  return UICollectionReusableView()
            
            }
        }
        
    }
    //
//    class HeaderCollectionReusableView: UICollectionReusableView {
//      @IBOutlet var label: UILabel!
//    }
   
