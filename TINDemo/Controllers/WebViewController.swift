//
//  WebViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 15/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import Alamofire

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    var urlString:String?
    var controllerName = ""
    
    override func loadView()
    {
        self.title = controllerName
        print("title = \(controllerName)")
        
        webView = WKWebView()
        webView.navigationDelegate = self as! WKNavigationDelegate
        view = webView
       
    }
    
    func getPageDataApi() {
    
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
     
        let myurl = self.urlString
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
          
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                    
        let swiftyJsonVar = JSON(result)

        print("response = \(swiftyJsonVar)")
                               
        switch swiftyJsonVar["response"] {
                
        case "200":
                    let pagesData = Utility.getPagesData(dict: swiftyJsonVar)
                    self.webView.loadHTMLString(pagesData[0].page_content ?? "", baseURL: nil)
                    self.title = pagesData[0].page_title
                    break

                
        default:   Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)

        }

       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
     }
   }
    
    override func viewDidLoad() {
      super.viewDidLoad()
        
        if self.controllerName == "Privacy Policy" {
            
        urlString = kPagesDataUrl + "privacypolicy"
        self.getPageDataApi()
        }
        else if self.controllerName == "Cookie Policy" {
            
          urlString = kPagesDataUrl + "cookiepolicy"
          self.getPageDataApi()
        }
        else if self.controllerName == "Copyright Policy" {
            
            urlString = kPagesDataUrl + "copyrightpolicy"
            self.getPageDataApi()
        }
        else if self.controllerName == "Subscriber Agreement & Terms of Use" {
            
            urlString = kPagesDataUrl + "subpolicy"
            self.getPageDataApi()
        }
        else if self.controllerName == "Data Policy"{
            
            urlString = kPagesDataUrl + "datapolicy"
            self.getPageDataApi()
        }
        else if self.controllerName == "About Us"
        {
            urlString = kPagesDataUrl + "aboutus"
            self.getPageDataApi()
        }
        else if self.controllerName == "Contact Us"
        {
            urlString = kPagesDataUrl + "contactus"
            self.getPageDataApi()
        }
        else {
            
          let url = URL(string: urlString ?? "")!
          webView.load(URLRequest(url: url))
          webView.allowsBackForwardNavigationGestures = true
          let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
          spinnerActivity?.labelText = kLodindMessage
        }

        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
      
        print("Start loading")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }

    func webView(_ webView: WKWebView,didFail navigation: WKNavigation!,withError error: Error){
        
        print("erro in loading")
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
}
