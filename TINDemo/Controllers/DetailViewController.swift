//
//  DetailViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 05/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var lbl_Heading: UILabel!
    var  newId:String?
    var catName:String?
    var news = [NewsDetailModel]()
    var upcommingEvents = [NewsModel]()
    var bookmarkDetail = [NewsModel]()
    @IBOutlet weak var lbl_daysageo: UILabel!
    @IBOutlet weak var text_description: UITextView!
    @IBOutlet weak var image: UIImageView!
    var isBookMarkEnabled:Bool = true
     let bookmarkBtn = UIButton(type: .custom)
    
    override func viewDidLoad() {
        
       super.viewDidLoad()
    
       self.lbl_Heading.text = ""
       self.setNavigationBarItem()
        
       self.image.isHidden = true
       
        if Utility.isConnected() {
            
        if catName == "UPCOMING EVENTS" || catName == "Webinars"  || catName == "Fam Trips" || catName == "Conferences" || catName == "Networking" || catName == "Trade Shows" {

                self.callUpcommingEventApi()
            
            }
           else if catName == "bookmarked" {
                self.callNewsDetailApi()
            
            }
           else {
                  self.callNewsDetailApi()
            
            }
               print("Internet is available.")
               // ...
        }
        else {
            
            Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
            
        }
        
    }
    
    func setNavigationBarItem()
       {
           
          self.isBookmarked(newsID: self.newId!)
        
          bookmarkBtn.setImage(UIImage(named: "bookmarked"), for: .normal)
          bookmarkBtn.imageEdgeInsets.left = 5.0
          bookmarkBtn.imageEdgeInsets.right = 5.0
          bookmarkBtn.imageEdgeInsets.top = 5.0
          bookmarkBtn.imageEdgeInsets.bottom = 5.0
          bookmarkBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
          bookmarkBtn.addTarget(self, action: #selector(self.actionBookMark), for:.touchUpInside)
          bookmarkBtn.tag = 401
          bookmarkBtn.imageView?.contentMode = .scaleAspectFit
          bookmarkBtn.isEnabled = isBookMarkEnabled
        
    
        let shareBtn = UIButton(type: .custom)
        shareBtn.setImage(UIImage(named: "shareicon.png"), for: .normal)
        shareBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        shareBtn.imageEdgeInsets.left = 5.0
        shareBtn.imageEdgeInsets.right = 5.0
        shareBtn.imageEdgeInsets.top = 5.0
        shareBtn.imageEdgeInsets.bottom = 5.0
        shareBtn.addTarget(self, action: #selector(self.actionShare), for:.touchUpInside)
        shareBtn.imageView?.contentMode = .scaleAspectFit
        shareBtn.isEnabled = isBookMarkEnabled
        
          let item3 = UIBarButtonItem(customView: bookmarkBtn)
          let item2 = UIBarButtonItem(customView: shareBtn)
           self.navigationItem.setRightBarButtonItems([item3,item2],animated: true)
        
       }
    
       @objc func openMenu()  {
           print("firstName")
           
            let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems, fromController: "DetailViewController" )
                        // rootView.categoryList = DBManager.shared.loadCategories()
            let transition:CATransition = CATransition.init()
            transition.type = CATransitionType.moveIn
            transition.subtype = CATransitionSubtype.fromLeft
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            rootView.layer.add(transition, forKey: "rightToLeftTransition")
            window.addSubview(rootView)
        
       }
    
    func isBookmarked(newsID:String)->Bool
       {
         var isbookmarked:Bool = false
         let myurl = kBookMarkCheckPath + kUUIDValue + "/" + newsID
         var param =  Parameters()
         
        param["api_key"] = kApiKey
        print("url = \(myurl)")
                   
      WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
        
        let swiftyJsonVar = JSON(result)
        switch swiftyJsonVar["response"] {
                         
        case "200":
                   switch swiftyJsonVar["isBookmarked"] {
                    
                   case 0:
                          isbookmarked = false
                          self.bookmarkBtn.setImage(UIImage(named: "bookmarked.png"), for: .normal)
                    
                   case 1:
                           isbookmarked = true
                           self.bookmarkBtn.setImage(UIImage(named: "bookMarkSaved.png"), for: .normal)
                
                   default:
                            isbookmarked = false
                                           
                    }
        
        default: isbookmarked = false
                              /// Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
           }
        
        }
    return isbookmarked
  
  }
    
    
@objc func secondName()  {

    print("secondName")
    
    }
       
@objc func profile()  {
    
        print("profile")
    
    }
    
@objc func search()  {
              print("search")
    
    }
    
   
func callUpcommingEventApi()  {
    
    let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
    spinnerActivity?.labelText = kLodindMessage
    
    let myurl = kEventsDetailPath + self.newId!
       
    var param =  Parameters()
    param["api_key"] = kApiKey
          
    WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
          
        let swiftyJsonVar = JSON(result)
        self.upcommingEvents = Utility.getEvents(dict: swiftyJsonVar)
    
        if self.upcommingEvents.count > 0
          {
            let lblDayString = "Date: " + (self.upcommingEvents[0].date_from ?? "") + " - " +  (self.upcommingEvents[0].date_to ?? "")
    
            self.lbl_daysageo.text = lblDayString
            self.lbl_Heading.text = self.upcommingEvents[0].title

            self.image.sd_setImage(with: URL(string: self.upcommingEvents[0].image! ), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            self.text_description.attributedText = self.upcommingEvents[0].description
            
            self.image.isHidden = false

         }
        
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
     }
}
    
    
    func callNewsDetailApi() {
        
    let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
    spinnerActivity?.labelText = kLodindMessage
    
    let myurl = kNewsDetailPath + self.newId!
    var param =  Parameters()
    param["api_key"] = kApiKey
          
    WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
          
        let swiftyJsonVar = JSON(result)
        self.news = Utility.getNewsDetails(dict: swiftyJsonVar)
            
        if self.news.count > 0  {
            
          let lblDayString = (self.news[0].from ?? "") + "    " +  (self.news[0].days_ago ?? "")
          let colorText = self.news[0].from ?? ""
            
          self.lbl_daysageo.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
            
          self.lbl_Heading.text = self.news[0].news_heading

          self.image.sd_setImage(with: URL(string: self.news[0].image! ), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            
          self.text_description.attributedText = self.news[0].news_description
          self.image.isHidden = false
        
         }
            
       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
     }
 }
    
    
func callBookmarkedDetailApi()  {
    
   let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
   spinnerActivity?.labelText = kLodindMessage
   
   let myurl = kViewBookMarkNewsPath + kUUIDValue + "/"  + self.newId!
   var param =  Parameters()
   param["api_key"] = kApiKey
             
   WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
             
     let swiftyJsonVar = JSON(result)
     self.bookmarkDetail  = Utility.getNewsBookmarkedDetail(dict: swiftyJsonVar)
               
     if self.bookmarkDetail.count > 0 {
        
        let lblDayString = (self.bookmarkDetail[0].from ?? "") + "    " +  (self.bookmarkDetail[0].days_ago ?? "")
        let colorText = self.bookmarkDetail[0].from ?? ""
               
        self.lbl_daysageo.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
               
        self.lbl_Heading.text = self.bookmarkDetail[0].news_heading

        self.image.sd_setImage(with: URL(string: self.bookmarkDetail[0].image! ), placeholderImage: UIImage(named: "imagePlaceholder.png"))

        }
            
       self.view.layoutIfNeeded()
            
       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
               
     }
  }
    
    
    @objc func actionShare() {
        
        if catName == "UPCOMING EVENTS" || catName == "Webinars"  || catName == "Fam Trips" || catName == "Conferences" || catName == "Networking" || catName == "Trade Shows" {
            
            if self.upcommingEvents.count > 0 {
                
            let newsUrl = self.upcommingEvents[0].news_url ?? ""
            
            if newsUrl.count > 0  {
                
            let someText:String = "Share Event"
            let objectsToShare:URL = URL(string: newsUrl)!
            let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
            let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view

            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]

            self.present(activityViewController, animated: true, completion: nil)
          }
        }
      }
    else {
            
      if self.news.count > 0 {
        
        let newsUrl = self.news[0].news_url ?? ""
        if newsUrl.count > 0 {
            
         let someText:String = "Share News"
         let objectsToShare:URL = URL(string: newsUrl)!
         let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
         let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        
         activityViewController.popoverPresentationController?.sourceView = self.view

         activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]

         self.present(activityViewController, animated: true, completion: nil)
         }
      }
    }
       
}
    
    
    @objc func actionBookMark() {
        
        print("addTransaction")
       // let myurl = kAddBookMarkPath + kUUIDValue + "/" + (self.news[0].id ?? "")
        let myurl = kAddBookMarkPath + kUUIDValue + "/" + (newId ?? "")
        var param =  Parameters()
        
        param["api_key"] = kApiKey
        print("url = \(myurl)")
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
         let swiftyJsonVar = JSON(result)
        switch swiftyJsonVar["response"] {
            
         case "200":
                    switch swiftyJsonVar["inserted"] {
            
                    case 1:
                        
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name("UserLoggedIn"), object: nil)
                                    
                            Utility.displayAlert(title: kAppTitle, message: kBookMarkAdded)
                            self.bookmarkBtn.setImage(UIImage(named: "bookMarkSaved"), for: .normal)
                            self.bookmarkBtn.imageView?.contentMode = .scaleAspectFit
                            self.bookmarkBtn.layoutIfNeeded()
                            self.view.layoutIfNeeded()
                        
                    case 2:
                        
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name("UserLoggedIn"), object: nil)
                            Utility.displayAlert(title: kAppTitle, message: kBookMarkRemoved)
                            self.bookmarkBtn.setImage(UIImage(named: "bookmarked"), for: .normal)
                            self.bookmarkBtn.imageView?.contentMode = .scaleAspectFit
                            self.bookmarkBtn.layoutIfNeeded()
                            self.view.layoutIfNeeded()
                                
                    default:
                            print("default")
                                
                    }
            
        default:   Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
            
        }
            
        print("result = \(swiftyJsonVar)")
        print("result \(swiftyJsonVar["inserted"])")
          
    }
   
  }
    
}
