//
//  VedioDetailsVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 13/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage
import AVFoundation
import AVKit


class VedioDetailsVC: UIViewController {
    @IBOutlet weak var vedioTableView: UITableView!
    var vedioId:String?
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var lbl_from: UILabel!
    @IBOutlet weak var vedioImageVedio: UIImageView!
    @IBOutlet weak var textviewDetails: UITextView!
    var newId:String?
    var catName:String?
    @IBOutlet weak var vediosImageView: UIView!
    var vedioDetails = [NewsModel]()
    var vediosList = [NewsModel]()
    @IBOutlet weak var lbl_heading: UILabel!
    var vedioImagaeString:String?
    var fromController:String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.vedioTableView.isHidden = true
        self.callVediosApi()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionMoreVideos(_ sender: Any) {
        
        if self.fromController == "Home" {
            
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
            pdvc.previousController = "Home"
            self.navigationController?.pushViewController(pdvc, animated: true)
        }
        else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func callVediosApi() {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
                    
        let myurl = kVediosDetailsPath + "/" + (vedioId! )
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
                    
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                        
        let swiftyJsonVar = JSON(result)
         
        print("vedios data = \(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"] {
            
          case "200":
                    let newArray =   Utility.getVediosDetais(dict: swiftyJsonVar)
                    self.vedioDetails =  newArray.vediosDetail
                    self.vediosList = newArray.vediosList
                                   
                    if self.vedioDetails.count > 0 {
                        
                    self.vedioImageVedio.sd_setImage(with: URL(string: self.vedioDetails[0].image_url!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                    self.vedioTableView.isHidden = false
                    }
                                    
                    self.lbl_from.text =  self.vedioDetails[0].days_ago
                    self.lbl_heading.text = self.vedioDetails[0].news_heading
                    self.textviewDetails.attributedText = self.vedioDetails[0].description
                    self.vedioImagaeString = self.vedioDetails[0].image_url!
                    self.vedioTableView.reloadData()
                    print("imageurl = \(self.vedioDetails[0].image_url!)")
                               
                    break
                    
        case "404" :
                               
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                               
        default:
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    break
         }
            
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
       }
    }
    
    @IBAction func actionPlayVedio(_ sender: Any)
    {
//        let player = AVPlayer(url: URL(fileURLWithPath: self.vedioDetails[0].video_url ?? ""))
//         let vc =  AVPlayerViewController()
//         vc.player = player
//        self.present(vc, animated: true) {
//        vc.player?.play()
            
          if let url = URL(string: self.vedioDetails[0].video_url ?? "") {
                UIApplication.shared.open(url)
            }
//     }
    }
}


extension VedioDetailsVC:UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (self.vediosList.count + 3)
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
                
            var cell : VedioDetailImageTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "VedioDetailImageTableViewCell") as! VedioDetailImageTableViewCell
            if cell == nil {
                        
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "VedioDetailImageTableViewCell") as! VedioDetailImageTableViewCell
            }
                
              //  print("image = \(self.vedioDetails[0].image_url!)")
            cell?.vedioImage.sd_setImage(with: URL(string: self.vedioImagaeString ?? ""), placeholderImage: UIImage(named: "imagePlaceholder.png"))
             
            return cell!
                
        }
    else  if indexPath.row == 1 {
            
        var cell : TextViewTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "TextViewTableViewCell") as! TextViewTableViewCell
        
        if cell == nil {
            
           cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TextViewTableViewCell") as! TextViewTableViewCell
        }
             
        if vedioDetails.count > 0 {
            
           cell?.lbl_from.text =  self.vedioDetails[0].days_ago
           cell?.lbl_heading.text = self.vedioDetails[0].news_heading
           cell?.textViewDescription.attributedText = self.vedioDetails[0].description
         }
        return cell!
           
      }
    else if indexPath.row == vediosList.count + 2 {
            
        var cell : MoreVideosTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "MoreVideosTableViewCell") as! MoreVideosTableViewCell

        if cell == nil {
            
          cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MoreVideosTableViewCell") as! MoreVideosTableViewCell
            
        }
    
    return cell!
    }
    else {
                
        // create a new cell if needed or reuse an old one
        var cell : VediosDetailsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "VediosDetailsTableViewCell") as! VediosDetailsTableViewCell
        
        if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "VediosDetailsTableViewCell") as! VediosDetailsTableViewCell
        }
    if self.vediosList.count > 0
        {
  
         cell?.lbl_heading.text = vediosList[indexPath.row - 2].news_heading
         cell?.lbl_timeDetail.text = vediosList[indexPath.row - 2 ].days_ago
         cell?.vedioImageView.sd_setImage(with: URL(string: self.vediosList[indexPath.row - 2].image_url!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
        
        }
        
    cell?.textLabel?.numberOfLines = 0
                
    return cell!
            
    }
        
            
    var cell : VediosDetailsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "VediosDetailsTableViewCell") as! VediosDetailsTableViewCell
        
        if cell == nil {
            
              cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "VediosDetailsTableViewCell") as! VediosDetailsTableViewCell
            }
            return cell!
        
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == 0 {
            
            guard let vedioUrl =  URL(string: self.vedioDetails[0].video_url ?? "") else { return  }
            
            UIApplication.shared.open(vedioUrl)
        }
    
    else if indexPath.row == vediosList.count + 2 {
            
            
        }
    
    else if let url = URL(string: self.vediosList[indexPath.row - 2].video_url ?? "") {
            
            UIApplication.shared.open(url)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
        
}

