//
//  ChangePasswordViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 09/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var txt_newpassword: UITextField!
    @IBOutlet weak var txt_oldpassword: UITextField!
    @IBOutlet weak var txt_confirmpassword: UITextField!
     @IBOutlet weak var cardView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.setPlaceholderColor()

        // Do any additional setup after loading the view.
    }
    
    func setPlaceholderColor() {
        
        self.txt_oldpassword.placeholderColor(textfieldPlaceholder)
        self.txt_newpassword.placeholderColor(textfieldPlaceholder)
        self.txt_confirmpassword.placeholderColor(textfieldPlaceholder)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        
        let oldpassword = Utility.stringAfterTrim(name: txt_oldpassword.text!)
        let newpassword = Utility.stringAfterTrim(name:txt_newpassword.text!)
        let confirmpassword = Utility.stringAfterTrim(name:txt_confirmpassword.text!)

        guard oldpassword.count != 0 else {
            
           Utility.displayAlert(title: kAppTitle, message: kEnterOldPassword)
           return
        }
          
        guard newpassword.count != 0 else{
            
            Utility.displayAlert(title: kAppTitle, message: kEnterNewPassword)
            return
         }
               
        guard confirmpassword.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterConfirmPassword)
            return
         }
               
        if newpassword  != confirmpassword {
            
           Utility.displayAlert(title: kAppTitle, message: kPasswordNotMatched)
            return
         }
        
        print("password = \(UserDetails.sharedInstance.password)")
        if oldpassword != UserDetails.sharedInstance.password {
            
            Utility.displayAlert(title: kAppTitle, message: kWrongOldPasswordNotMatched)
             return
        }
       
        callChangePasswordApi()
    }
    
    
     func callChangePasswordApi() {
                
        self.view.endEditing(true)
        
        let myurl = kChangePasswordPath
                      
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        print("path url = \(myurl)")
      
        var param =  Parameters()
        param["api_key"] = kApiKey
        param["loogeddata"] = UserDetails.sharedInstance.userdata
        param["npassword"] =  Utility.stringAfterTrim(name:txt_newpassword.text!)
                
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                                    
        let swiftyJsonVar = JSON(result)
                        
        switch swiftyJsonVar["response"] {
            
         case  "200":
                
                if swiftyJsonVar["updated"] == "1" {
                        
                let alert = UIAlertController(title: kAppTitle, message:"Password changed successfully.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//                                Utility.tabBarAsRootViewController()
                UserDetails.sharedInstance.password = Utility.stringAfterTrim(name:self.txt_newpassword.text!)
                Utility.saveUserDetails()
                self.navigationController?.popViewController(animated: true)
            }))
                
               self.present(alert, animated: true, completion: nil)
                                
             }
          else if swiftyJsonVar["updated"] == "0" {
                    
              Utility.displayAlert(title: kAppTitle, message: "Password not changed.")
            }
                            
        
         default:
                    Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
                    break
          }
            
          print("result = \(swiftyJsonVar)")
         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
      }
   }
    
}
