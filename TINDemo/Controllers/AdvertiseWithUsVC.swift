//
//  AdvertiseWithUsVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 10/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class AdvertiseWithUsVC: UIViewController ,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_mobile: UITextField!
    @IBOutlet weak var txt_companyName: UITextField!
    @IBOutlet weak var txt_website: UITextField!
    @IBOutlet weak var txt_city: UITextField!
    @IBOutlet weak var txt_country: UITextField!
    @IBOutlet weak var txt_editorialissue: UITextField!
     var editorialIssues = ["First","Second","Third","Four"]
        var selectedIssue:String?
     var editorialPicker:UIPickerView?
     var backgroundView:UIView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        // Do any additional setup after loading the view.
        
        self.setPlaceholderColor()
        IQKeyboardManager.shared.enable = true
      
    }
    
    @IBAction func DoneButtonClick(sender: UIButton) {
        
        view.endEditing(true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
      print("Hello World")
      self.view.endEditing(true)
      self.backgroundView?.removeFromSuperview()
    }

  func setPlaceholderColor() {
    
      self.txt_name.placeholderColor(textfieldPlaceholder)
      self.txt_email.placeholderColor(textfieldPlaceholder)
      self.txt_mobile.placeholderColor(textfieldPlaceholder)
      self.txt_companyName.placeholderColor(textfieldPlaceholder)
      self.txt_website.placeholderColor(textfieldPlaceholder)
      self.txt_city.placeholderColor(textfieldPlaceholder)
      self.txt_country.placeholderColor(textfieldPlaceholder)
  }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        print("TextField should begin editing method called")
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_editorialissue
        {
            addPickerSize()
         }
        // became first responder
        print("TextField did begin editing method called")
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("TextField should snd editing method called")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        print("While entering the characters this method gets called")
        print("Search text string = \(string)")
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
       
        print("TextField should return method called")
       
        // may be useful: textField.resignFirstResponder()
        return true
    }
    
     func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return editorialIssues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        self.selectedIssue = editorialIssues[row]
        return editorialIssues[row]
    }
    
    func addPickerSize() {
        
     self.editorialPicker = UIPickerView(frame:CGRect(x: 0, y: self.view.frame.height-216, width: self.view.frame.size.width, height: 216))
    
     self.editorialPicker?.delegate = self
     self.editorialPicker?.dataSource = self
     self.editorialPicker?.backgroundColor = UIColor.white
     self.editorialPicker?.tag = 102
            
    if selectedIssue != nil {
                selectedIssue = self.txt_editorialissue.text
        }
            
            // Adding Button ToolBar
    let toolBar = UIToolbar()
    toolBar.barStyle = .default
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
    toolBar.barTintColor = .white
    toolBar.sizeToFit()
        
    let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.editorialDoneClick))
            
    let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.editorialCancelClick))
            
    toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
            
    backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
    // Change UIView background colour
    // backgroundView?.backgroundColor =  UIColor(white: 1, alpha: 0.5)
            
    backgroundView?.backgroundColor =  UIColor.black.withAlphaComponent(0.2)
    view.isOpaque = false
            
    let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
    backgroundView?.addGestureRecognizer(tap)
    self.view.addSubview(backgroundView!)
            
    self.txt_editorialissue?.inputView = editorialPicker
    self.txt_editorialissue?.inputAccessoryView = toolBar
 }
    
    
 @objc func editorialCancelClick() {
    
        self.view.endEditing(true)
        self.backgroundView?.removeFromSuperview()
    }
    
 @objc func editorialDoneClick() {
    
     if selectedIssue == nil {
        
        self.txt_editorialissue?.text = editorialIssues[0]
        selectedIssue = editorialIssues[0]
      }
     else {
         
      self.txt_editorialissue?.text = selectedIssue
      }
    
     self.view.endEditing(true)
     self.backgroundView?.removeFromSuperview()
    }
 }


