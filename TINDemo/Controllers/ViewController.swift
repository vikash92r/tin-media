//
//  ViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 03/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import Reachability
import AlamofireImage

enum collectionViewsTag:Int
{
    case topMenu = 101, carsole = 102, story = 103, upcoming = 104
}

protocol closeLeftMenuDelegate {
    func  leftMenuClosed()
}

class ViewController: UIViewController ,UIScrollViewDelegate,closeLeftMenuDelegate{
    
    
//     let topmenuCollectionTag = 101
//     let carsoleCollectionTag = 102
//     let storyCollectionTag  = 103
    private let reuseIdentifier = "Cell"
    private let topreuseIdentifier = "topCell"
    private let storyreuseIdentifier = "storyCell"
    private let eventuseIdentifier = "UpComingEventCollectionViewCell"
    
    var refreshControl: UIRefreshControl!
    
    var backgroundView:UIView?
    var arrayelements = ["a","b","c"]
    var topMenuelements = ["Top","International","Domestic","Tourison Malaysia","Transportation"]
    var storyElements = [["1A", "1B", "1C"], ["2A", "2B"], ["3A", "3B", "3C", "3D", "3E"]]
    var selectedIndex = 0
    var topNewsModels = [NewsModel]()
    var homeTopBanners = homeTopBannerModel()
    var homeMidleAdd = HomeMidleAddModel()
    var topMenuList = [MenuModel]()
    var newList = [NewsModel]()
    var recentNewsModels = [NewsModel]()
    var newsListModels = [[NewsModel]]()
    var sectionKeysArray = [String]()
    
    
    var homeCombinNewsArray = [[NewsModel]]()
    var homeComineSection = [String]()
    var topNewsSectionsKeysArray = [String]()
    var topNewsListModels = [NewsModel]()
    
    var popularNewsListModels = [NewsModel]()
    var popularSectionKeyArray = [String]()
    
    var mustWatchSectionKeysArray = [String]()
    var mustWatchListModels = [NewsModel]()
    
    var eventSectionKeysArray = [String]()
    var eventListModels = [NewsModel]()
    var  sampleTextField:UITextField!
    
    var selectedIndexPath:IndexPath?
    var didLayoutFlag:Bool = false
    @IBOutlet weak var bannerHeightConstraint: NSLayoutConstraint!
    var heightOfHomeBanner:CGFloat = 0
    
    @IBOutlet weak var topMenuCollectionView: UICollectionView!
    @IBOutlet weak var pageview: UIPageControl!
    @IBOutlet weak var carsoleCollectionView: UICollectionView!
    @IBOutlet weak var upcomingCollectionView: UICollectionView!
    
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var topStoryCollectionView: UICollectionView!
    @IBOutlet weak var BannerViewHeightConsraint: NSLayoutConstraint!
    var heightOfBanner:CGFloat = 0
    
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var upcommingBannerViewHeight: NSLayoutConstraint!
    var heightOfupcomingEvent:CGFloat = 0
    
    @IBOutlet weak var upcomingPageControl: UIPageControl!
    @IBOutlet weak var storyCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var upcomingHeadingView: UIView!
    
    @IBOutlet weak var viewMoreUpcoming: UIButton!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var upcomingView: UIView!
    var menuButton:UIButton!
    
    var bannerOriginalHeight = 0.0
    
    let updateUserPictureNotification = NotificationCenter.default
    
    override func viewDidLoad() {
        
    super.viewDidLoad()

    self.setStyleTabBar()
    self.setNavigationBarItem()
    self.carsoleCollectionView.layer.cornerRadius = 10
       
        self.topMenuCollectionView.isHidden = true
        self.mainScrollView.isHidden = true
        self.carsoleCollectionView.isHidden = true
        self.upcomingView.isHidden = true
        
    if Utility.isConnected()
        {
          // self.callTopNewsApi()
        DispatchQueue.global(qos: .background).async {
        print("This is run on the background queue")
        DispatchQueue.main.async {
        print("This is run on the main queue, after the previous code in outer block")
            
        self.addRefreshControll()
            
        self.callHomeMidleAdd()
        self.callHomeTopBanner()
        self.callRecentNewsApi()
        self.callMenuApi()
        // self.callTopNewsApi()
        self.callHomeMenuClick()

            }
        }
        }
        else
        {
            Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
        }
        
        heightOfBanner = self.BannerViewHeightConsraint.constant
        heightOfHomeBanner = self.bannerHeightConstraint.constant
        heightOfupcomingEvent  = self.upcommingBannerViewHeight.constant
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        bannerImageView.isUserInteractionEnabled = true
        bannerImageView.addGestureRecognizer(tapGestureRecognizer)
        
      //  self.userUpdateProfilePic()
        
         updateUserPictureNotification.addObserver(self, selector: #selector(userUpdateProfilePic), name: Notification.Name("userUpdateProfilePic"), object: nil)
        
        print("userdata = \(UserDetails.sharedInstance.userdata)")
        
    }
    
    func addRefreshControll()
    {
        self.mainScrollView.alwaysBounceVertical = true
        self.mainScrollView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.scrollView.addSubview(refreshControl)
        
    }
    
    @objc func didPullToRefresh() {

      print("Refersh")
        DispatchQueue.global(qos: .background).async {
        print("This is run on the background queue")
        DispatchQueue.main.async
        {
            self.callHomeMidleAdd()
            self.callHomeTopBanner()
            self.callRecentNewsApi()
           // self.callMenuApi()
          //  self.callHomeMenuClick()
            
           //***
//            self.callTopNewsAllApi()
//            self.callMustWatchApi()
//            self.callTopNewsApi()
//            self.callEventApi()
           //**
        
        if self.selectedIndex == 0
        {
            self.callHomeMenuClick()
           // self.callTopNewsApi()
            self.BannerViewHeightConsraint.constant =  self.heightOfBanner
            self.pageview.isHidden = false
            self.bannerImageView.isHidden = false
            self.upcomingPageControl.isHidden = false
            self.bannerHeightConstraint.constant = self.heightOfHomeBanner
            self.upcommingBannerViewHeight.constant = self.heightOfupcomingEvent
            self.upcomingHeadingView.isHidden = false
            self.upcomingCollectionView.isHidden = false
                               
        }
     else
        {
            self.callNewsListApi(id: self.topMenuList[self.selectedIndex ].id ?? "")
                    
            self.bannerHeightConstraint.constant = 0.0
            self.BannerViewHeightConsraint.constant =  0.0
            self.upcommingBannerViewHeight.constant = 0.0
            self.pageview.isHidden = true
            self.upcomingPageControl.isHidden = true
            self.upcomingHeadingView.isHidden = true
            self.upcomingCollectionView.isHidden = true
        }

            self.topMenuCollectionView.reloadData()
            self.topMenuCollectionView.layoutIfNeeded()
            let indexPath = IndexPath(item: self.selectedIndex, section: 0)
            
            self.topMenuCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                 //  collectionView.scrollToItem(at: indexPath, at: .left, animated: false)

            self.carsoleCollectionView.layoutIfNeeded()
            self.upcomingCollectionView.layoutIfNeeded()
            
            
            // For End refrshing
            self.refreshControl?.endRefreshing()
          }
      }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("banner tapped")
        
        if self.homeTopBanners.urlPath.count > 0
        {
         guard let vedioUrl =  URL(string: self.homeTopBanners.urlPath[0]) else { return  }
                    UIApplication.shared.open(vedioUrl)
        }
        // Your action
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           self.tabBarController?.tabBar.isHidden = false
       }
    

    func callHomeMenuClick()
    {
        self.callTopNewsAllApi()
        self.callMustWatchApi()
        self.callTopNewsApi()
        self.callEventApi()
      //  self.callPopularNewsAllApi()
    
    }
    
    func leftMenuClosed()
    {
        backgroundView?.removeFromSuperview()
    }
    
    func setNavigationBarItem()
    {
       
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu_icon.png"), for: .normal)
        btn1.imageEdgeInsets.left = 5.0
        btn1.imageEdgeInsets.right = 5.0
        btn1.imageEdgeInsets.top = 5.0
        btn1.imageEdgeInsets.bottom = 5.0
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)
        btn1.imageView?.contentMode = .scaleAspectFit
        menuButton = btn1
        let item1 = UIBarButtonItem(customView: btn1)
//        let currWidth = item1.customView?.widthAnchor.constraint(equalToConstant: 50)
//           currWidth?.isActive = true
//        let currHeight = item1.customView?.heightAnchor.constraint(equalToConstant: 50)
//           currHeight?.isActive = true


        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "logo.png"), for: .normal)
        btn2.imageEdgeInsets.left = 0.0
        btn2.imageEdgeInsets.right = 0.0
        btn2.imageEdgeInsets.top = 0.0
        btn2.imageEdgeInsets.bottom = 0.0
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
        btn2.imageView?.contentMode = .scaleAspectFit
        let item2 = UIBarButtonItem(customView: btn2)
//        let currWidth = item2.customView?.widthAnchor.constraint(equalToConstant: 20)
//                  currWidth?.isActive = true
//        let currHeight = item2.customView?.heightAnchor.constraint(equalToConstant: 20)
//                  currHeight?.isActive = true
//
        
        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
        
        let btn3 = UIButton(type: .custom)
        btn3.setImage(UIImage(named: "profile.png"), for: .normal)
        
        if UserDetails.sharedInstance.email.count > 0
        {
         Alamofire.request(UserDetails.sharedInstance.image, method: .get).validate()
            
           .responseData(completionHandler: { (responseData) in
                               
            btn3.layer.cornerRadius = btn3.bounds.size.width/2
            //btn3.layer.borderColor = UIColor(red:0.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1).CGColor as CGColorRef
            // btn3.layer.borderWidth = 2.0
            btn3.layer.masksToBounds = false
            btn3.clipsToBounds = true
            btn3.setImage(UIImage(data: responseData.data!), for: .normal)
            DispatchQueue.main.async {
                    // Refresh you views
                }
            })
        }
        
//        btn3.imageEdgeInsets.left = 5.0
//        btn3.imageEdgeInsets.right = 5.0
//        btn3.imageEdgeInsets.top = 5.0
//        btn3.imageEdgeInsets.bottom = 5.0
        
        btn3.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn3.addTarget(self, action: #selector(self.profile), for:.touchUpInside)
        btn3.imageView?.contentMode = .scaleAspectFit
        let item3 = UIBarButtonItem(customView: btn3)
        
        let currWidth = item3.customView?.widthAnchor.constraint(equalToConstant: 35)
        currWidth?.isActive = true
               
        let currHeight = item3.customView?.heightAnchor.constraint(equalToConstant: 35)
        currHeight?.isActive = true

        let btn4 = UIButton(type: .custom)
        btn4.setImage(UIImage(named: "search.png"), for: .normal)
        btn4.imageEdgeInsets.left = 5.0
        btn4.imageEdgeInsets.right = 5.0
        btn4.imageEdgeInsets.top = 5.0
        btn4.imageEdgeInsets.bottom = 5.0
        btn4.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn4.addTarget(self, action: #selector(self.search), for: .touchUpInside)
        btn4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        btn4.imageView?.contentMode = .scaleAspectFit
        let item4 = UIBarButtonItem(customView: btn4)
        

        self.navigationItem.setRightBarButtonItems([item4,item3],animated: true)
        
    }
    
    @objc func userUpdateProfilePic()
    {
        
        self.navigationItem.rightBarButtonItems?.removeAll()
               
        let btn3 = UIButton(type: .custom)
        btn3.setImage(UIImage(named: "profile.png"), for: .normal)
        
        if UserDetails.sharedInstance.email.count > 0
        {
            Alamofire.request(UserDetails.sharedInstance.image, method: .get)
                     .validate()
                     .responseData(completionHandler: { (responseData) in
                        
                        btn3.layer.cornerRadius = btn3.bounds.size.width/2
                        //btn3.layer.borderColor = UIColor(red:0.0/255.0, green:122.0/255.0, blue:255.0/255.0, alpha:1).CGColor as CGColorRef
                       // btn3.layer.borderWidth = 2.0
                        btn3.layer.masksToBounds = false
                        btn3.clipsToBounds = true
                        btn3.setImage(UIImage(data: responseData.data!), for: .normal)
                         DispatchQueue.main.async {
                             // Refresh you views
                         }
                     })
        }
        
//        btn3.imageEdgeInsets.left = 5.0
//        btn3.imageEdgeInsets.right = 5.0
//        btn3.imageEdgeInsets.top = 5.0
//        btn3.imageEdgeInsets.bottom = 5.0
        btn3.frame = CGRect(x: 0, y: 0, width: 20 , height: 20)
        btn3.addTarget(self, action: #selector(self.profile), for:.touchUpInside)
        btn3.imageView?.contentMode = .scaleAspectFit
        
       
        let item3 = UIBarButtonItem(customView: btn3)
        
        let currWidth = item3.customView?.widthAnchor.constraint(equalToConstant: 35)
        currWidth?.isActive = true
        
        let currHeight = item3.customView?.heightAnchor.constraint(equalToConstant: 35)
        currHeight?.isActive = true
        
        let btn4 = UIButton(type: .custom)
        btn4.setImage(UIImage(named: "search.png"), for: .normal)
        btn4.imageEdgeInsets.left = 5.0
        btn4.imageEdgeInsets.right = 5.0
        btn4.imageEdgeInsets.top = 5.0
        btn4.imageEdgeInsets.bottom = 5.0
        btn4.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn4.addTarget(self, action: #selector(self.search), for: .touchUpInside)
        btn4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        btn4.imageView?.contentMode = .scaleAspectFit
        let item4 = UIBarButtonItem(customView: btn4)
               
        self.navigationItem.setRightBarButtonItems([item4,item3],animated: true)
               
    }
    
    @objc func openMenu()  {
        print("firstName")
        
        let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems, fromController: "ViewController" )
        rootView.delegate = self
        
        let mainWindow = UIApplication.shared.keyWindow!
        backgroundView = UIView(frame: CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height))
           
        backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainWindow.addSubview(backgroundView!);
        
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
        
    }
    @objc func secondName()  {
       
        print("secondName")
    }
    
    @objc func profile()  {
        
        print("profile")
        if UserDetails.sharedInstance.email.count > 0
        {
        let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(pdvc, animated: true)
        }
        else
        {
          let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
          self.navigationController?.pushViewController(pdvc, animated: true)
        }
        
    }
   
    @objc func search()
    {

        print("search")
        let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(pdvc, animated: true)
 
       }
    

    func setStyleTabBar()
    {
        self.tabBarController?.tabBar.layer.shadowColor = UIColor.gray.cgColor
        self.tabBarController?.tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.tabBarController?.tabBar.layer.shadowRadius = 5
        self.tabBarController?.tabBar.layer.shadowOpacity = 1
        self.tabBarController?.tabBar.layer.masksToBounds = false
             
    }
    
    func callTopNewsApi()
    {
        print("callTopNewsApi")
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = homeCombineCategoryPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
        
           self.mainScrollView.isHidden = false
        let swiftyJsonVar = JSON(result)
         
        let newbyId = Utility.getHomeCombineCategory(dict: swiftyJsonVar)
        
        self.homeCombinNewsArray = newbyId.newsModelArray
        self.homeComineSection = newbyId.sectionKeyArray
            
            
        self.newsListModels = []
        self.sectionKeysArray = []
            
        if self.topNewsListModels.count > 0
            {
                self.newsListModels.append(self.topNewsListModels)
            //  self.sectionKeysArray.append("TOP NEWS")
                self.sectionKeysArray.append(kTopStoriesTitle)
            }
            
            if self.mustWatchListModels.count > 0
            {
                self.newsListModels.append(self.mustWatchListModels)
                 self.sectionKeysArray.append("MUST WATCH")
            }
            
            if self.popularNewsListModels.count > 0
                {
                    self.newsListModels.append(self.popularNewsListModels)
                    self.sectionKeysArray.append("Popular News")
                }
            
            if self.homeCombinNewsArray.count > 0
            {
                self.newsListModels = self.newsListModels + self.homeCombinNewsArray
                self.sectionKeysArray = self.sectionKeysArray + self.homeComineSection
            }
            
            if self.eventListModels.count > 0
            {
//                self.newsListModels.append([self.eventListModels[0]])
//                self.sectionKeysArray.append("UPCOMING EVENTS")
            }
            
            self.topStoryCollectionView.reloadData()
            
            self.scrollViewHeightConstraint.constant =  self.heightOfBanner + self.heightOfHomeBanner + self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height + self.heightOfupcomingEvent
            self.view.layoutIfNeeded()
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
          }
    }
    
    func callTopNewsAllApi()
        {
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
            let myurl = kTopNewsPath
            var param =  Parameters()
            param["api_key"] = kApiKey
            
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
              self.mainScrollView.isHidden = false
            let swiftyJsonVar = JSON(result)
            
            self.topNewsListModels = Utility.getTopNewsModels(dict: swiftyJsonVar)
//                self.newsListModels.append(self.topNewsListModels)
//                self.sectionKeysArray.append("Top News")
                
            self.newsListModels = []
            self.sectionKeysArray = []
                          
            if self.topNewsListModels.count > 0
              {
                self.newsListModels.append(self.topNewsListModels)
              //  self.sectionKeysArray.append("TOP NEWS")
                self.sectionKeysArray.append(kTopStoriesTitle)
              }
                          
            if self.mustWatchListModels.count > 0
              {
                self.newsListModels.append(self.mustWatchListModels)
                self.sectionKeysArray.append("MUST WATCH")
             }
                
            if self.popularNewsListModels.count > 0
              {
                self.newsListModels.append(self.popularNewsListModels)
                self.sectionKeysArray.append("Popular News")
             }
                        
            if self.homeCombinNewsArray.count > 0
              {
                self.newsListModels = self.newsListModels + self.homeCombinNewsArray
                self.sectionKeysArray = self.sectionKeysArray + self.homeComineSection
              }
                          
            if self.eventListModels.count > 0
              {
//                self.newsListModels.append([self.eventListModels[0]])
//                self.sectionKeysArray.append("UPCOMING EVENTS")
             }
                
                self.topStoryCollectionView.reloadData()
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
          }
    }
    
     func callPopularNewsAllApi()
            {
                let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
                spinnerActivity?.labelText = kLodindMessage
                let myurl = kPopularNewsPath
                var param =  Parameters()
                param["api_key"] = kApiKey
                
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                
                let swiftyJsonVar = JSON(result)
    
                self.popularNewsListModels = Utility.getPopularNews(dict: swiftyJsonVar)
    
                self.newsListModels = []
                self.sectionKeysArray = []
                              
                if self.topNewsListModels.count > 0
                    {
                        self.newsListModels.append(self.topNewsListModels)
                      //  self.sectionKeysArray.append("TOP NEWS")
                        self.sectionKeysArray.append(kTopStoriesTitle)
                    }
                              
                if self.mustWatchListModels.count > 0
                    {
                        self.newsListModels.append(self.mustWatchListModels)
                        self.sectionKeysArray.append("MUST WATCH")
                    }
                    
              if self.popularNewsListModels.count > 0
                    {
                        self.newsListModels.append(self.popularNewsListModels)
                        self.sectionKeysArray.append("Popular News")
                    }
                                       
            if self.homeCombinNewsArray.count > 0
                    {
                        self.newsListModels = self.newsListModels + self.homeCombinNewsArray
                        self.sectionKeysArray = self.sectionKeysArray + self.homeComineSection
                    }
                              
            if self.eventListModels.count > 0
                    {
//                       self.newsListModels.append(self.eventListModels)
//                        self.newsListModels.append([self.eventListModels[0]])
//                       self.sectionKeysArray.append("UPCOMING EVENTS")
                    }
                    
                self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height + self.upcommingBannerViewHeight.constant
                
            self.topStoryCollectionView.reloadData()
                
            
//                print("story collectionview  height = \(self.topStoryCollectionView)")
//            self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//            self.scrollView.layoutIfNeeded()
//                    self.topStoryCollectionView.layoutIfNeeded()
//            self.view.layoutIfNeeded()
                
//                CGFloat height = myCollectionView.collectionViewLayout.collectionViewContentSize.height
//                heightConstraint.constant = height
//                self.view.setNeedsLayout() Or self.view.layoutIfNeeded()
                
               self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant
                self.view.layoutIfNeeded()
                
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    func callHomeTopBanner()
    {
        print("callHomeTopBanner")
            let myurl = kHomeTopBannerPath
            var param =  Parameters()
            param["api_key"] = kApiKey

            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param)
               { (result, error) in

                 self.mainScrollView.isHidden = false
                  let swiftyJsonVar = JSON(result)
                  let homeBannermodels =   Utility.getHomeTopBannerModel(dict: swiftyJsonVar)
                
                for mstring in homeBannermodels.banner
                   {

                }
                
                self.homeTopBanners = homeBannermodels
                if self.homeTopBanners.banner.count > 0
                {
                    UserDetails.sharedInstance.bannerUrl = self.homeTopBanners.banner[0]
                    UserDetails.sharedInstance.bannerAddUrl = self.homeTopBanners.urlPath[0]
                    Utility.saveUserDetails()
                    self.bannerImageView.sd_setImage(with: URL(string: self.homeTopBanners.banner[0] ), placeholderImage: UIImage(named: "placeholder.png"))
                }
                
               
//                 print("story collectionview  height = \(self.topStoryCollectionView)")
//                  self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//                 self.scrollView.layoutIfNeeded()
//                    self.topStoryCollectionView.layoutIfNeeded()
//                 self.view.layoutIfNeeded()
                
//                self.pageview.numberOfPages = self.homeTopBanners.banner.count
//                self.carsoleCollectionView.reloadData()
                
                self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant
                               self.view.layoutIfNeeded()
            }
    }
    
    func callHomeMidleAdd()
        {
            let myurl = kHomeMidleAdd
            var param =  Parameters()
            param["api_key"] = kApiKey

        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param)
                { (result, error) in

                    let swiftyJsonVar = JSON(result)
                    let homeAdd =   Utility.getHomeMidleAdd(dict: swiftyJsonVar)
                    for mstring in homeAdd.banner
                       {
                        print("image Name = \(mstring)")
                      }
                                        
                    self.homeMidleAdd = homeAdd
//                    if self.homeTopBanners.banner.count > 0
//                    {
//                    self.bannerImageView.sd_setImage(with: URL(string: self.homeTopBanners.banner[0] ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
//                    }
                    
//
//                     print("story collectionview  height = \(self.topStoryCollectionView)")
//                      self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//                     self.scrollView.layoutIfNeeded()
//
//                    self.topStoryCollectionView.layoutIfNeeded()
//                    self.view.layoutIfNeeded()
                    
                    self.mainScrollView.isHidden = false
                    
                    self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant + self.upcommingBannerViewHeight.constant
                                   self.view.layoutIfNeeded()
                }
        }
    
    
    func callRecentNewsApi()
        {
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
            let myurl = kRecentNewsPath
            var param =  Parameters()
            param["api_key"] = kApiKey
            
            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
             self.mainScrollView.isHidden = false
             self.carsoleCollectionView.isHidden = false
                
            let swiftyJsonVar = JSON(result)
                        
            self.recentNewsModels = Utility.getTopNewsRecents(dict: swiftyJsonVar)
            for model in self.topNewsModels
            {
                print("image = \(model.image!)")
                print("days_ago = \(model.days_ago!)")
                print("news_heading = \(model.news_heading!)")
            }
                
                self.pageview.numberOfPages = self.recentNewsModels.count
                self.carsoleCollectionView.reloadData()
               
               
//                 print("story collectionview  height = \(self.topStoryCollectionView)")
//                  self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//                 self.scrollView.layoutIfNeeded()
//                    self.topStoryCollectionView.layoutIfNeeded()
//                 self.view.layoutIfNeeded()
//
                self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant + self.upcommingBannerViewHeight.constant
                self.view.layoutIfNeeded()
                
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
              }
        }
    
    
    
    func callEventApi()
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kEventsPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
            
            self.upcomingView.isHidden = false
            self.mainScrollView.isHidden = false
            let swiftyJsonVar = JSON(result)
            self.eventListModels = Utility.getEvents(dict: swiftyJsonVar)
            
            self.upcomingPageControl.numberOfPages = self.eventListModels.count
//            self.newsListModels.append(self.eventListModels)
//           // self.newsListModels.append([self.eventListModels[0]])
//            self.sectionKeysArray.append("UPCOMING EVENTS")
        
            self.newsListModels = []
            self.sectionKeysArray = []
                      
            if self.topNewsListModels.count > 0
                {
                    self.newsListModels.append(self.topNewsListModels)
                  //  self.sectionKeysArray.append("TOP NEWS")
                    self.sectionKeysArray.append(kTopStoriesTitle)
                }
                      
            if self.mustWatchListModels.count > 0
                {
                    self.newsListModels.append(self.mustWatchListModels)
                    self.sectionKeysArray.append("MUST WATCH")
                }
            
            if self.popularNewsListModels.count > 0
                    {
                       self.newsListModels.append(self.popularNewsListModels)
                       self.sectionKeysArray.append("Popular News")
                    }
                      
            if self.homeCombinNewsArray.count > 0
                {
                    self.newsListModels = self.newsListModels + self.homeCombinNewsArray
                    self.sectionKeysArray = self.sectionKeysArray + self.homeComineSection
                }
                      
            if self.eventListModels.count > 0
                {
//                    self.newsListModels.append([self.eventListModels[0]])
//                    self.sectionKeysArray.append("UPCOMING EVENTS")
                }
    
            self.topStoryCollectionView.reloadData()
            self.upcomingCollectionView.reloadData()
            
//
//             print("story collectionview  height = \(self.topStoryCollectionView)")
//              self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//             self.scrollView.layoutIfNeeded()
//                self.topStoryCollectionView.layoutIfNeeded()
//             self.view.layoutIfNeeded()
            self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant + self.upcommingBannerViewHeight.constant
            self.view.layoutIfNeeded()
          }
    }
    
    
    func callMustWatchApi()
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kMustWatchPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
          self.mainScrollView.isHidden = false
        let swiftyJsonVar = JSON(result)
        self.mustWatchListModels = Utility.getNewsListByMustWatch(dict: swiftyJsonVar)
        
//            self.newsListModels.append(self.mustWatchListModels )
//            self.sectionKeysArray.append("Must Watch")
//
            
        self.newsListModels = []
        self.sectionKeysArray = []
                                 
        if self.topNewsListModels.count > 0
                {
                    self.newsListModels.append(self.topNewsListModels)
                  //  self.sectionKeysArray.append("TOP NEWS")
                    self.sectionKeysArray.append(kTopStoriesTitle)
                }
                                 
            if self.mustWatchListModels.count > 0
                {
                    self.newsListModels.append(self.mustWatchListModels)
                    self.sectionKeysArray.append("MUST WATCH")
                }
                           
            if self.popularNewsListModels.count > 0
                {
                    self.newsListModels.append(self.popularNewsListModels)
                    self.sectionKeysArray.append("Popular News")
                }
            
            if self.homeCombinNewsArray.count > 0
                {
                    self.newsListModels = self.newsListModels + self.homeCombinNewsArray
                    self.sectionKeysArray = self.sectionKeysArray + self.homeComineSection
                }
                                 
            if self.eventListModels.count > 0
                {
//                    self.newsListModels.append([self.eventListModels[0]])
//                    self.sectionKeysArray.append("UPCOMING EVENTS")
               }
                         
            self.topStoryCollectionView.reloadData()
            
          
//             print("story collectionview  height = \(self.topStoryCollectionView)")
//              self.scrollViewHeightConstraint.constant = self.bannerHeightConstraint.constant   + self.BannerViewHeightConsraint.constant  +  self.topStoryCollectionView.contentSize.height
//            self.scrollView.layoutIfNeeded()
//                self.topStoryCollectionView.layoutIfNeeded()
//             self.view.layoutIfNeeded()
            
            self.scrollViewHeightConstraint.constant = self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height  + self.bannerHeightConstraint.constant + self.BannerViewHeightConsraint.constant + self.upcommingBannerViewHeight.constant
            self.view.layoutIfNeeded()
        }
    }
    
    
    func callMenuApi() {
       
        let myurl = kMenuListPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
      
            self.mainScrollView.isHidden = false
    
        let swiftyJsonVar = JSON(result)
        self.topMenuList =   Utility.getMenuList(dict: swiftyJsonVar)
        for menu in self.topMenuList
               {
                  print(menu.cat_name!)
                  print(menu.id!)
                }
            
        let menuItem = MenuModel()
        menuItem.cat_name = "Home"

        self.topMenuList.insert(menuItem, at: 0)
           
            self.topMenuCollectionView.isHidden = false
           
           // self.callTopNewsApi()
        self.selectedIndex = 0
//            if self.topMenuList.count > 0
//            {
//                 self.callNewsListApi(id: self.topMenuList[0].id ?? "")
//            }
        
            self.topMenuCollectionView.reloadData()
        }
    }
    
    func callNewsListApi(id:String)
    {
        print("callNewsListApi With Id")
//        let myurl =  kNewsListPath + id
       let myurl =  kNewsListByParentId + id
        var param =  Parameters()
        param["api_key"] = kApiKey
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

        let swiftyJsonVar = JSON(result)
            
         print("newsListApi = \(swiftyJsonVar)")
//            self.newList =   Utility.getNewsList(dict: swiftyJsonVar)
            
        let newbyId = Utility.getNewByParentId(dict: swiftyJsonVar)
        self.newsListModels = newbyId.newsModelArray
        self.sectionKeysArray = newbyId.sectionKeyArray
    
        self.mainScrollView.isHidden = false
            
        self.topStoryCollectionView.reloadData()
            
        self.scrollViewHeightConstraint.constant = 0.0 + 0.0 + 0.0 + self.topStoryCollectionView.collectionViewLayout.collectionViewContentSize.height
        self.view.layoutIfNeeded()
            
             MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    @IBAction func menuOpenAction(_ sender: Any) {
        
       let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems, fromController: "ViewController" )
               // rootView.categoryList = DBManager.shared.loadCategories()
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
        
    }
    
       // delegete method of category

   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pagewidth:CGFloat = self.carsoleCollectionView.frame.size.width;
        let currentPage:Int = Int(self.carsoleCollectionView.contentOffset.x/pagewidth);
        self.pageview.currentPage = currentPage;
        
        let pagewidth2:CGFloat = self.upcomingCollectionView.frame.size.width;
            let currentPage2:Int = Int(self.upcomingCollectionView.contentOffset.x/pagewidth2);
            self.upcomingPageControl.currentPage = currentPage2;
    }
    
    @IBAction func actionViewMore(_ sender: UIButton)
    {
        print("view more button click")
    
//        let buttonPostion = (sender as AnyObject).convert((sender as AnyObject).bounds.origin, to: self.topStoryCollectionView)
        print("button tag =\(sender.tag)")
        var indexV = sender.tag
        
        if indexV == 601
        {
            print("cat id =\(String(describing: self.eventListModels[0].event_cat))")
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
            pdvc.catId = self.eventListModels[0].event_cat ?? ""
            pdvc.catName = "UPCOMING EVENTS"
            self.navigationController?.pushViewController(pdvc, animated: true)
        }
        else
        {
            
        if self.newsListModels[sender.tag][0].modelType == ktopnewsTitle
            {
                print("cat id =\(String(describing: self.newsListModels[sender.tag][0].categoryId))")
                let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
                pdvc.catId = self.newsListModels[sender.tag][0].categoryId ?? ""
                pdvc.catName = self.sectionKeysArray[sender.tag]
                self.navigationController?.pushViewController(pdvc, animated: true)
                       
            }
        else  if self.newsListModels[sender.tag][0].modelType == kEventsTitle
            {
                print("cat id =\(String(describing: self.newsListModels[sender.tag][0].event_cat))")
                
                let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
               pdvc.catId = self.newsListModels[sender.tag][0].event_cat ?? ""
                pdvc.catName = self.sectionKeysArray[sender.tag]
                self.navigationController?.pushViewController(pdvc, animated: true)
            }
        
        else if self.newsListModels[sender.tag][0].modelType == kMustWatchTitle
            {

                let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
                pdvc.previousController = "Home"
                self.navigationController?.pushViewController(pdvc, animated: true)
            }
        else
          {
             print("cat id =\(String(describing: self.newsListModels[sender.tag][0].categoryId))")
            
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MoreNewsVC") as! MoreNewsVC
            pdvc.catId = self.newsListModels[sender.tag][0].categoryId ?? ""
            pdvc.catName = self.sectionKeysArray[sender.tag]
            self.navigationController?.pushViewController(pdvc, animated: true)
                       
          }
            
        }
//        }
        
    }
 
}

extension ViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        print("TextField should begin editing method called")
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        // became first responder
        print("TextField did begin editing method called")
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("TextField should snd editing method called")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        print("While entering the characters this method gets called")
        print("Search text string = \(string)")
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        print("TextField should return method called")
        // may be useful: textField.resignFirstResponder()
        return true
    }

}


extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView.tag == collectionViewsTag.story.rawValue {

            return self.newsListModels.count
       }
        
      return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView.tag == collectionViewsTag.topMenu.rawValue {
            
            return self.topMenuList.count

        }
        else if collectionView.tag == collectionViewsTag.carsole.rawValue {
            
            return self.recentNewsModels.count
        }
        else if collectionView.tag == collectionViewsTag.story.rawValue {
            
              return self.newsListModels[section].count
        }
        else if collectionView.tag == collectionViewsTag.upcoming.rawValue {
            
            return self.eventListModels.count
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == collectionViewsTag.story.rawValue {
            
            if self.newsListModels[indexPath.section][indexPath.row].modelType == kEventsTitle {
                
                return CGSize(width: collectionView.bounds.width , height: (collectionView.bounds.width*2)/3)
              }
            else {
                let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
              }
            }
        let kWhateverHeightYouWant = collectionView.frame.height
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
        
//        return CGSize(width: UIScreen.main.bounds.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == collectionViewsTag.carsole.rawValue {
            
             let cell = collectionView
                    .dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeBannerCollectionViewCell
            cell.backgroundColor = .black
        
                  // cell style
            cell.contentView.layer.cornerRadius = 10.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
        
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 10.0
                     // cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            cell.layer.backgroundColor = UIColor.clear.cgColor

                    
            if self.recentNewsModels.count > 0 {
        
            let lblDayString = (self.recentNewsModels[indexPath.row].from ?? "") + "    " + (self.recentNewsModels[indexPath.row].days_ago ?? "")
                    
            let fromTime = self.recentNewsModels[indexPath.row].from ?? ""
            cell.lbl_timeDetails.attributedText = Utility.getAttributedString(color:UIColor.orange,colorText: fromTime, fullString: lblDayString)
            cell.lbl_newsheading.text = self.recentNewsModels[indexPath.row].news_heading
                    
            cell.image.sd_setImage(with: URL(string: self.recentNewsModels[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                
            }
                  
            cell.overlayView.layer.sublayers?.removeAll()
                
            let gradientLayer: CAGradientLayer! = CAGradientLayer()
            gradientLayer.frame = collectionView.frame
            gradientLayer.colors = [ UIColor.clear.cgColor,UIColor.black.cgColor]
            gradientLayer.locations = [0.05, 1.0]
            cell.overlayView.layer.addSublayer(gradientLayer)
    
            return cell
                
        }
        else if collectionView.tag == collectionViewsTag.upcoming.rawValue {
            
            let cell = collectionView
                              .dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeBannerCollectionViewCell
            cell.backgroundColor = .black
                  
                            // cell style
            cell.contentView.layer.cornerRadius = 10.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
                  
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 10.0
                               // cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            cell.layer.backgroundColor = UIColor.clear.cgColor

            cell.lbl_timeDetails.text = self.eventListModels[indexPath.row].date_from!
            cell.lbl_newsheading.text = self.eventListModels[indexPath.row].title!
            print("date from = \(self.eventListModels[indexPath.row].date_from)")
            print("description  = \(self.eventListModels[indexPath.row].title)")
                                   
            cell.image.sd_setImage(with: URL(string: self.eventListModels[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                            
            cell.overlayView.layer.sublayers?.removeAll()
                          
            let gradientLayer: CAGradientLayer! = CAGradientLayer()
            gradientLayer.frame = collectionView.frame
            gradientLayer.colors = [ UIColor.clear.cgColor,UIColor.black.cgColor]
            gradientLayer.locations = [0.05, 1.0]
            cell.overlayView.layer.addSublayer(gradientLayer)
              
            return cell
        }
    else if collectionView.tag == collectionViewsTag.story.rawValue {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storyreuseIdentifier, for: indexPath) as! StoryCollectionViewCell
                    
                    // cell style
            cell.imageView.layer.cornerRadius = 10.0
            cell.imageView.layer.borderWidth = 1.0
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
            cell.imageView.layer.masksToBounds = true
        
            cell.imageView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.imageView.layer.backgroundColor = UIColor.clear.cgColor
                    
           if self.newsListModels[indexPath.section][indexPath.row].modelType == ktopnewsTitle {
            
              let lblDayString = (self.newsListModels[indexPath.section][indexPath.row].from ?? "") + "    " +  (self.newsListModels[indexPath.section][indexPath.row].days_ago ?? "")
                                     
              let fromTime = self.newsListModels[indexPath.section][indexPath.row].from ?? ""
              cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText:fromTime, fullString: lblDayString)
                                                     
              cell.lbl_detail.text = self.newsListModels[indexPath.section][indexPath.row].news_heading
                                           
              cell.imageView.sd_setImage(with: URL(string: self.newsListModels[indexPath.section][indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                        
             cell.playVideosIcon.isHidden = true
            }
//           else if self.newsListModels[indexPath.section][indexPath.row].modelType == kEventsTitle
//                {
//
//                  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: eventuseIdentifier, for: indexPath) as! UpComingEventCollectionViewCell
//
//                 cell.upcomingImageContainerView.layer.cornerRadius = 10.0
//                 cell.upcomingImageContainerView.layer.borderWidth = 2.0
//                 cell.upcomingImageContainerView.layer.borderColor = UIColor.clear.cgColor
//                 cell.upcomingImageContainerView.layer.masksToBounds = true
//                 cell.eventImage.clipsToBounds = true
//
//                 cell.lbl_datedeatail.text = self.newsListModels[indexPath.section][indexPath.row].date_from!
//                 cell.lbl_description.text = self.newsListModels[indexPath.section][indexPath.row].title!
//                 print("date from = \(self.newsListModels[indexPath.section][indexPath.row].date_from)")
//                 print("description  = \(self.newsListModels[indexPath.section][indexPath.row].title)")
//
//                cell.eventImage.sd_setImage(with: URL(string: self.newsListModels[indexPath.section][indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
//
//                    return cell
//                }
//
        else if self.newsListModels[indexPath.section][indexPath.row].modelType == kMustWatchTitle {
            
                 let lblDayString = (self.newsListModels[indexPath.section][indexPath.row].from ?? "tin.media") + "    " +  (self.newsListModels[indexPath.section][indexPath.row].days_ago ?? "")
                                     
                let fromTime = self.newsListModels[indexPath.section][indexPath.row].from ?? "tin.media"
                cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText:fromTime, fullString: lblDayString)
                                                     
                cell.lbl_detail.text = self.newsListModels[indexPath.section][indexPath.row].news_heading
                                           
                cell.imageView.sd_setImage(with: URL(string: self.newsListModels[indexPath.section][indexPath.row].image_url!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                cell.playVideosIcon.isHidden = false
                        
                }
            else {
                   
                 let lblDayString = (self.newsListModels[indexPath.section][indexPath.row].from ?? "") + "    " +  (self.newsListModels[indexPath.section][indexPath.row].days_ago ?? "")

                 let fromTime = self.newsListModels[indexPath.section][indexPath.row].from ?? ""
                 cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText:fromTime, fullString: lblDayString)

                 cell.lbl_detail.text = self.newsListModels[indexPath.section][indexPath.row].news_heading

                 cell.imageView.sd_setImage(with: URL(string: self.newsListModels[indexPath.section][indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                    cell.playVideosIcon.isHidden = true
                }
                
             return cell
          }
       else
          {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topreuseIdentifier, for: indexPath) as! TopMenuCollectionViewCell
                        
                cell.lbl_Menu.text = self.topMenuList[indexPath.row].cat_name?.uppercased()
                cell.underlineView.layoutIfNeeded()
           
               if selectedIndex == indexPath.row {
                
                  cell.underlineView.isHidden = false
                  cell.underlineView.backgroundColor  = UIColor.gray
                  cell.lbl_Menu.textColor = selectedMenuTextColor
                }
            else {
                
                  cell.underlineView.isHidden = true
                // cell.lbl_Menu.textColor = .lightGray
                  cell.lbl_Menu.textColor = unselectedUnderLineTextColor
             }
                
          return cell
        }
        
    }
    

    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == collectionViewsTag.topMenu.rawValue {
            
            if Utility.isConnected() {
                
                selectedIndex = indexPath.row
                if indexPath.row == 0 {
                    
                  selectedIndex = 0
                  self.callTopNewsApi()
                    
                  self.BannerViewHeightConsraint.constant =  heightOfBanner
                  self.pageview.isHidden = false
                  self.bannerImageView.isHidden = false
                  self.upcomingPageControl.isHidden = false
                  self.bannerHeightConstraint.constant = heightOfHomeBanner
                  self.upcommingBannerViewHeight.constant = heightOfupcomingEvent
                  self.upcomingHeadingView.isHidden = false
                 self.upcomingCollectionView.isHidden = false
                    
                }
              else {
                    
                  self.callNewsListApi(id: self.topMenuList[selectedIndex].id ?? "")
                    
                  self.bannerHeightConstraint.constant = 0.0
                  self.BannerViewHeightConsraint.constant =  0.0
                  self.upcommingBannerViewHeight.constant = 0.0
                  self.pageview.isHidden = true
                  self.upcomingPageControl.isHidden = true
                  self.upcomingHeadingView.isHidden = true
                  self.upcomingCollectionView.isHidden = true
                }
                print("Internet is available.")
                   // ...
            }
            else {
                
                 Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
            }
            
           self.topMenuCollectionView.reloadData()
           collectionView.layoutIfNeeded()
           collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        //  collectionView.scrollToItem(at: indexPath, at: .left, animated: false)

           self.carsoleCollectionView.layoutIfNeeded()
           self.upcomingCollectionView.layoutIfNeeded()
   
        }
        else if collectionView.tag == collectionViewsTag.upcoming.rawValue {
            
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            pdvc.newId = self.eventListModels[indexPath.row].id
            pdvc.catName = "UPCOMING EVENTS"
            self.navigationController?.pushViewController(pdvc, animated: true)
            
        }
        else if collectionView.tag == collectionViewsTag.story.rawValue {
            
            if Utility.isConnected() {
                
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                
            if self.newsListModels[indexPath.section][indexPath.row].modelType == kEventsTitle {
                
                pdvc.newId = self.newsListModels[indexPath.section][indexPath.row].id
                pdvc.catName = "UPCOMING EVENTS"
                self.navigationController?.pushViewController(pdvc, animated: true)
              }
            else if self.newsListModels[indexPath.section][indexPath.row].modelType == kMustWatchTitle {
                
                let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "VedioDetailsVC") as! VedioDetailsVC
                pdvc.vedioId = self.newsListModels[indexPath.section][indexPath.row].id
                pdvc.fromController = "Home"
                self.navigationController?.pushViewController(pdvc, animated: true)
             }
            else
             {
                pdvc.newId = self.newsListModels[indexPath.section][indexPath.row].id
                self.navigationController?.pushViewController(pdvc, animated: true)
              }
           
                print("Internet is available.")
                   // ...
            }
            else {
                  Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
            }
        }
        else {
         // to show detail of slider news
            let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            pdvc.newId = self.recentNewsModels[indexPath.row].id
            self.navigationController?.pushViewController(pdvc, animated: true)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.size.width, height: 48)
//    }

       
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if collectionView.tag == collectionViewsTag.story.rawValue {
            
        switch kind {

            
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
//            if selectedIndex == 0
//            {
//                headerView.label.text = "Top News"
//            }
//            else
//            {
                 headerView.label.text = self.sectionKeysArray[indexPath.section].uppercased()
                 headerView.buttonViewMore.tag = indexPath.section
//            }
            
            return headerView

        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
              
    
            if indexPath.section < self.homeMidleAdd.banner.count {
                
                footerView.addImage.sd_setImage(with: URL(string: self.homeMidleAdd.banner[indexPath.section]), placeholderImage: UIImage(named: "placeholder.png"))
                if self.homeMidleAdd.add_url[indexPath.section].count > 0
                {
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(footerAddTapped(tapGestureRecognizer:)))
                    footerView.addImage.isUserInteractionEnabled = true
                    footerView.addImage.addGestureRecognizer(tapGestureRecognizer)
                    footerView.addImage.tag = indexPath.section
                }
            }

            return footerView

        default:
            return UICollectionReusableView()
        }
            
        }
        return  UICollectionReusableView()
    }
    
    @objc func footerAddTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let index = tappedImage.tag
        
        guard let addImageUrl =  URL(string: self.homeMidleAdd.add_url[index]) else { return  }
        UIApplication.shared.open(addImageUrl)
        
      }
    
    //********* footer height
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForFooterInSection section: Int) -> CGSize
    {
        
     if collectionView.tag == collectionViewsTag.story.rawValue {
                    
        if section < self.homeMidleAdd.banner.count {
                        
            if self.selectedIndex == 0  && self.homeMidleAdd.addType[section] == "image"{
                
            print("indexpath section = \(section)")
            print("banner count = \(self.homeMidleAdd.banner.count)")
            print("url = \(self.homeMidleAdd.banner[section])")
            
           //*****   To find the Image Size of particular url

            let imageSize = Utility.getImageSizeFromUrlString(urlString: self.homeMidleAdd.banner[section])
            let imageHeight = imageSize.height
            let imageWidth =  imageSize.width
              
            if imageWidth == CGSize.zero.width {
                
                    return CGSize.zero
            }
                
           //******  end to find image size
                
            let aspectRatio =  imageHeight / imageWidth
                
            let imageViewWidth = (collectionView.frame.width - 10)
            let imageViewHeight = imageViewWidth*aspectRatio
                
            print("screen width = \(imageViewWidth)")
            print("screen height = \(imageViewHeight)")
                
            return CGSize(width: imageViewWidth, height: imageViewHeight)
            
                // return CGSize(width: (collectionView.frame.width - 10), height: 100.0)
             }
           else if self.selectedIndex == 0  && self.homeMidleAdd.addType[section] == "google_add"
           {
               return CGSize.zero
           }
         else {
                    return CGSize.zero
              }
          }
       else {
            return CGSize.zero
         }
      }
        
     return CGSize.zero
        
   }
    
    private func getImageheight(index:Int ,  failure: ((String) -> Void)? = nil ,completion: (() -> CGSize)? = nil) {
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()

        Alamofire.request(self.homeMidleAdd.banner[index]).responseImage { response in
                                    
         debugPrint(response)

         print(response.request)
         print(response.response)
         debugPrint(response.result)

        if let image = response.result.value {
                                        
         print("image downloaded: \(image)")
        // print("image height = \(image.size.height)")
         self.bannerOriginalHeight  = Double(CGFloat(image.size.height))
                                              
         }
        
         dispatchGroup.leave()
        }
            
        dispatchGroup.notify(queue: DispatchQueue.main) {
            completion?()
        }
    }
}


//
class HeaderCollectionReusableView: UICollectionReusableView {
  @IBOutlet var label: UILabel!
    @IBOutlet weak var buttonViewMore: UIButton!
}

class FooterCollectionReusableView: UICollectionReusableView {
  @IBOutlet weak var label: UILabel!
    @IBOutlet weak var addImage: UIImageView!
   // @IBOutlet weak var addImage: RatioBasedImageView!
    
}

/*
@IBDesignable
public class RatioBasedImageView : UIImageView {
    /// constraint to maintain same aspect ratio as the image
    private var aspectRatioConstraint:NSLayoutConstraint? = nil

    // This makes it use the correct size in Interface Builder
    public override func prepareForInterfaceBuilder() {
        invalidateIntrinsicContentSize()
    }

    @IBInspectable
    var maxAspectRatio: CGFloat = 999 {
        didSet {
            updateAspectRatioConstraint()
        }
    }

    @IBInspectable
    var minAspectRatio: CGFloat = 0 {
        didSet {
            updateAspectRatioConstraint()
        }
    }


    required public init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        self.setup()
    }

    public override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }

    public override init(image: UIImage!) {
        super.init(image:image)
        self.setup()
    }

    public override init(image: UIImage!, highlightedImage: UIImage?) {
        super.init(image:image,highlightedImage:highlightedImage)
        self.setup()
    }

    override public var image: UIImage? {
        didSet { self.updateAspectRatioConstraint() }
    }

    private func setup() {
        self.updateAspectRatioConstraint()
    }

    /// Removes any pre-existing aspect ratio constraint, and adds a new one based on the current image
    private func updateAspectRatioConstraint() {
        // remove any existing aspect ratio constraint
        if let constraint = self.aspectRatioConstraint {
            self.removeConstraint(constraint)
        }
        self.aspectRatioConstraint = nil

        if let imageSize = image?.size, imageSize.height != 0 {
            var aspectRatio = imageSize.width / imageSize.height
            aspectRatio = max(minAspectRatio, aspectRatio)
            aspectRatio = min(maxAspectRatio, aspectRatio)

            let constraint = NSLayoutConstraint(item: self, attribute: .width,
                                       relatedBy: .equal,
                                       toItem: self, attribute: .height,
                                       multiplier: aspectRatio, constant: 0)
            constraint.priority = .required
            self.addConstraint(constraint)
            self.aspectRatioConstraint = constraint
        }
    }
}
*/
