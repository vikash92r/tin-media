//
//  SignupVc.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 12/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift

class SignupVc: UIViewController {

    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_confirmPassword: UITextField!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_title: UITextField!
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        IQKeyboardManager.shared.enable = true
        // Do any additional setup after loading the view.
        
     self.cardView.layer.shadowColor = UIColor.black.cgColor
     self.cardView.layer.shadowOpacity = 0.2
     self.cardView.layer.shadowOffset = .zero
     self.cardView.layer.shadowRadius = 10

     self.setPlaceholderColor()
     self.setNavigationBarItem()
        
     IQKeyboardManager.shared.enable = true
  }
    
  
 func setNavigationBarItem() {
    
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "backIcon.png"), for: .normal)
    //               btn1.imageEdgeInsets.left = 5.0
    //               btn1.imageEdgeInsets.right = 5.0
    //               btn1.imageEdgeInsets.top = 5.0
    //               btn1.imageEdgeInsets.bottom = 5.0
        btn1.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                
        btn1.addTarget(self, action: #selector(self.actionBackButtonTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
                
        let currWidth = item1.customView?.widthAnchor.constraint(equalToConstant: 50)
                           currWidth?.isActive = true
        let currHeight = item1.customView?.heightAnchor.constraint(equalToConstant: 50)
                           currHeight?.isActive = true

        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "logo.png"), for: .normal)
        btn2.imageEdgeInsets.left = 0.0
        btn2.imageEdgeInsets.right = 0.0
        btn2.imageEdgeInsets.top = 0.0
        btn2.imageEdgeInsets.bottom = 0.0
        btn2.imageView?.contentMode = .scaleAspectFill
                           
        btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                   //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
        btn2.isEnabled = false
        btn2.isUserInteractionEnabled = false
        
        let item2 = UIBarButtonItem(customView: btn2)
        
        let currWidth2 = item2.customView?.widthAnchor.constraint(equalToConstant: 50)
                                          currWidth?.isActive = true
        
        let currHeight2 = item2.customView?.heightAnchor.constraint(equalToConstant: 50)
                                          currHeight?.isActive = true
        
        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
        
    }
    
    @objc func actionBackButtonTapped() {
        
           self.navigationController?.popViewController(animated: true)
       }
    
    func setPlaceholderColor() {
        
        self.txt_name.placeholderColor(textfieldPlaceholder)
        self.txt_lastName.placeholderColor(textfieldPlaceholder)
        self.txt_email.placeholderColor(textfieldPlaceholder)
        self.txt_password.placeholderColor(textfieldPlaceholder)
        self.txt_confirmPassword.placeholderColor(textfieldPlaceholder)
        self.txt_title.placeholderColor(textfieldPlaceholder)
    }

    
    @IBAction func actionBackSingIn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionRegistrationSubmit(_ sender: Any) {
        
        let firstName = Utility.stringAfterTrim(name: txt_name.text!)
        let lName = Utility.stringAfterTrim(name:txt_lastName.text!)
        let email = Utility.stringAfterTrim(name:txt_email.text!)
        let password = Utility.stringAfterTrim(name:txt_password.text!)
        let confirmpassword = Utility.stringAfterTrim(name:txt_confirmPassword.text!)
        let title = Utility.stringAfterTrim(name: txt_title.text!)
        
        guard title.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterTitle)
            return
         }
        
        guard firstName.count != 0 else {
              
            Utility.displayAlert(title: kAppTitle, message: kEnterFirstName)
            return
         }
        
        guard lName.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterLastName)
            return
        }
        
        guard email.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterEmail)
            return
        }
        
        guard !Utility.isNotValidEmail(email: email) else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterValidEmail)
            return
        }
        
        guard password.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterPassword)
            return
        }
        
        guard confirmpassword.count != 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterConfirmPassword)
            return
        }
        
        if password  != confirmpassword {
            
            Utility.displayAlert(title: kAppTitle, message: kPasswordNotMatched)
            return
        }
        
        
        print("fname = \(firstName)")
        print("last name = \(lName)")
        print("email = \(email)")
        print("password = \(password)")
        
        self.callRegistrationApi(firstName: firstName, lastName: lName, email: email, password: password,title: title)
        
    }
    
    
    func callRegistrationApi(firstName:String,lastName:String,email:String,password:String,title:String)
    {
       
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
                                  spinnerActivity?.labelText = kLodindMessage
        print("uuid Value =\(kUUIDValue)")
        let myurl = kRegistrationApi
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
        param["fname"]  = firstName
        param["lname"] = lastName
        param["email"] = email
        param["password"] = password
        param["title"] = title
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                       
        let swiftyJsonVar = JSON(result)
        print("response = \(swiftyJsonVar["response"])")
        switch swiftyJsonVar["response"] {
            
            case "200":
                
                      if swiftyJsonVar["inserted"] == "ok" {
                        
                        self.callLoginApi(name: email, password: password)
                     }
                    
            case "404":
                          if swiftyJsonVar["emailexist"] == "YES" {
                            
                             Utility.displayAlert(title: kAppTitle, message: emailAlreadyExist)
                          }
                         print("not found")
                   
              default:   Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
            }
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }
    }
    
    
    func callLoginApi(name:String,password:String)
        {
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
            print("uuid Value =\(kUUIDValue)")
           
            let myurl = kLoginApi
            print("path url = \(myurl)")
            var param =  Parameters()
            param["api_key"] = kApiKey
            param["email"] = name
            param["password"] = password

            WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

            let swiftyJsonVar = JSON(result)

            print("result = \(swiftyJsonVar)")
            print("response = \(swiftyJsonVar["response"])")
                       
            switch swiftyJsonVar["response"] {
             
            case "200":
                        guard let dictObject = swiftyJsonVar["userDetails"].arrayObject else { return  }
    //                        print("fname = \(dictObject["title"] as! String)")
                            
                        let mdict = dictObject as!  [[String:String]]
                        UserDetails.sharedInstance.firstName = mdict[0]["fname"] ?? ""
                        UserDetails.sharedInstance.email = mdict[0]["email"] ?? ""
                        UserDetails.sharedInstance.lastName = mdict[0]["last_name"] ?? ""
                        UserDetails.sharedInstance.company = mdict[0]["company"] ?? ""
                        UserDetails.sharedInstance.designation = mdict[0]["designation"] ?? ""
                        UserDetails.sharedInstance.password = mdict[0]["password"] ?? ""
                        UserDetails.sharedInstance.userdata = mdict[0]["userdata"] ?? ""
                        UserDetails.sharedInstance.name = mdict[0]["name"] ?? ""
                        UserDetails.sharedInstance.image = mdict[0]["image"] ?? ""
                        UserDetails.sharedInstance.usercontact = mdict[0]["usercontact"] ?? ""
                        UserDetails.sharedInstance.accountType = "NormalLogin"
                        print("first name = \(mdict[0]["fname"])")
                  
                        Utility.saveUserDetails()
                            
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        Utility.tabBarAsRootViewController()
    //                  self.present(alert, animated: true, completion: nil)

                                    
            case "0":
                        let alert = UIAlertController(title: kAppTitle, message:"User Does Not  Exist.", preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)

                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
    //                                       self.navigationController?.popViewController(animated: true)
                        }))
                                   
                
            default:     Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
                
                }
                            
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                            
             }
        }
        
                
}
