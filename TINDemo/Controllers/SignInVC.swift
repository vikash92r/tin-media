//
//  SignInVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 13/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Alamofire
import GoogleSignIn
import FBSDKLoginKit
import FacebookLogin
import FacebookCore

class SignInVC: UIViewController ,GIDSignInDelegate {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.setPlaceholderColor()
        self.setNavigationBarItem()
        
        IQKeyboardManager.shared.enable = true
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        // Do any additional setup after loading the view.
    }
    
    func setNavigationBarItem()
           {
              
               let btn1 = UIButton(type: .custom)
               btn1.setImage(UIImage(named: "backIcon.png"), for: .normal)
//               btn1.imageEdgeInsets.left = 5.0
//               btn1.imageEdgeInsets.right = 5.0
//               btn1.imageEdgeInsets.top = 5.0
//               btn1.imageEdgeInsets.bottom = 5.0
               btn1.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            
              btn1.addTarget(self, action: #selector(self.actionBackButtonTapped), for: .touchUpInside)
               let item1 = UIBarButtonItem(customView: btn1)
            
                    let currWidth = item1.customView?.widthAnchor.constraint(equalToConstant: 50)
                       currWidth?.isActive = true
                    let currHeight = item1.customView?.heightAnchor.constraint(equalToConstant: 50)
                       currHeight?.isActive = true

               let btn2 = UIButton(type: .custom)
               btn2.setImage(UIImage(named: "logo.png"), for: .normal)
               btn2.imageEdgeInsets.left = 0.0
               btn2.imageEdgeInsets.right = 0.0
               btn2.imageEdgeInsets.top = 0.0
               btn2.imageEdgeInsets.bottom = 0.0
               btn2.imageView?.contentMode = .scaleAspectFill
               
               btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
       //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
               btn2.isEnabled = false
               btn2.isUserInteractionEnabled = false
               let item2 = UIBarButtonItem(customView: btn2)

               self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
           }
      @objc func actionBackButtonTapped()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setPlaceholderColor()
       {
          self.txt_username.placeholderColor(textfieldPlaceholder)
          self.txt_password.placeholderColor(textfieldPlaceholder)
       }
    @IBAction func actionForgotPassword(_ sender: Any) {
        
        let fpvc = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(fpvc, animated: true)
    }
    
    @IBAction func actionLoginWithFacebook(_ sender: Any) {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
    
        FacebookLoginManager.sharedInstance.delegate = self
    FacebookLoginManager.sharedInstance.callLoginMangerWithCompletion { (result , error) in
                  print("successfully login")
             MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
              }
    }
    
    @IBAction func actionLoginWithGoogle(_ sender: Any) {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
               spinnerActivity?.labelText = kLodindMessage
        
        GIDSignIn.sharedInstance()?.delegate = self as? GIDSignInDelegate
        GIDSignIn.sharedInstance()?.signIn()
        
        
    }
    
    @IBAction func actionLogin(_ sender: Any) {
//        Utility.displayAlert(title: kAppTitle, message: "Coming soon...")
//        return;
        let name = Utility.stringAfterTrim(name: txt_username.text!)
         let password = Utility.stringAfterTrim(name: txt_password.text!)
       
        
        guard name.count != 0 else {
            Utility.displayAlert(title: kAppTitle, message: "Pleas enter your email id.")
            return
        }
        guard password.count != 0 else {
            Utility.displayAlert(title: kAppTitle, message: "Pleas enter password.")
            return
        }

        
        self.callLoginApi(name: name, password: password)
    }
    @IBAction func actionSignup(_ sender: Any) {
            let signupvc = self.storyboard!.instantiateViewController(withIdentifier: "SignupVc") as! SignupVc
            self.navigationController?.pushViewController(signupvc, animated: true)
    }
    
    func callLoginApi(name:String,password:String)
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        print("uuid Value =\(kUUIDValue)")
       
        let myurl = kLoginApi
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
        param["email"] = name
        param["password"] = password

        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

        let swiftyJsonVar = JSON(result)

        print("result = \(swiftyJsonVar)")
        print("response = \(swiftyJsonVar["response"])")
                   
        switch swiftyJsonVar["response"] {
            case "200":
                
                        guard let dictObject = swiftyJsonVar["userDetails"].arrayObject else { return  }
                        
                        let mdict = dictObject as!  [[String:String]]
                        UserDetails.sharedInstance.firstName = mdict[0]["fname"] ?? ""
                        UserDetails.sharedInstance.email = mdict[0]["email"] ?? ""
                        UserDetails.sharedInstance.lastName = mdict[0]["last_name"] ?? ""
                        UserDetails.sharedInstance.company = mdict[0]["company"] ?? ""
                        UserDetails.sharedInstance.designation = mdict[0]["designation"] ?? ""
                        UserDetails.sharedInstance.password = mdict[0]["password"] ?? ""
                        UserDetails.sharedInstance.userdata = mdict[0]["userdata"] ?? ""
                        UserDetails.sharedInstance.name = mdict[0]["name"] ?? ""
                        UserDetails.sharedInstance.image = mdict[0]["image"] ?? ""
                        UserDetails.sharedInstance.usercontact = mdict[0]["usercontact"] ?? ""
                        UserDetails.sharedInstance.accountType = "NormalLogin"
                        print("first name = \(mdict[0]["fname"])")
                       
                        Utility.saveUserDetails()
                        
                         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        Utility.tabBarAsRootViewController()
                                
            case "0":
                        let alert = UIAlertController(title: kAppTitle, message:"Invalid email or password.", preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)

                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//                                       self.navigationController?.popViewController(animated: true)
                        }))
                               
            
            default:      Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
            
            }
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
         }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!) {
        
         if let error = error {
           if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
             print("The user has not signed in before or they have since signed out.")
           } else {
             print("\(error.localizedDescription)")
           }
             MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
           return
         }
         // Perform any operations on signed in user here.
       
         let userId = user.userID                  // For client-side use only!
         let idToken = user.authentication.idToken // Safe to send to the server
         let fullName = user.profile.name ?? ""
         let firstName = user.profile.givenName ?? ""
         let lastName = user.profile.familyName ?? ""
         let email = user.profile.email ?? ""
         let image = user.profile.imageURL(withDimension: 50)?.absoluteString ?? ""
       
        print("user id = \(String(describing: userId))")
        print("idToken = \(String(describing: idToken))")
        print("full name  = \(String(describing: fullName))")
        let url = URL(string: image)
        guard let data = try? Data(contentsOf: url!) else { return  }
        
        var imageData  = Array<Data>()
        imageData.append(data)
        
        let pathUrl = kSocialLoginApi
        
        let parameters: Parameters = [
        "api_key" : "8a90436bdef6e286465a918d089a9ca1",
        "fname":firstName,
        "lname": lastName,
        "email":email
            ]
            
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
                               spinnerActivity?.labelText = kLodindMessage
        Utility.performMultipartRequest(urlString: pathUrl, fileName: "image", params: parameters, imageDataArray: imageData, accessToken: nil) { (response, Error) in
        
        if Error == nil {
                         
          print("image upload")
                                
          switch response?["response"] {
            
            case "200":
                                                    
                       UserDetails.sharedInstance.accountType = "google"
                       Utility.saveUserDetails()
//                                            Utility.displayAlert(title: kAppTitle, message: "Login successfully updated."
                                                     Utility.tabBarAsRootViewController()
            case "404":
                        Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                        
            default:
                                           
                        break
                }
                               
            }
          
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
       }
    }
       
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,withError error: Error!) {
        
           print("diddisconnect with")
         // Perform any operations when the user disconnects from app here.
         // ...
       }
    
   
}
