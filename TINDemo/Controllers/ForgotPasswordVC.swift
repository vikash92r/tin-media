//
//  ForgotPasswordVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 13/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Alamofire


class ForgotPasswordVC: UIViewController {
   
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var cardView: UIView!
    
    override func viewDidLoad()
     {
        
        super.viewDidLoad()
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.setPlaceholderColor()
        
        IQKeyboardManager.shared.enable = true
        
        self.setNavigationBarItem()
    
        // Do any additional setup after loading the view.
    }
    
    
    func setNavigationBarItem() {
           
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "backIcon.png"), for: .normal)
                // btn1.setBackgroundImage(UIImage(named: "backIcon.png"), for: .normal)
//               btn1.imageEdgeInsets.left = 5.0
//               btn1.imageEdgeInsets.right = 5.0
//               btn1.imageEdgeInsets.top = 5.0
//                btn1.imageEdgeInsets.bottom = 5.0
        btn1.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                   
        btn1.addTarget(self, action: #selector(self.actionBackButtonTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        btn1.imageView?.contentMode = .scaleAspectFill
        let currWidth = item1.customView?.widthAnchor.constraint(equalToConstant: 50)
        currWidth?.isActive = true
        let currHeight = item1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true

        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "logo.png"), for: .normal)
        btn2.imageEdgeInsets.left = 0.0
        btn2.imageEdgeInsets.right = 0.0
        btn2.imageEdgeInsets.top = 0.0
        btn2.imageEdgeInsets.bottom = 0.0
        btn2.imageView?.contentMode = .scaleAspectFill
                              
        btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                      //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
        btn2.isEnabled = false
        btn2.isUserInteractionEnabled = false
        let item2 = UIBarButtonItem(customView: btn2)

        let currWidth2 = item2.customView?.widthAnchor.constraint(equalToConstant: 50)
                                             currWidth?.isActive = true
        let currHeight2 = item2.customView?.heightAnchor.constraint(equalToConstant: 50)
                                             currHeight?.isActive = true
        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
            
        }
       
       @objc func actionBackButtonTapped() {
        
              self.navigationController?.popViewController(animated: true)
          }
    

    func setPlaceholderColor() {
        
             self.txt_email.placeholderColor(textfieldPlaceholder)
          }
    
    
   
    @IBAction func actionSubmit(_ sender: Any) {
        
        let email = Utility.stringAfterTrim(name: txt_email.text!)
         
         guard email.count != 0 else {
            
             Utility.displayAlert(title: kAppTitle, message: "Pleas enter email.")
             return
         }
        
        guard !Utility.isNotValidEmail(email: email) else {
            
            Utility.displayAlert(title: kAppTitle, message: "Please enter valid email")
            return
        }
        
        self.callForgotApi(email: email)
        
    }
    
    func callForgotApi(email:String) {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        print("uuid Value =\(kUUIDValue)")
      
        let myurl = kForgotApi
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
        param["email"] = email
                           
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                
        let swiftyJsonVar = JSON(result)

        print("response = \(swiftyJsonVar["response"])")
                           
        switch swiftyJsonVar["response"] {
            
          case "200":
                     let alert = UIAlertController(title: kAppTitle, message:"Password sent to your email.", preferredStyle: UIAlertController.Style.alert)
                     self.present(alert, animated: true, completion: nil)

                     alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    
                        self.navigationController?.popViewController(animated: true)
                     }))

                                        
           case "0":
                      let alert = UIAlertController(title: kAppTitle, message:"Email does not exist.", preferredStyle: UIAlertController.Style.alert)
                      self.present(alert, animated: true, completion: nil)
                      alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
        //                                       self.navigationController?.popViewController(animated: true)
                     }))
                                       
           default:   Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
            
            }

                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
             //  Utility.tabBarAsRootViewController()
            }

    }
    
}
