//
//  SubscribeNewsletterVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 06/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Alamofire
import SDWebImage
import Firebase

class SubscribeNewsletterVC: UIViewController {

    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.btnSubmit.layer.cornerRadius = 4
        self.setPlaceholderColor()
        self.setNavigationBarItem()
        IQKeyboardManager.shared.enable = true
        
        self.bannerImageView.sd_setImage(with: URL(string: UserDetails.sharedInstance.bannerUrl ), placeholderImage: UIImage(named: "placeholder.png"))
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        bannerImageView.isUserInteractionEnabled = true
        bannerImageView.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
       {
           print("banner tapped")
           
        if  UserDetails.sharedInstance.bannerAddUrl.count > 0
           {
            guard let addUrl =  URL(string:  UserDetails.sharedInstance.bannerAddUrl) else { return  }
                       UIApplication.shared.open(addUrl)
           }
           // Your action
       }
    
     func setNavigationBarItem() {
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "backIcon.png"), for: .normal)
        
        btn1.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                       
        btn1.addTarget(self, action: #selector(self.actionBackButtonTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        btn1.imageView?.contentMode = .scaleAspectFill
        let currWidth = item1.customView?.widthAnchor.constraint(equalToConstant: 50)
                currWidth?.isActive = true
        let currHeight = item1.customView?.heightAnchor.constraint(equalToConstant: 50)
        currHeight?.isActive = true
        
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
                
    }
        
    
    @objc func actionBackButtonTapped(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setPlaceholderColor(){
        
            self.txt_email.placeholderColor(textfieldPlaceholder)
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
        print("submit")
        
        guard   self.txt_email.text?.count ?? 0 > 0 else {
    
            Utility.displayAlert(title: kAppTitle, message: kEnterEmailToSubscribe)
            return
        }
        
        let email = self.txt_email.text ?? ""
        guard !Utility.isNotValidEmail(email: email) else {
            
             Utility.displayAlert(title: kAppTitle, message: kEnterValidEmail)
             return
            }
        
        callSubscribeApi()
        self.view.endEditing(true)
        
    }
    
    func callSubscribeApi()
        {
        
         let myurl = kSubscribePath
            guard   self.txt_email.text?.count ?? 0 > 0 else {
                
                Utility.displayAlert(title: kAppTitle, message: kEnterEmailToSubscribe)
                return
            }
            
            let email = self.txt_email.text ?? ""
            guard !Utility.isNotValidEmail(email: email) else {
                
                    Utility.displayAlert(title: kAppTitle, message: kEnterValidEmail)
                    return
            }
            
                  
            print("path url = \(myurl)")
            var param =  Parameters()
            param["api_key"] = kApiKey
            param["email"] = email
                            
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                                
         let swiftyJsonVar = JSON(result)
                    
          switch swiftyJsonVar["response"] {
            
            case  "200":
                
                        Utility.displayAlert(title: kAppTitle, message: swiftyJsonVar["message"].stringValue)
                
            default:
                        Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
                        break
             }
                    print("result = \(swiftyJsonVar)")
                  
        }
    }
}
