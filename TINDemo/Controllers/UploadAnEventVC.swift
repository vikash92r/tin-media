//
//  UploadAnEventVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 12/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import Alamofire
import SDWebImage

class UploadAnEventVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var txt_PostCategory: UITextField!
    @IBOutlet weak var cardView: UIView!
    var categories = ["First","Second","Third","Four"]
     var subCategory = ["First1","Second2","Third3","Four4"]
    var categoryModelArray = [CategoryModel]()
    var subCategoryModelArray = [TopicModel]()
    var selectedCategory:String?
    var selectedSubCategory:String?
    var editorialPicker:UIPickerView?
    var backgroundView:UIView?
    var selectedTextField:UITextField!
    @IBOutlet weak var txt_title: UITextField!

    @IBOutlet weak var txt_chooseImage: UITextField!
    @IBOutlet weak var txt_location: UITextField!
    @IBOutlet weak var txt_dateFrom: UITextField!
    @IBOutlet weak var txt_dateTo: UITextField!
    @IBOutlet weak var textView_description: UITextView!
    var datePicker:UIDatePicker!
    var selectedDateText:UITextField!
    var delegate:UIViewController!
    var selectedCat = CategoryModel()
    var selectedSubCat = [TopicModel]()
    var dateFrom = ""
    var dateTo = ""
    var fromController = ""
    var eventId = ""
    var selectedEventId = ""
    var selectedEvent:NewsModel!
    
    var selectedImage:UIImage!
    var selectedUploadImage:UIImage!
    var imagePicker = UIImagePickerController()
  
    @IBOutlet weak var fileImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.cornerRadius = CGFloat(kCornerRadiusValue)
        
        self.setPlaceholderColor()
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        // Do any additional setup after loading the view.
        
        self.callCategoryApi()
        self.callSubCategoryApi()
            
        self.textView_description.inputAccessoryView  = self.toolbarWithTarget()
        
    }
    
    func toolbarWithTarget() -> UIToolbar
    {
        // Adding Button ToolBar
               let toolBar = UIToolbar()
               toolBar.barStyle = .default
               toolBar.isTranslucent = true
               toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
               toolBar.barTintColor = .white
               toolBar.sizeToFit()
               
               let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.done))
               
               let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
              
               let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
               
               toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
               toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    @objc func cancelClick()
    {
        self.view.endEditing(true)
         print("Cancel button is clicked")
    }
    
    @objc func done()
    {
        self.view.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        
        if self.fromController.count > 0
        {
            self.title = "Edit Event"
        }
    }
    
    func callEventDetails()  {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kEventDetailPath + self.eventId
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                         
               let swiftyJsonVar = JSON(result)
               print("result = \(swiftyJsonVar)")
                      
                  print("swiftyjsonvar result = \(swiftyJsonVar)")
                  switch swiftyJsonVar["response"]
            {
                          
            case "200" :
                        let eventDetail = Utility.getEventDetail(dict: swiftyJsonVar)
                        self.txt_title.text = eventDetail[0].title
                        self.txt_location.text = eventDetail[0].location
                       // self.txt_PostCategory.text = eventDetail[0].event_cat
                        var index = Int(eventDetail[0].event_cat ?? "0")!
                        if index > 0
                        {
                            index = index  - 1
                        }
                        
                        let m = self.categoryModelArray.filter { (model) -> Bool in
                            model.categoryId == eventDetail[0].event_cat
                        }
                        
                        if m.count > 0
                        {
                        self.txt_PostCategory.text = m[0].cat_name
                        self.selectedCat = m[0]
                        }
                        self.txt_dateTo.text = eventDetail[0].date_to
                        self.txt_dateFrom.text = eventDetail[0].date_from
//                        self.txt_chooseImage.text = eventDetail[0].image
                        self.textView_description.text = eventDetail[0].description?.string
                        self.dateTo = eventDetail[0].date_to ?? ""
                        self.dateFrom = eventDetail[0].date_from ?? ""
                    
                        self.txt_dateFrom.text = Utility.formatedDateStringFromString(dateString: eventDetail[0].date_from ?? "")
                        self.txt_dateTo.text = Utility.formatedDateStringFromString(dateString:  eventDetail[0].date_to ?? "")
                        
                        
                        Alamofire.request(eventDetail[0].image ?? "", method: .get).validate()
                      .responseData(completionHandler: { (responseData) in
                          self.selectedImage = UIImage(data: responseData.data!)
                        self.fileImageView.image = self.selectedImage
                          DispatchQueue.main.async {
                              // Refresh you views
                          }
                      })
                        break
                          
            case "404" :
                        break
                                  
            default:
                    break
                    }
                       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }
        }
        
    
    func callCategoryApi()
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kEventCategoryPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                  
        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        print("swiftyjsonvar result = \(swiftyJsonVar)")
        switch swiftyJsonVar["response"]
            {
                
            case "200" :
                        self.categoryModelArray = Utility.getCategories(dict: swiftyJsonVar)
                      
                        if self.fromController.count > 0
                               {
                                    self.callEventDetails()
                               }
                        break
                
            case "404" :
                         break
                        
            default:
                        break
                }
             MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
        
    }
    
    func callSubCategoryApi()  {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        let myurl = kNewsTopicsPath
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in

        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"]{
            
        case "200" :
                    let topics = Utility.getTopics(dict: swiftyJsonVar)
                    self.subCategoryModelArray = topics
                    break
            
        case "404" :
                     break
                    
        default:
                    break
            }
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
        
    }
    
    func addDatePicker()
    {
        datePicker = UIDatePicker.init()
        datePicker.datePickerMode = UIDatePicker.Mode.date;
        
        self.selectedDateText.inputView = datePicker
    
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.barTintColor = .white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.datelDoneClick))
                      
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                      
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.dateCancelClick))
                      
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
                      
        backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
                     // Change UIView background colour
                     // backgroundView?.backgroundColor =  UIColor(white: 1, alpha: 0.5)
                      
        backgroundView?.backgroundColor =  UIColor.black.withAlphaComponent(0.2)
        view.isOpaque = false
                      
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                  backgroundView?.addGestureRecognizer(tap)
        self.view.addSubview(backgroundView!)
                      

        self.selectedDateText.inputAccessoryView = toolBar
    }
    
    @objc func dateCancelClick(){
            self.view.endEditing(true)
            self.backgroundView?.removeFromSuperview()
        }

    @objc func datelDoneClick(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let dateString = formatter.string(from: datePicker.date)
       self.selectedDateText.text = dateString
        formatter.dateFormat = "yyyy/MM/dd"
        if self.selectedDateText == self.txt_dateFrom
        {
            self.dateFrom = formatter.string(from: datePicker.date)
            // self.selectedDateText.text = self.dateFrom
        }
        else if self.selectedDateText == self.txt_dateTo
        {
            self.dateTo = formatter.string(from: datePicker.date)
          //   self.selectedDateText.text = self.dateTo
        }
        
        
        self.view.endEditing(true)
        self.backgroundView?.removeFromSuperview()
    }
    
    func setPlaceholderColor()
    {
//        self.txt_name.placeholderColor(textfieldPlaceholder)
//        self.txt_email.placeholderColor(textfieldPlaceholder)
//        self.txt_mobile.placeholderColor(textfieldPlaceholder)
//        self.txt_companyName.placeholderColor(textfieldPlaceholder)
//        self.txt_website.placeholderColor(textfieldPlaceholder)
//        self.txt_city.placeholderColor(textfieldPlaceholder)
//        self.txt_country.placeholderColor(textfieldPlaceholder)
    }
    
    @IBAction func actionEventCategoryClick(_ sender: Any) {
        let window = UIApplication.shared.keyWindow!
        self.selectedTextField = self.txt_PostCategory
           
           self.view.endEditing(true)
          // let rootView:PopoverTableView = PopoverTableView.init(frame:self.view.frame)
        let rootView:PopoverTableView = PopoverTableView.init(frame:window.bounds,categoryArray:categoryModelArray ,selectedTextField:kEventCategory,selectedCat:self.selectedCat)
         rootView.selectedCategory = self.selectedCat
          rootView.delegate = self
        
           window.addSubview(rootView)
        self.view.endEditing(true)
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        print("TextField should begin editing method called")
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txt_chooseImage
        {
            self.selectedTextField = textField
           // self.alertshow()
        }
        else if textField == txt_dateTo
        {
            self.selectedDateText = textField
            self.addDatePicker()
        }
        else if textField == txt_dateFrom
        {
             self.selectedDateText = textField
            self.addDatePicker()
        }
        // became first responder
        print("TextField did begin editing method called")
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        print("TextField should snd editing method called")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        print("TextField did end editing method called")
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        print("TextField did end editing with reason method called")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        print("While entering the characters this method gets called")
        print("Search text string = \(string)")
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        print("TextField should clear method called")
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
       
        print("TextField should return method called")

       textField.resignFirstResponder()
        // may be useful: textField.resignFirstResponder()
        return true
    }
    
    
func numberOfComponents(in pickerView: UIPickerView) -> Int {
               return 1
           }
        
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if selectedTextField == txt_PostCategory
     {
      
        return self.categoryModelArray.count
//        return categories.count
    }
  else
     {
        return subCategory.count
     }
           
   }
        
func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
        {
        if selectedTextField == txt_PostCategory
         {
//            print("selected category = \(self.categoryModelArray[row].cat_name)")
//            self.selectedCategory = self.categoryModelArray[row].cat_name
            
//
            return self.categoryModelArray[row].cat_name
//            self.selectedCategory = categories[row]
//            return categories[row]
                
            }
            else
            {
                self.selectedSubCategory = subCategory[row]
                return subCategory[row]
            }
        }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if selectedTextField == txt_PostCategory
        {
            self.selectedCategory = self.categoryModelArray[row].cat_name
            
        }
        
    }
        
        func addPostCategoryPicker()
        {
            self.editorialPicker = UIPickerView(frame:CGRect(x: 0, y: self.view.frame.height-216, width: self.view.frame.size.width, height: 216))
            self.editorialPicker?.delegate = self
            self.editorialPicker?.dataSource = self
            self.editorialPicker?.backgroundColor = UIColor.white
            self.editorialPicker?.tag = 102
                
        if selectedTextField == txt_PostCategory
        {
//            if selectedCategory != nil
//            {
//             selectedCategory = self.txt_PostCategory.text
//            if selectedCategory?.count ?? 0 > 0
//            {
//                // let selectedIndex = categories.index(of: selectedCategory!)
//                var i = 0
//                for cat in self.categoryModelArray{
//
//                if cat.cat_name == selectedCategory{
//
//                    break
//                }
//                    i = i + 1
//                }
//            let selectedIndex = i
//                  //  let selectedIndex = categories.index(of: selectedCategory!)
//                    editorialPicker?.selectRow(selectedIndex, inComponent: 0, animated: true)
//                }
//            }
                
        }
      
                
                // Adding Button ToolBar
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar.barTintColor = .white
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.editorialDoneClick))
                
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.editorialCancelClick))
                
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
                
            backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
               // Change UIView background colour
               // backgroundView?.backgroundColor =  UIColor(white: 1, alpha: 0.5)
                
            backgroundView?.backgroundColor =  UIColor.black.withAlphaComponent(0.2)
            view.isOpaque = false
                
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            backgroundView?.addGestureRecognizer(tap)
            self.view.addSubview(backgroundView!)
                
            if selectedTextField == txt_PostCategory
            {
            self.txt_PostCategory?.inputView = editorialPicker
            self.txt_PostCategory?.inputAccessoryView = toolBar
            }
           
        }
        
        
         @objc func editorialCancelClick()
         {
               self.view.endEditing(true)
               self.backgroundView?.removeFromSuperview()
           }
        @objc func editorialDoneClick()
            {
                
            if selectedTextField == txt_PostCategory
            {
                if selectedCategory == nil
                {
//                    self.txt_PostCategory?.text = categories[0]
//                   selectedCategory = categories[0]
                    
                    self.txt_PostCategory?.text = self.categoryModelArray[0].cat_name
                    selectedCategory = self.categoryModelArray[0].cat_name
                }
                else
                {
//                  self.txt_PostCategory?.text = selectedCategory
                    self.txt_PostCategory?.text = selectedCategory
                    
                }
            }
        
            
                
                 self.view.endEditing(true)
                self.backgroundView?.removeFromSuperview()
            }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
           print("Hello World")
           self.view.endEditing(true)
        self.backgroundView?.removeFromSuperview()
    }

    
     func alertshow(){
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
               
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
               
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
            
    }
        
    
    func openCamera()  {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
         self.imagePicker.allowsEditing = true
         self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
//                self.imagePicker.cameraCaptureMode = .photo
//                self.imagePicker.modalPresentationStyle = .fullScreen
         self.present(self.imagePicker,animated: true,completion: nil)
        }
       else {
                Utility.displayAlert(title: kAppTitle, message: "camera is not available")
        }
    }

 func openGallary() {
    
        self.imagePicker.allowsEditing = false
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
        
    
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

     if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
        if selectedTextField == self.txt_chooseImage {
            
            self.selectedImage = pickedImage
            self.fileImageView.image = pickedImage
        }
      }

     dismiss(animated: true, completion: nil)
    
    }
           
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
            dismiss(animated: true, completion: nil)
        }
        

   
    @IBAction func upladImageAction(_ sender: Any) {
        
        self.view.endEditing(true)
        self.selectedTextField = self.txt_chooseImage
        self.alertshow()
       
    }
    
    @IBAction func callUploadEventApi(_ sender: Any) {
        
        print("selc")
        if validation() {
            
        var pathUrl = kAddEventPath + UserDetails.sharedInstance.userdata
            
        if self.fromController.count > 0 {
            
            pathUrl = kEditEventPath + self.eventId
        }
        
        print("pathurl = \(pathUrl)")
    
        let keys =  self.selectedSubCat.reduce(""){ (Result,topicModel) in
    
        if Result.count > 0 {
            return Result + "," + (topicModel.key ?? "")
        }
       else {
            return topicModel.key ?? ""
          }
       }
            
    var allkeys = [String]()
            
    for subCat in self.selectedSubCat  {
               
        allkeys.append(subCat.key ?? "")
      }
             
    print("all keys = \(keys)")
             

    let selectedCategoryIDs = self.selectedCat.categoryId ?? ""
            
    print("all selected category id = \(selectedCategoryIDs)")

    if selectedImage != nil {
        
            if let data = self.selectedImage.jpegData(compressionQuality: 1) {
        
            let parameters: Parameters = [
            "api_key" : "8a90436bdef6e286465a918d089a9ca1",
            "event_cat": selectedCategoryIDs,
            "event_heading": self.txt_title.text ?? "" ,
            "news_topic": allkeys,
            "event_desc":self.textView_description.text ?? "",
            "location": self.txt_location.text ?? "",
            "date_from" : self.dateFrom,
            "date_to" : self.dateTo
            ]
                        
            var imageData  = Array<Data>()
                       
            imageData.append(self.selectedImage.jpegData(compressionQuality: 1)!)

            //
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity?.labelText = kLodindMessage
            
            Utility.performMultipartRequestAddEvent(urlString: pathUrl, fileName: "event_pic", params: parameters, imageDataArray: imageData, accessToken: nil) { (response, Error) in
                            
            if Error == nil {
                
            print("image upload")
            print("result =\(response?["response"]!)")
                
            switch response?["response"]    {
                
            case "200":
                        if response?["inserted"] == 1   {
                            
                        let dialogMessage = UIAlertController(title: kAppTitle, message: "Event Added Successfully.", preferredStyle: .alert)
                                                                             
                            // Create OK button with action handler
                        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                            
                        print("Ok button tapped")
                                               
                        let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
                        self.navigationController?.pushViewController(pdvc, animated: true)
                        
                     })
                                                                             
                    //Add OK and Cancel button to dialog message
                    dialogMessage.addAction(ok)
                    self.present(dialogMessage, animated: true, completion: nil)
                        // Utility.displayAlert(title: kAppTitle, message: "Event Added Successfully.")
                    }
                  else if response?["updated"] == 1 {
                            
                    let dialogMessage = UIAlertController(title: kAppTitle, message: "Event Updated Successfully.", preferredStyle: .alert)
                                                                                                        
                    // Create OK button with action handler
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                                                       
                    print("Ok button tapped")
                    (self.delegate as! MyEventVC).eventUpdated()
//                   let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
                    self.navigationController?.popViewController(animated: true)
                                                                                
                    })
                                                                                                        
                    //Add OK and Cancel button to dialog message
                    dialogMessage.addAction(ok)
                    self.present(dialogMessage, animated: true, completion: nil)
                                                     
                    }
                                    
        case "404":
                    Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
            
        default:
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    break
            }
        }
            
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            }
          }
        }
     else
       {
               
        let parameters: Parameters = [
        
        "api_key" : "8a90436bdef6e286465a918d089a9ca1",
        "event_cat": selectedCategoryIDs,
        "event_heading": self.txt_title.text ?? "" ,
        "news_topic": allkeys,
        "event_desc":self.textView_description.text ?? "",
        "location": self.txt_location.text ?? "",
        "date_from" : self.dateFrom,
        "date_to" : self.dateTo
        ]
                                    
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
            
        Utility.performMultipartRequestAddEvent(urlString: pathUrl, fileName: "event_pic", params: parameters, imageDataArray: nil, accessToken: nil) { (response, Error) in
                                            
        if Error == nil {
            
        print("image upload")
        print("result =\(response?["response"]!)")
            
        switch response?["response"] {
            
        case "200":
                    if response?["inserted"] == 1   {
                                            
                    let dialogMessage = UIAlertController(title: kAppTitle, message: "Event Added Successfully.", preferredStyle: .alert)
                                                                                             
                // Create OK button with action handler
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                                            
                    print("Ok button tapped")
                                                               
                    let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
                    self.navigationController?.pushViewController(pdvc, animated: true)
                                                
                    })
                                                                                             
                    //Add OK and Cancel button to dialog message
                    dialogMessage.addAction(ok)
                    self.present(dialogMessage, animated: true, completion: nil)
                    // Utility.displayAlert(title: kAppTitle, message: "Event Added Successfully.")
                    }
                 else if response?["updated"] == 1 {
                                            
                    let dialogMessage = UIAlertController(title: kAppTitle, message: "Event Updated Successfully.", preferredStyle: .alert)
                    // Create OK button with action handler
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
                    print("Ok button tapped")
                        
                    (self.delegate as! MyEventVC).eventUpdated()
                //    let pdvc = self.storyboard!.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
                self.navigationController?.popViewController(animated: true)
                                                                                                
                })
                                                                                                                        
        //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                self.present(dialogMessage, animated: true, completion: nil)
                                            
                }
                                                    
    case "404":
                Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                            
    default:
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                break
              }
            }
                            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
                          
      }
    }
    
        print("upload event action")
    }
    
    
    func validation() -> Bool {
        
        var isValidate:Bool = true
        
        guard self.txt_PostCategory.text?.count ?? 0 > 0 else {
        
            Utility.displayAlert(title: kAppTitle, message:kEnterCategory)
            return false
        }
    
        guard self.txt_title.text?.count ?? 0 > 0 else {
            Utility.displayAlert(title: kAppTitle, message: kEnterTitle)
            return false
        }
        
        guard self.textView_description.text.count > 0 else {
            Utility.displayAlert(title: kAppTitle, message: kEnterDescription)
            return false
        }
        
        return isValidate
        
    }
    
    func selectedCategory(selectedCategory:CategoryModel) {
        
        print("selected Category")
//        print("category coun = \(selectedCategory.count)")
        self.selectedCat = selectedCategory
        self.txt_PostCategory.text = self.selectedCat.cat_name
        
    }
}
