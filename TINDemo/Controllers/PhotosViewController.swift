//
//  PhotosViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 17/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PhotosViewController: UIViewController {

    @IBOutlet weak var photosCollectionView: UICollectionView!
     var newList = [GalleryModel]()
    let reusedIdentifire = "PhotosCollectionViewCell"
    var items = ["first","second","third","fourth","fifth"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.photosCollectionView.register(UINib(nibName: "PhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reusedIdentifire)
        
        self.callPhotosApi()
        // Do any additional setup after loading the view.
    }
    
    func callPhotosApi() {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
       
        let myurl = KGalleryPath 
        var param =  Parameters()
        param["api_key"] = kApiKey
        
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
        
        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"] {
            
          case "200":
                    let events = Utility.getGalleryModels(dict: swiftyJsonVar)
                    self.newList = events
                    self.photosCollectionView.reloadData()
                   
                        
                    break
            
           case "404":
                      // self.noDataFound.isHidden = false
                       break
                       
            default:
                       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        break
            }
            
          MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
        
    }
}

extension PhotosViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1

    }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int  {
        return newList.count
    
    }
       
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
    let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                       
    return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
       
    }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
        let cell = collectionView
           .dequeueReusableCell(withReuseIdentifier: reusedIdentifire, for: indexPath) as! PhotosCollectionViewCell
    
        var dateText = ""
         
        if let timeText = newList[indexPath.row].date
        {
            dateText = dateText + timeText
        }
        if let locationText = newList[indexPath.row].location
        {
            dateText = dateText + "(" + locationText + ")"
        }
       
        cell.lbl_timeDuration.attributedText = Utility.getAttributedString(color: UIColor.red, colorText: dateText, fullString: dateText)
        
        cell.lbl_heading.text = newList[indexPath.row].event
        
       // cell.lbl_timeDuration.text  = dateText
        cell.photoImage.sd_setImage(with: URL(string: newList[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
        
        cell.layoutIfNeeded()
        return cell
 
       }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
       
       func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return UICollectionReusableView()
        
    }
}


