//
//  VideosVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 13/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class VideosVC: UIViewController  ,closeLeftMenuDelegate{
    @IBOutlet weak var topStoryCollectionView: UICollectionView!
    private let storyreuseIdentifier = "storyCell"
    private let loadMoreuseIdentifier = "loadMoreCell"
    var newList = [NewsModel]()
    var previousController = ""
    var backgroundView:UIView?
    var noDataFound:UILabel!
    
    var initialLoadNews = 0
    var incrementLoad:Int = 10
    var isMore:Bool = false
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setNavigationBarItem()
        self.callVediosApi()
        self.topStoryCollectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: loadMoreuseIdentifier)
        self.addRefreshControll()
        self.nodataFoundLabel()
        self.noDataFound.isHidden = true
    }
    
    
    func addRefreshControll() {
        
        self.topStoryCollectionView.alwaysBounceVertical = true
        self.topStoryCollectionView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.topStoryCollectionView.addSubview(refreshControl)
              
    }
    
    @objc func didPullToRefresh() {
        
        self.initialLoadNews = 0
        self.incrementLoad = 10
        var isMore:Bool = false
        self.newList.removeAll()
        self.callVediosApi()
        self.nodataFoundLabel()
        self.noDataFound.isHidden = true
        self.refreshControl.endRefreshing()
        
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(true)
              self.tabBarController?.tabBar.isHidden = false
          }
    
    func nodataFoundLabel() {
        
           noDataFound = UILabel()
           noDataFound.text = ""
           noDataFound.translatesAutoresizingMaskIntoConstraints = false
           noDataFound.lineBreakMode = .byWordWrapping
           noDataFound.numberOfLines = 0
           noDataFound.textAlignment = .center

           self.view.addSubview(noDataFound)

           noDataFound.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
           noDataFound.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
           noDataFound.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
       }

    
     func callVediosApi() {
        
         let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
         spinnerActivity?.labelText = kLodindMessage
                 
//        let myurl = kVediosListPath + "/\(initialLoadNews)"
         let myurl = kVediosListPath
         print("path url = \(myurl)")
         var param =  Parameters()
         param["api_key"] = kApiKey
                 
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                     
        let swiftyJsonVar = JSON(result)
            
        print("swiftyjsonvar result =\(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"] {
        case "200" :
                    var newArray =   Utility.getNewsListByMustWatch(dict: swiftyJsonVar)
                    // self.newList = newArray
                    
                    // if self.initialLoadNews < self.newList.count
                    if swiftyJsonVar["next"] != "0" {
                            
                      if self.newList.count > 0{
                                
                        self.newList.removeLast()
                        }
                    //  self.newList = Array(self.newList.prefix(self.initialLoadNews))
                        self.newList =  self.newList + newArray + [NewsModel()]
                        self.isMore = true
                     }
                    else {
                            if self.newList.count > 0 {
                                self.newList.removeLast()
                            }
                            self.newList = self.newList + newArray
                            self.isMore = false
                            self.topStoryCollectionView.reloadData()
                            //  self.initialLoadNews = 4
                        }
                       
                            self.topStoryCollectionView.reloadData()
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            case "404" :
                        if self.newList.count > 0 {
                            
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                         }
                        else {
                            self.noDataFound.text = kNoVideoFound
                            self.noDataFound.isHidden = false
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        default:
                        if self.newList.count > 0 {
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                        }
            break
            }
        
       
               MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }
    }
    
    func callLoadMoreVediosApi() {
        
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
                 
        let myurl = kVediosListPath + "/\(initialLoadNews)"
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
                 
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                     
        let swiftyJsonVar = JSON(result)
            
        print("swiftyjsonvar result =\(swiftyJsonVar)")
        switch swiftyJsonVar["response"] {
        case "200" :
                    var newArray =   Utility.getNewsListByMustWatch(dict: swiftyJsonVar)
                    // self.newList = newArray
                    
                    // if self.initialLoadNews < self.newList.count
                    if swiftyJsonVar["next"] != "0" {
                            
                       if self.newList.count > 0 {
                           self.newList.removeLast()
                        }
                        //  self.newList = Array(self.newList.prefix(self.initialLoadNews))
                        self.newList =  self.newList + newArray + [NewsModel()]
                        self.isMore = true
                     }
                    else {
                            if self.newList.count > 0 {
                                self.newList.removeLast()
                            }
                            self.newList = self.newList + newArray
                            self.isMore = false
                            self.topStoryCollectionView.reloadData()
                            //  self.initialLoadNews = 4
                        }
                       
                            self.topStoryCollectionView.reloadData()
                            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            case "404" :
                        if self.newList.count > 0 {
                                self.isMore = false
                                self.newList.removeLast()
                                self.topStoryCollectionView.reloadData()
                                print("number of items = \(self.newList.count)")
                            }
                        else {
                            self.noDataFound.text = kNoVideoFound
                            self.noDataFound.isHidden = false
                        }
                        
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        default:
                        if self.newList.count > 0 {
                            self.isMore = false
                            self.newList.removeLast()
                            self.topStoryCollectionView.reloadData()
                        }
                      break
            }
        
       
               MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }
    }
    

        func setNavigationBarItem()
        {
           
            let btn1 = UIButton(type: .custom)
        
            if self.previousController == "Home" || self.previousController == "LeftMenu" {
                btn1.setImage(UIImage(named: "backIcon.png"), for: .normal)
                btn1.imageEdgeInsets.left = 5.0
                btn1.imageEdgeInsets.right = 5.0
                btn1.imageEdgeInsets.top = 5.0
                btn1.imageEdgeInsets.bottom = 5.0
                btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                btn1.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
            }
            else {
                btn1.setImage(UIImage(named: "menu_icon.png"), for: .normal)
                btn1.imageEdgeInsets.left = 5.0
                btn1.imageEdgeInsets.right = 5.0
                btn1.imageEdgeInsets.top = 5.0
                btn1.imageEdgeInsets.bottom = 5.0
                btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                btn1.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)
                
            }
            
            let item1 = UIBarButtonItem(customView: btn1)

            let btn2 = UIButton(type: .custom)
            btn2.setImage(UIImage(named: "logo.png"), for: .normal)
            btn2.imageEdgeInsets.left = 0.0
            btn2.imageEdgeInsets.right = 0.0
            btn2.imageEdgeInsets.top = 0.0
            btn2.imageEdgeInsets.bottom = 0.0
            btn2.imageView?.contentMode = .scaleAspectFill
            
            btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
            btn2.isEnabled = false
            btn2.isUserInteractionEnabled = false
            let item2 = UIBarButtonItem(customView: btn2)

            self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
        }
    
    @objc func backAction()  {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func leftMenuClosed() {
        
        backgroundView?.removeFromSuperview()
    }
    
    @objc func openMenu()  {
        
        print("open menu")
        let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems,fromController: "Videos" )
        rootView.delegate = self
                       
        let mainWindow = UIApplication.shared.keyWindow!
        backgroundView = UIView(frame: CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height))
                         
        backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainWindow.addSubview(backgroundView!);
                       
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
        
    }
}



extension VideosVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView.tag == collectionViewsTag.story.rawValue {
          return 1
         }
      return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
                return self.newList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
      if collectionView.tag == collectionViewsTag.story.rawValue {
        
            if isMore && indexPath.row == (self.newList.count - 1) {
                let kWhateverHeightYouWant = 80.0
                return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
            }
            else {
                let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant))
            }
         }
        
        let kWhateverHeightYouWant = collectionView.frame.height
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(kWhateverHeightYouWant))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       if collectionView.tag == collectionViewsTag.story.rawValue {
            if isMore  && (indexPath.row == (self.newList.count - 1)) {
                
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadMoreuseIdentifier, for: indexPath) as! LoadMoreCollectionViewCell
            cell.backgroundColor = UIColor.red
            cell.lbl_loadMore.text = "Load More..."
//            initialLoadNews = initialLoadNews + incrementLoad
//            callVediosApi()
            initialLoadNews = initialLoadNews + incrementLoad
            callLoadMoreVediosApi()
            isMore = false
            return cell
            }
        else {
            let cell = collectionView
                      .dequeueReusableCell(withReuseIdentifier: storyreuseIdentifier, for: indexPath) as! StoryCollectionViewCell
        
                    // cell style
            cell.imageView.layer.cornerRadius = 10.0
            cell.imageView.layer.borderWidth = 1.0
            cell.imageView.layer.borderColor = UIColor.clear.cgColor
            cell.imageView.layer.masksToBounds = true
        
            cell.imageView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.imageView.layer.backgroundColor = UIColor.clear.cgColor
                
     if self.newList.count > 0 {
            cell.lbl_detail.text = self.newList[indexPath.row].news_heading
            cell.lbl_timedetail.text = self.newList[indexPath.row].days_ago
                    
            let lblDayString = (self.newList[indexPath.row].from ?? "tin.media") + "    " +  (self.newList[indexPath.row].days_ago ?? "")
            let colorText = self.newList[indexPath.row].from ?? "tin.media"
                    

            cell.lbl_timedetail.attributedText = Utility.getAttributedString(color:UIColor.red,colorText: colorText, fullString: lblDayString)
                                                 
            cell.lbl_detail.text = self.newList[indexPath.row].news_heading
                   
            cell.imageView.sd_setImage(with: URL(string: self.newList[indexPath.row].image_url!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
            }

         return cell
        }
      }
        
        let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) 
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
      if collectionView.tag == collectionViewsTag.story.rawValue {
        
            if Utility.isConnected() {
                
             let VedioDetailVc = self.storyboard!.instantiateViewController(withIdentifier: "VedioDetailsVC") as! VedioDetailsVC
               
             VedioDetailVc.vedioId = self.newList[indexPath.row].id
             self.navigationController?.pushViewController(VedioDetailVc, animated: true)
             print("Internet is available.")
               // ...
                
             }
            else {
                
                Utility.displayAlert(title: kAppTitle, message: kNewtworkNotAvailable)
            }
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {

        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.label.text = "VIDEOS"
            return headerView

        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            
            return footerView

        default:
                return UICollectionReusableView()
        }
    }
    
}
