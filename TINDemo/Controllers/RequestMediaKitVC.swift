//
//  RequestMediaKitVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 10/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class RequestMediaKitVC: UIViewController {
 @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_mobile: UITextField!
    @IBOutlet weak var txt_companyName: UITextField!
    @IBOutlet weak var txt_website: UITextField!
    @IBOutlet weak var txt_city: UITextField!
    @IBOutlet weak var txt_country: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.setPlaceholderColor()
         IQKeyboardManager.shared.enable = true
        // Do any additional setup after loading the view.
    }
    
    func setPlaceholderColor()
    {
        self.txt_name.placeholderColor(textfieldPlaceholder)
        self.txt_email.placeholderColor(textfieldPlaceholder)
        self.txt_mobile.placeholderColor(textfieldPlaceholder)
        self.txt_companyName.placeholderColor(textfieldPlaceholder)
        self.txt_website.placeholderColor(textfieldPlaceholder)
        self.txt_city.placeholderColor(textfieldPlaceholder)
        self.txt_country.placeholderColor(textfieldPlaceholder)
    }
}
