//
//  WorldseafoodVc.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 17/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class WorldseafoodVc: UIViewController {


    @IBOutlet weak var photosCollectionView: UICollectionView!
     var newList = [NewsModel]()
    let reusedIdentifire = "SeafoodCollectionViewCell"
    
    var items = ["first","second","third","fourth","fifth"]
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
      
        self.photosCollectionView.register(UINib(nibName: "SeafoodCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reusedIdentifire)
        // Do any additional setup after loading the view.
    }
}

extension WorldseafoodVc: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int  {
        return items.count
    
    }
       
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = collectionView.bounds.size.width/2
                       
        return CGSize(width: collectionView.bounds.size.width/2, height: CGFloat(kWhateverHeightYouWant/2))
    
    }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusedIdentifire, for: indexPath) as! SeafoodCollectionViewCell

        return cell
        
    }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
}


