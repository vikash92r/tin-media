//
//  UpdateProfileViewController.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 16/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import SDWebImage


class UpdateProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var txt_title: UITextField!
    @IBOutlet weak var txt_first: UITextField!
    @IBOutlet weak var txt_last: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_designation: UITextField!
    @IBOutlet weak var txt_department: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    var image1:UIImageView!
    var selectedImage:UIImage!
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad()
     {
        super.viewDidLoad()
        
        self.title = "Update Profile"
        self.callGetProfileApi()
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.shadowRadius = 10
        self.setPlaceholderColor()
        
         IQKeyboardManager.shared.enable = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        self.userImageView.isUserInteractionEnabled = true
        self.userImageView.addGestureRecognizer(tapGestureRecognizer)
        
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
//         imagePicker.sourceType = .photoLibrary
        
        self.userImageView.layer.borderWidth = 1
        self.userImageView.layer.masksToBounds = false
        self.userImageView.layer.borderColor = UIColor.gray.cgColor
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height/2
        self.userImageView.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    func callGetProfileApi()
    {
        let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinnerActivity?.labelText = kLodindMessage
        print("uuid Value =\(kUUIDValue)")
       
        let myurl = kuserDetailApi  +  UserDetails.sharedInstance.userdata
        print("path url = \(myurl)")
        var param =  Parameters()
        param["api_key"] = kApiKey
                   
        WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                       
        let swiftyJsonVar = JSON(result)
                      
        print("response = \(swiftyJsonVar["response"])")
        print("result = \(swiftyJsonVar)")
                    
        switch swiftyJsonVar["response"] {
             
         case "200":
                    guard let dictObject = swiftyJsonVar["userdetails"].arrayObject else { return  }
                    let dict = dictObject as! [[String:String]]
                    self.txt_first.text = dict[0]["fname"]
                    self.txt_last.text = dict[0]["last_name"]
                    self.txt_email.text = dict[0]["email"]
                    self.txt_phone.text = dict[0]["usercontact"]
                    self.txt_title.text = dict[0]["title"]
                    self.txt_designation.text = dict[0]["designation"]
                    self.txt_department.text = dict[0]["company"]
                    
                    UserDetails.sharedInstance.title = dict[0]["title"] ?? ""
                    UserDetails.sharedInstance.firstName = dict[0]["fname"] ?? ""
                    UserDetails.sharedInstance.lastName = dict[0]["last_name"] ?? ""
                    UserDetails.sharedInstance.company = dict[0]["company"] ?? ""
                    UserDetails.sharedInstance.designation = dict[0]["designation"] ?? ""
                    UserDetails.sharedInstance.usercontact = dict[0]["usercontact"] ?? ""
                    UserDetails.sharedInstance.email =  dict[0]["email"] ?? ""
                    UserDetails.sharedInstance.userdata =  dict[0]["userdata"] ?? ""
                    UserDetails.sharedInstance.image =  dict[0]["image"] ?? ""
                    UserDetails.sharedInstance.password =  dict[0]["password"] ?? ""
                                                               
                    Utility.saveUserDetails()
                    
                    let imageurl = dict[0]["image"]
                   
                    if  let url = URL(string:imageurl ?? ""){
                        
                        if let data = try? Data(contentsOf: url) {
                            
                           let image: UIImage = UIImage(data: data)!
                           self.selectedImage  = image
                        }
                     }

                    self.userImageView.sd_setImage(with: URL(string: imageurl ?? ""), placeholderImage: UIImage(named: "imagePlaceholder.png"))
                
           default:   Utility.displayAlert(title: kAppTitle, message: kSomethingWrong)
                        
            }
                 
                  MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("image Tapped.")
        alertshow()
    }
    
    func setPlaceholderColor() {
        
        self.txt_title.placeholderColor(textfieldPlaceholder)
        self.txt_first.placeholderColor(textfieldPlaceholder)
        self.txt_last.placeholderColor(textfieldPlaceholder)
//        self.txt_email.placeholderColor(textfieldPlaceholder)
        self.txt_email.placeholderColor(textfieldPlaceholder)
        self.txt_phone.placeholderColor(textfieldPlaceholder)
        self.txt_department.placeholderColor(textfieldPlaceholder)
        self.txt_designation.placeholderColor(textfieldPlaceholder)
     }
    
    func alertshow() {
        
           let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
               self.openCamera()
           }))
           
           alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
               self.openGallary()
           }))
           
           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
    
    func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
//            self.imagePicker.cameraCaptureMode = .photo
//            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker,animated: true,completion: nil)
         }
       else {
            
            Utility.displayAlert(title: kAppTitle, message: "camera is not available")
         }
       }

   
 func openGallary() {
    
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
          }
    
    
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            self.userImageView.contentMode = .scaleToFill
            self.userImageView.image = pickedImage
            self.selectedImage = pickedImage
            
        }

        dismiss(animated: true, completion: nil)
    }
       
func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        
        let pathUrl = kUpdateProfilePath + UserDetails.sharedInstance.userdata
        
        guard   self.txt_title.text?.count ?? 0 > 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterTitle)
            return
        }
        
        guard   self.txt_first.text?.count ?? 0 > 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterFirstName)
            return
        }
        
        guard self.txt_last.text?.count ?? 0 > 0 else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterLastName)
            return
        }
        
        let title = self.txt_title.text!
        let firstName = self.txt_first.text!
        let lastName = self.txt_last.text!
        
        guard let email = self.txt_email.text else {
            
            Utility.displayAlert(title: kAppTitle, message: kEnterEmail)
            return
        }
        
        guard let phone  = self.txt_phone.text else {
            
            Utility.displayAlert(title: kAppTitle, message: "Enter phone number")
            return
        }
        
        guard let designation = self.txt_designation.text else {
            
            Utility.displayAlert(title: kAppTitle, message: "Enter designation")
            return
        }
        
        guard let department = self.txt_department.text else {
            
            Utility.displayAlert(title: kAppTitle, message: "Enter department")
            return
        }
//
        
        if selectedImage != nil {
            
        if let data = self.selectedImage.jpegData(compressionQuality: 0.5) {
          let parameters: Parameters = [
            "api_key" : "8a90436bdef6e286465a918d089a9ca1",
          "title" : title,
          "fname":firstName,
          "lname": lastName,
          "usercontact":phone,
          "company":department,
          "designation":designation
          ]
            
            var imageData  = Array<Data>()
           
            imageData.append(self.selectedImage.jpegData(compressionQuality: 0.5)!)
//
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
                    spinnerActivity?.labelText = kLodindMessage
            Utility.performMultipartRequest(urlString: pathUrl, fileName: "profile_image", params: parameters, imageDataArray: imageData, accessToken: nil) { (response, Error) in
                
         if Error == nil {
            
            print("image upload")
            print("result =\(response?["response"]!)")
        
        switch response?["response"] {
            
          case "200":
                    if response?["is_updated"] == 1 {
                    UserDetails.sharedInstance.firstName = firstName
                    UserDetails.sharedInstance.lastName = lastName
                    UserDetails.sharedInstance.company = department
                    UserDetails.sharedInstance.designation = designation
                    UserDetails.sharedInstance.title = title
                    UserDetails.sharedInstance.usercontact = phone
                        
                                    
                    Utility.saveUserDetails()
                                    
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name("profileUpdated"), object: nil)
                                    
                    Utility.displayAlert(title: kAppTitle, message: "Profile successfully updated.")
                                    
                    }
                        
            case "404":
                        Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                      
            default:
                         MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        break
                    }
                }
                 MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
             }                        // You can change your image name here, i use NSURL image and convert into string

           }
         }
        else {
           
            let parameters: Parameters = [
                "api_key" : "8a90436bdef6e286465a918d089a9ca1",
                "title" : title,
                "fname":firstName,
                "lname": lastName,
                "usercontact":phone,
                "company":department,
                "designation":designation
                ]
            
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
                                spinnerActivity?.labelText = kLodindMessage
            
            Utility.performMultipartRequest(urlString: pathUrl, fileName: "profile_image", params: parameters, imageDataArray: nil, accessToken: nil) { (response, Error) in
                               
            if Error == nil {
                             
             print("image upload")
             print("result =\(response?["response"]!)")
             
            switch response?["response"] {
                
             case "200":
                        if response?["is_updated"] == 1 {
                            
                        UserDetails.sharedInstance.firstName = firstName
                        UserDetails.sharedInstance.lastName = lastName
                        UserDetails.sharedInstance.company = department
                        UserDetails.sharedInstance.designation = designation
                        UserDetails.sharedInstance.title = title
                        UserDetails.sharedInstance.usercontact = phone
                                            
                        Utility.saveUserDetails()
                                                
                        let nc = NotificationCenter.default
                        nc.post(name: Notification.Name("profileUpdated"), object: nil)
                                                
                        Utility.displayAlert(title: kAppTitle, message: "Profile successfully updated.")
                       }
                                    
           case "404":
                        Utility.displayAlert(title: kAppTitle, message: "somthing is wrong")
                            
          default:
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        break
                }
            }
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
          }
        }
    }
}


