//
//  MyNewsVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 18/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MyNewsVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var allEventArray = [NewsModel]()
    var refreshControl: UIRefreshControl!
    var noDataFound:UILabel!
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        self.callMyNewsApi()
        self.tableView.tableFooterView = UIView()
       
        self.cardView.layer.shadowColor = UIColor.black.cgColor
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.layer.shadowOffset = .zero
        self.cardView.layer.cornerRadius = CGFloat(kCornerRadiusValue)
        self.cardView.isHidden = true
        
        self.addRefreshControll()
        self.nodataFoundLabel()
        self.noDataFound.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    func addRefreshControll()  {
        
        self.tableView.alwaysBounceVertical = true
        self.tableView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
    }
    
    
    @objc func didPullToRefresh() {
    
        self.callMyNewsApi()
        self.refreshControl.endRefreshing()
    }
    
    func nodataFoundLabel() {
        
//      noDataFound = UILabel()
//      noDataFound.text = "No MyNews Found"
//      noDataFound.translatesAutoresizingMaskIntoConstraints = false
//      noDataFound.lineBreakMode = .byWordWrapping
//      noDataFound.numberOfLines = 0
//      noDataFound.textAlignment = .center
        
      noDataFound = Utility.getNoDataLabel(msg: "No MyNews Found",view: self.view)
      self.view.addSubview(noDataFound)
        
      Utility.setConstraintForNoDataLabel(view: self.view, noDataLabel: noDataFound)

//      noDataFound.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
//      noDataFound.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//      noDataFound.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
  }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
            
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.allEventArray.count
    
    }
            
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        var cell : MyEventTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "MyEventTableViewCell") as! MyEventTableViewCell
        
        if cell == nil {
        cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MyEventTableViewCell") as! MyEventTableViewCell
        }
        
        if self.allEventArray.count > 0 {
      
        cell?.lbl_eventName.text = self.allEventArray[indexPath.row].news_heading
        cell?.lbl_date.text = (self.allEventArray[indexPath.row].created_date ?? "")
            
        cell?.lbl_location.text = self.allEventArray[indexPath.row].location
                    
        cell?.eventImage.sd_setImage(with: URL(string: self.allEventArray[indexPath.row].image!), placeholderImage: UIImage(named: "imagePlaceholder.png"))
        }
        
        cell?.textLabel?.numberOfLines = 0
                
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
     }
        
    

    func callMyNewsApi() {
        
     let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
     spinnerActivity?.labelText = kLodindMessage
     
     let myurl = kMyNewsPath + UserDetails.sharedInstance.userdata
     var param =  Parameters()
     param["api_key"] = kApiKey
        
    WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
    
        let swiftyJsonVar = JSON(result)
        print("result = \(swiftyJsonVar)")
            
        switch swiftyJsonVar["response"] {
            
          case "200":
                      let events = Utility.getMyNewsList(dict: swiftyJsonVar)
                      self.allEventArray = events
                      self.tableView.reloadData()
                      self.cardView.isHidden = false
                        
                      self.noDataFound.isHidden = true
                       
                      break
            
            case "404" :
                        self.noDataFound.isHidden = false
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                       
            default:
                       MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        break
          }
          
          MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        
       }
    }
    
    
    @IBAction func actionDelete(_ sender: Any) {
        
        let buttonPostion = (sender as AnyObject).convert((sender as AnyObject).bounds.origin, to: tableView)

    if let indexPath = tableView.indexPathForRow(at: buttonPostion) {
        
        let rowIndex =  indexPath.row
        print("index delete= \(rowIndex)")
        self.callDeleteEvent(index: indexPath.row)
        
      }
    }
    
  func callDeleteEvent(index:Int)  {
    
            
     let dialogMessage = UIAlertController(title: kAppTitle, message: "Are you sure, you want to delete?", preferredStyle: .alert)
                              
     // Create OK button with action handler
     let ok = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
    print("Ok button tapped")
                                  

     let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
     spinnerActivity?.labelText = kLodindMessage
     
     let myurl = kRemoveNewsPath + (self.allEventArray[index].id ?? "")
     var param =  Parameters()
     param["api_key"] = kApiKey
           
    WebServiceHandler.performPOSTRequest(urlString: myurl, andParameters: param) { (result, error) in
                 
      let swiftyJsonVar = JSON(result)
      print("result = \(swiftyJsonVar)")
               
      switch swiftyJsonVar["response"] {
        
        case "200":
                    if swiftyJsonVar["removed"] == 1  {
                        
                       self.allEventArray.remove(at: index)
                       self.tableView.reloadData()
                    }
                    
                    break
        
        case "404":
                    self.noDataFound.isHidden = false
                   
                    break
                                     
        default:
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    
                    break
            }
                    
    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
                          
    })
                              
        // Create Cancel button with action handlder
    let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
        
        print("Cancel button tapped")
        
    }
                              
        //Add OK and Cancel button to dialog message
    dialogMessage.addAction(ok)
    dialogMessage.addAction(cancel)
    
    self.present(dialogMessage, animated: true, completion: nil)
            
    }
    
    @IBAction func actionEdit(_ sender: Any) {
        
    let buttonPostion = (sender as AnyObject).convert((sender as AnyObject).bounds.origin, to: tableView)

    if let indexPath = tableView.indexPathForRow(at: buttonPostion) {

        let rowIndex =  indexPath.row
        print("index = \(rowIndex)")
            
        let addnewsVC = self.storyboard!.instantiateViewController(withIdentifier: "AddNewsVC") as! AddNewsVC
        addnewsVC.fromController = "MyNewsToEdit"
        addnewsVC.delegate = self
        addnewsVC.selectedEvent = self.allEventArray[indexPath.row]
        addnewsVC.catId = self.allEventArray[indexPath.row].id ?? ""
            
        self.navigationController?.pushViewController(addnewsVC, animated: true)
     }
        
 }

    func newsUpdated() {
        
           self.callMyNewsApi()
        
       }
    }

        
        
  

