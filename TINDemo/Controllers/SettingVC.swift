//
//  SettingVC.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 12/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FBSDKLoginKit

class SettingVC: UIViewController ,closeLeftMenuDelegate{
// var itemsMenu = ["Pravacy Policy","About Us","Contact Us","Data Policy","Signout"]
    var itemsMenu = ["Privacy Policy","Cookie Policy","Copyright Policy","Data Policy","Subscriber Agreement & Terms of Use","About Us","Contact Us","Update Profile","Logout"]
    var itemsMenuAfterLogin = ["unsubscribe newsletter","Privacy Policy","About Us","Contact Us","Data Policy","Update Profile","Logout"]
     var backgroundView:UIView?
    let singOutNotificationSetting = NotificationCenter.default
    var urkString = ""
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setNavigationBarItem()
        self.tableView.tableFooterView = UIView()
       
        if  UserDetails.sharedInstance.email.count > 0
        {
//              tableView.register(UINib(nibName: "UnsubscribeNewsletterTableViewCell", bundle: nil), forCellReuseIdentifier: "UnsubscribeNewsletterTableViewCell")
        }
        else
        {
           itemsMenu = ["Privacy Policy","Cookie Policy","Copyright Policy","Data Policy","Subscriber Agreement & Terms of Use","About Us","Contact Us"]
        }
        
        singOutNotificationSetting.addObserver(self, selector: #selector(userLogoutSetting), name: Notification.Name("userLogoutSetting"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
    
     }
    
    @objc func userLogoutSetting() {
        
        print("get message using notification")
        itemsMenu = ["Privacy Policy","Cookie Policy","Copyright Policy","Data Policy","Subscriber Agreement & Terms of Use","About Us","Contact Us"]
        self.tableView.reloadData()

     }
    
  func setNavigationBarItem() {
           
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu_icon.png"), for: .normal)
        btn1.imageEdgeInsets.left = 5.0
        btn1.imageEdgeInsets.right = 5.0
        btn1.imageEdgeInsets.top = 5.0
        btn1.imageEdgeInsets.bottom = 5.0
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "logo.png"), for: .normal)
        btn2.imageEdgeInsets.left = 0.0
        btn2.imageEdgeInsets.right = 0.0
        btn2.imageEdgeInsets.top = 0.0
        btn2.imageEdgeInsets.bottom = 0.0
        btn2.imageView?.contentMode = .scaleAspectFill
            
        btn2.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    //            btn2.addTarget(self, action: #selector(self.secondName), for: .touchUpInside)
        btn2.isEnabled = false
        btn2.isUserInteractionEnabled = false
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setLeftBarButtonItems([item1,item2], animated: true)
            
        }
    
    func leftMenuClosed() {
        
              backgroundView?.removeFromSuperview()
          }
    
    @objc func openMenu() {
              
        print("open menu")
        let window = UIApplication.shared.keyWindow!
        let rootView:PopoverView = PopoverView.init(frame:window.bounds,mArray:leftMenuItems, fromController: "SettingVC" )
        rootView.delegate = self
                     
        let mainWindow = UIApplication.shared.keyWindow!
        backgroundView = UIView(frame: CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height))
                       
        backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
           mainWindow.addSubview(backgroundView!);
                     
        let transition:CATransition = CATransition.init()
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        rootView.layer.add(transition, forKey: "rightToLeftTransition")
        window.addSubview(rootView)
              
    }
}

extension SettingVC:UITableViewDelegate, UITableViewDataSource
{
     func numberOfSections(in tableView: UITableView) -> Int {
          
          return 1
        }
        
      
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
            return self.itemsMenu.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            // create a new cell if needed or reuse an old one
            
//            if UserDetails.sharedInstance.email.count > 0
//            {
//                if indexPath.row == 0
//                {
//
//                    var cell : UnsubscribeNewsletterTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "UnsubscribeNewsletterTableViewCell") as! UnsubscribeNewsletterTableViewCell
//
//                    if cell == nil {
//                                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "UnsubscribeNewsletterTableViewCell") as! UnsubscribeNewsletterTableViewCell
//                            }
//                    return cell!
//                }
//            }
            
            var cell : SettingTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "SettingCell") as! SettingTableViewCell
            }
            
            if self.itemsMenu.count > 0 {
                cell?.lbl_MenuItem!.text = itemsMenu[indexPath.row]
            }
            cell?.textLabel?.numberOfLines = 0
            
            return cell!
        }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
          
        
        if itemsMenu[indexPath.row] == "Privacy Policy" {
            
            self.urkString = kPrivacyUrl
          }
        else if itemsMenu[indexPath.row] == "Cookie Policy" {
            
            self.urkString = kCookiePolicyUrl
        }
        else if itemsMenu[indexPath.row] == "Copyright Policy" {
            
            self.urkString = kCopyrightPolicyUrl
        }
    else if itemsMenu[indexPath.row] == "Data Policy" {
            
            self.urkString = kDatapolicyUrl
        }
        else if itemsMenu[indexPath.row] == "Subscriber Agreement & Terms of Use" {
            
            self.urkString = kTermsAndConditionUrl
        }
        else if itemsMenu[indexPath.row] == "About Us" {
            
              self.urkString = kAboutusUrl
        }
        else if itemsMenu[indexPath.row] == "Contact Us" {
            
                self.urkString = kContactUsUrl
        }
   
            
        if UserDetails.sharedInstance.email.count > 0 {
            
        if indexPath.row == (itemsMenu.count - 2) {
            
          let upVC = self.storyboard!.instantiateViewController(withIdentifier: "UpdateProfileViewController") as! UpdateProfileViewController
          self.navigationController?.pushViewController(upVC, animated: true)
            
        }
        else if indexPath.row == (itemsMenu.count - 1) {
            
         let dialogMessage = UIAlertController(title: kAppTitle, message: "Are you sure, you want to logout?", preferredStyle: .alert)
                   
         // Create OK button with action handler
        let ok = UIAlertAction(title: "Logout", style: .default, handler: { (action) -> Void in
        print("Ok button tapped")
                       
        Utility.clearUserDetails()
        Utility.saveUserDetails()
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("userLogout"), object: nil)
            
        let updateProfilePicNotification = NotificationCenter.default
        updateProfilePicNotification.post(name:Notification.Name("userUpdateProfilePic"),object: nil)
                            
        GIDSignIn.sharedInstance()?.signOut()
                            
       // Utility.displayAlert(title: kAppTitle, message: "You Logout Successfully.")
                            
        let loginManager = LoginManager()
        loginManager.logOut()
                            
        //   self.itemsMenu = ["Privacy Policy","About Us","Contact Us","Data Policy"]
        self.itemsMenu =  ["Privacy Policy","Cookie Policy","Copyright Policy","Data Policy","Subscriber Agreement & Terms of Use","About Us","Contact Us"]
        self.tableView.reloadData()
            
          Utility.tabBarAsRootViewController()
               
        })
                   
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                
        print("Cancel button tapped")
        }
                   
                   //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
            
     }
    else {
    
     if self.urkString.count > 0 {
        
        let webViewVc = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webViewVc.urlString = self.urkString
        webViewVc.controllerName = self.itemsMenu[indexPath.row]
        self.navigationController?.pushViewController(webViewVc, animated: true)
          }
            
        }
      }
    else {
        
      if self.urkString.count > 0 {
        
        let webViewVc = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webViewVc.urlString = self.urkString
        webViewVc.controllerName = self.itemsMenu[indexPath.row]
        self.navigationController?.pushViewController(webViewVc, animated: true)
        }
     }
  }
}
