//
//  PhotosCollectionViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 17/01/20.
//  Copyright © 2020 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var lbl_timeDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
