//
//  Constants.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 06/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import Foundation

let kApiKey = "8a90436bdef6e286465a918d089a9ca1"
let kClientID = "14340228236-14mmaurs8b17v4gn3coohl0q7gfh4cqb.apps.googleusercontent.com"

let kNoInternetConnection = "Check your Internet connection"
let APPDELEGATE = UIApplication.shared.delegate!

let kAppTitle = "TIN MEDIA"

let kBookMarkAdded = "You have successfully added this into bookmark."
let kBookMarkRemoved = "You have removed this from bookmark."
let kEnterTitle = "Please enter title."
let kEnterFirstName = "Please enter name."
let kEnterLastName = "Please enter last name."
let kEnterEmail = "Please enter email."
let kEnterEmailToSubscribe = "Please enter your email id."
let kEnterValidEmail = "Please enter valid email."
let kEnterPassword = "Please enter password."
let kEnterOldPassword = "Please enter old password."
let kEnterNewPassword = "Please enter new password."
let kEnterConfirmPassword = "Please enter confirm password."
let kPasswordNotMatched = "Password and confirm password not matched."
let kWrongOldPasswordNotMatched = "Old password not matched."
let kRegisteredSuccessfuly = "Registered successfully."
let kEnterCategory = "Please enter category."
let kEnterSubCategory = "Please enter subcategory."
let kEnterDescription = "Please enter description."
let kNewsAddedSuccessfuly = "News Added Successfully."
let kEnterNewsHeading = "Please enter news heading."
let kEnterNewsTopic = "Please enter news topic."
let kEnterLocation = "Please enter location."
let kEnterNewsDescription = "Please enter news description."
let kSelectImage = "Please Select Image."

let kNewtworkNotAvailable = "Please check your Newtwork connection.."
let kUUIDValue = UIDevice.current.identifierForVendor!.uuidString
let selectedMenuTextColor = UIColor(red:0.25, green:0.25, blue:0.26, alpha:1.0)
let selectedUnderLineColor = UIColor(red:0.78, green:0.22, blue:0.20, alpha:1.0)
let unselectedUnderLineTextColor = UIColor(red:0.45, green:0.45, blue:0.46, alpha:1.0)
let leftMenuItems = ["Events","Webinars","Fam Trips","Conferences","Networking","Trade Shows","Subscribe","Upload and Event","Photos","Videos","Advertise with Us","Break Your Own news","Submit a press release","Submit a travel Blog","Request Media Kit"]

let kSomethingWrong = "Something went wrong."
let emailAlreadyExist = "Email id already exists."
let textfieldPlaceholder = UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0)

let kCategory = "Category"
let kNewsTopic = "NewsTopic"
let kEventCategory = "EventCategory"

let ktopnewsTitle = "TopNews"
let krecentNewsTitle = "RecentNews"
let kEventsTitle = "Events"
let kMustWatchTitle = "MustWatch"
let kTopStoriesTitle = "Top Stories"

let kEventCat = "999"
let kNoNewsFound = "No News Found"
let kNoEventFound = "No Event Found"
let kNoVideoFound = "No Video Found"
let kNoBookMarkFound = "No BookMark Found"

// loader text message
let kLodindMessage = ""

let kCornerRadiusValue = 20.0
