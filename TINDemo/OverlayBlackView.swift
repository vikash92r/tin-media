//
//  OverlayBlackView.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 20/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class OverlayBlackView: UIView {
    
    override func draw(_ rect: CGRect) {
        // to remove all sublayers on overlayblackview
        self.layer.sublayers?.removeAll()
        
        let gradientLayer: CAGradientLayer! = CAGradientLayer()
        gradientLayer.frame = self.frame(forAlignmentRect: CGRect(x: 0.0, y: 0.0, width: self.frame.size.width  , height: self.frame.height))
        gradientLayer.colors = [ UIColor.clear.cgColor,UIColor.black.cgColor]
        gradientLayer.locations = [0.7, 1.0]
        self.clipsToBounds = true
        self.layer.addSublayer(gradientLayer)
        
    }

}
