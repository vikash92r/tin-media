//
//  TopMenuCollectionViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 03/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class TopMenuCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lbl_Menu: UILabel!
    @IBOutlet weak var underlineView: UIView!
}
