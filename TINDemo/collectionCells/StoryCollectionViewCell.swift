//
//  StoryCollectionViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 04/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl_timedetail: UILabel!
    @IBOutlet weak var lbl_detail: UILabel!
    @IBOutlet weak var playVideosIcon: UIImageView!
}
