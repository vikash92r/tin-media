//
//  HomeBannerCollectionViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 06/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class HomeBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lbl_timeDetails: UILabel!
    @IBOutlet weak var lbl_newsheading: UILabel!
    @IBOutlet weak var lbl_newsDetails: UILabel!
    @IBOutlet weak var overlayView: UIView!
}
