//
//  UpComingEventCollectionViewCell.swift
//  TINDemo
//
//  Created by Anuj Mohan Saxena on 19/12/19.
//  Copyright © 2019 Anuj Mohan Saxena. All rights reserved.
//

import UIKit

class UpComingEventCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventImage: UIImageView!

    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_datedeatail: UILabel!
    @IBOutlet weak var upcomingImageContainerView: UIView!
    @IBOutlet weak var overlayView: OverlayBlackView!
    
}
